import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector from "i18next-browser-languagedetector";

import { EN } from './translations/en';
import { ZH } from './translations/zh';

i18n
	.use(LanguageDetector)
	.use(initReactI18next) // passes i18n down to react-i18next
	.init({
		resources: {
            en: {
                translation: EN
            },
            zh: {
                translation: ZH
            }
        }
	});

i18n.changeLanguage(localStorage.getItem('lang') || 'en');

export default i18n;
