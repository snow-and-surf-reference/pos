import {
	IonApp,
	IonContent,
	IonImg,
	IonLoading,
	IonPage,
	IonRouterOutlet,
	IonSplitPane,
	setupIonicReact
} from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";
import { Route, Switch } from "react-router-dom";
import Menu from "./components/Global/Menu";

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/display.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/padding.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";

/* Theme variables */
import { useEffect, useState } from "react";
import { initFirebase } from "./app/firebase/global";
import { useAppSelector } from "./app/hooks";
import { selectAuthUserIsInit, selectLoggingIn } from "./app/slices/authUserSlice";
import Logo from "./assets/images/2x/logo.png";
import AuthCheck from "./components/AuthCheck";
import UserMenu from "./components/Global/UserMenu";
import Action from "./pages/Auth/Action";
import ForgotPassword from "./pages/Auth/ForgotPassword";
import Login from "./pages/Auth/Login";
import Signup from "./pages/Auth/Signup";
import Checkout from "./pages/Booking/Checkout";
import NewBooking from "./pages/Booking/NewBooking";
import OrderPreview from "./pages/Booking/OrderPreview";
import PaymentCompleted from "./pages/Booking/PaymentCompleted";
import Home from "./pages/Home/Home";
import "./theme/variables.css";

import { selectCapacities } from "./app/slices/venueSlice";
import EmailNotVerify from "./pages/Auth/EmailNotVerify";
import ComingSoon from "./pages/Booking/ComingSoon";
import MyBookings from "./pages/Booking/MyBookings";
import SessionDetails from "./pages/Booking/SessionDetails";
import FaqList from "./pages/Faq/FaqList";
import PageLoader from "./pages/PageLoader";
import MyTopups from "./pages/Topup/MyTopups";
import TopupPage from "./pages/Topup/TopupPage";
import TopupSuccess from "./pages/Topup/TopupSuccess";
import MyAccount from "./pages/User/MyAccount";
import "./theme/style.scss";
import { t } from "i18next";

setupIonicReact({
	mode: "ios",
});

const App: React.FC = () => {
	const authUserIsInit: boolean = useAppSelector(selectAuthUserIsInit);
	const caps = useAppSelector(selectCapacities);
	const [devMode, setDevMode] = useState(false);
	const loggingIn = useAppSelector(selectLoggingIn);

	useEffect(() => {
		initFirebase();
	}, []);

	return (
		<>
			{!authUserIsInit || !caps ? (
				<IonApp>
					<IonPage>
						<IonContent color={"dark"}>
							<div
								style={{
									display: "flex",
									height: "100vh",
								}}
								className="ion-justify-content-center ion-align-items-center ion-text-center"
							>
								<IonImg
									src={Logo}
									className="flashing-logo"
									style={{
										width: "156px",
										height: "auto",
										maxWidth: "70vh",
									}}
								/>
							</div>
						</IonContent>
					</IonPage>
				</IonApp>
			) : (
				<IonApp>
					<IonReactRouter>
						<IonSplitPane contentId="main" when={false}>
							<UserMenu />
							<Menu />
							<IonRouterOutlet id="main">
									<AuthCheck
										setDevMode={(bool: boolean) => setDevMode(bool)}
									>
									<Switch>
										<Route path={`/newBooking/:presetType`}>
											{caps && caps.hidePos && !devMode ? <ComingSoon /> : <NewBooking />}
										</Route>
										<Route path={`/newBooking`} exact>
											{caps && caps.hidePos && !devMode ? <ComingSoon /> : <NewBooking />}
										</Route>
										<Route path={`/newBookingSnow`} exact>
											{caps && caps.hidePos && !devMode ? (
												<ComingSoon />
											) : (
												<NewBooking playType="snow" />
											)}
										</Route>
										<Route path={`/newBookingSurf`} exact>
											{caps && caps.hidePos && !devMode ? (
												<ComingSoon />
											) : (
												<NewBooking playType="surf" />
											)}
										</Route>
										<Route path={`/orderPreview`} exact>
											<>
												<OrderPreview />
											</>
										</Route>
										<Route path={`/checkout/:sessionId`}>
											<>
												<Checkout />
											</>
										</Route>
										<Route path={`/paymentCompleted/:sessionId`}>
											<>
												<PaymentCompleted />
											</>
										</Route>
										<Route path={`/orderCompleted/:sessionId`}>
											<>
												<PaymentCompleted />
											</>
										</Route>
										<Route path={`/login`} exact>
											<>
												<Login />
											</>
										</Route>
										<Route path={`/signup`} exact>
											<>
												<Signup />
											</>
										</Route>
										<Route path={`/forgotPassword`} exact>
											<>
												<ForgotPassword />
											</>
										</Route>
										<Route path={`/action`} exact>
											<>
												<Action />
											</>
										</Route>

										<Route path={`/topup`} exact>
											<>
												{caps && caps.hidePos && !devMode ? <ComingSoon /> : <TopupPage />}
											</>
										</Route>
										<Route path={`/topupSuccess/:id`}>
											<TopupSuccess />
										</Route>
										<Route path={`/myBookings`} exact>
											<>
												<MyBookings />
											</>
										</Route>
										<Route path={`/myTopups`} exact>
											<>
												<MyTopups />
											</>
										</Route>
										<Route path={`/emailNotVerify`} exact>
											<>
												<EmailNotVerify />
											</>
										</Route>
										<Route path={`/myAccount`} exact>
											<>
												<MyAccount />
											</>
										</Route>
										<Route path={`/sessionDetails/:sessionId`}>
											<>
												<SessionDetails />
											</>
										</Route>
										<Route path={`/faq`}>
											<>
												<FaqList />
											</>
										</Route>
										<Route path={`/page/:title`}>
											<>
												<PageLoader />
											</>
										</Route>
										<Route path={`/`} exact>
											<Home />
										</Route>
									</Switch>
								</AuthCheck>
							</IonRouterOutlet>
								<IonLoading
									isOpen={loggingIn}
									spinner={`dots`}
									message={t(`Logging In, Please wait`)}
								/>
						</IonSplitPane>
					</IonReactRouter>
				</IonApp>
			)}
		</>
	);
};

export default App;
