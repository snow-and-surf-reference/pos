import {
	IonButton,
	IonCard,
	IonCardContent,
	IonCardHeader,
	IonCardSubtitle,
	IonCardTitle,
	IonContent,
	IonIcon,
	IonItem,
	IonLabel,
	IonList,
	IonPage,
	IonTitle,
} from "@ionic/react";
import * as TopupTypes from "@tsanghoilun/snow-n-surf-interface/types/topUp";
import dayjs from "dayjs";
import { diamondOutline, walletOutline } from "ionicons/icons";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useHistory, useLocation, useParams } from "react-router";
import { getTopupOrderById } from "../../app/firebase/global";
import { useAppSelector } from "../../app/hooks";
import { selectAuthUser } from "../../app/slices/authUserSlice";
import Header from "../../components/Global/Header";
import LoadingCard from "../../components/Global/LoadingCard";

interface TopupOrderState {
	status: "loading" | "loaded" | "invalid";
	order: TopupTypes.TopupOrder | null;
}

const TopupSuccess: React.FC = () => {
	const { t } = useTranslation();
	const authUser = useAppSelector(selectAuthUser);
	const history = useHistory();
	const location = useLocation<any>();
	const { id } = useParams<{ id: string }>();
	const [order, setOrder] = useState<TopupOrderState>({
		status: "loading",
		order: null,
	});

	useEffect(() => {
		const updateOrder = async () => {
			const orderDoc = await getTopupOrderById(id);
			if (!orderDoc) {
				setOrder({
					status: "invalid",
					order: null,
				});
				return;
			}
			setOrder({
				status: "loaded",
				order: orderDoc as TopupTypes.TopupOrder,
			});
		};
		updateOrder();
	}, [id]);

	return (
		<IonPage>
			<Header />
			<IonContent className="fluid">
				{order.status === "loading" ? (
					<LoadingCard />
				) : order.status === "loaded" && order.order ? (
					<div>
						{!location?.state?.from ? (
							<IonCard color={`success`}>
								<IonCardContent>
									<IonLabel>
										<h1>
											<b>{t(`Topup Success!`)}</b>
										</h1>
									</IonLabel>
								</IonCardContent>
							</IonCard>
						) : null}
						<IonCard color={`dark`}>
							{!location?.state?.from ? (
								<>
									<IonCardHeader>
										<IonCardSubtitle>
											<b>{t(`Your have successfully topped up!`)}</b>
										</IonCardSubtitle>
									</IonCardHeader>
								</>
							) : (
								<>
									<IonCardHeader>
										<IonCardTitle>
											<b>{t(`Topup Record`)}</b>
										</IonCardTitle>
									</IonCardHeader>
								</>
							)}
							<IonCardContent className="ion-no-padding ion-padding-bottom">
								{!location?.state?.from ? (
									<IonList color="dark">
										<IonItem color={`dark`}>
											<IonIcon icon={walletOutline} slot="start" />
											<IonLabel>
													<b>{t(`Latest Balance`)}</b>
											</IonLabel>
										</IonItem>
										<IonItem lines="none" color={`dark`}>
											<IonIcon
												icon={diamondOutline}
												slot="start"
												size="large"
											/>
											<IonLabel>
												<h1>
													<b>{authUser?.wallet.balance.toLocaleString()}</b>
												</h1>
											</IonLabel>
										</IonItem>
									</IonList>
								) : null}
								<br />
								<br />
								<IonList>
									<IonItem color={"dark"}>
											<IonLabel color={"medium"}>{t(`Topup Date`)}</IonLabel>
										<IonLabel>
											{dayjs(order.order.createdAt).format(
												`hh:mmA, DD MMM YYYY`
											)}
										</IonLabel>
									</IonItem>
									<IonItem color={"dark"}>
											<IonLabel color={"medium"}>{t(`Order ID`)}</IonLabel>
										<IonLabel>{order.order.id}</IonLabel>
									</IonItem>
									{order.order.paymentMethod ? (
										<IonItem color={"dark"}>
												<IonLabel color={"medium"}>{t(`Payment Method`)}</IonLabel>
											<IonLabel>
													{order.order.paymentMethod === "online_cc"
														? t("Credit Card (Online)")
														: order.order.paymentMethod === "cc" || order.order.paymentMethod === 'vm' || order.order.paymentMethod === 'ae'
															? t("Credit Card")
															: order.order.paymentMethod === 'online_vm'
																? 'Visa/Master (Online)'
																: order.order.paymentMethod === 'online_ae'
																	? 'American Express (Online)'
																	: `${t("Cash / Other")}} (${order.order.paymentMethod})`}
											</IonLabel>
										</IonItem>
									) : null}
									<IonItem color={"dark"}>
											<IonLabel color={"medium"}>{t(`Request ID`)}</IonLabel>
										<IonLabel>{order.order.requestId}</IonLabel>
									</IonItem>
									<IonItem color={"dark"}>
											<IonLabel color={"medium"}>{t(`Credit Received`)}</IonLabel>
										<IonLabel>
											{order.order.topupPackage
												? `${order.order.topupPackage.receivedCredits.toLocaleString()}`
												: `${order.order.exactAmount?.toLocaleString()}`}
										</IonLabel>
									</IonItem>
									<IonItem color={"dark"}>
											<IonLabel color={"medium"}>{t(`total`)}</IonLabel>
										<IonLabel>
											{`HKD $${
												order.order.topupPackage
													? `${order.order.topupPackage.receivedCredits.toLocaleString()}`
													: `${order.order.exactAmount?.toLocaleString()}`
											}`}
										</IonLabel>
									</IonItem>
								</IonList>
								<br />
								{!location?.state?.from ? (
									<IonButton
										expand="block"
										onClick={() => history.push(`/newBooking`)}
										color="secondary"
										className="ion-margin"
										size="default"
									>
										{t(`bookNow`)}
									</IonButton>
								) : null}

								<IonButton
									expand="block"
									color={`darker`}
									size="default"
									className="ion-margin"
									onClick={() =>
										history.push(
											location?.state?.from ? location?.state?.from : "/"
										)
									}
								>
									{location?.state?.from ? t(`back`) : t(`Back To Home`)}
								</IonButton>
							</IonCardContent>
						</IonCard>
					</div>
				) : (
					<IonCard color={"light"}>
						<IonContent>
									<IonTitle>{t(`No Topup Order Found.`)}</IonTitle>
							<IonLabel>
								<h3>
									{t("No topup order found with the given order ID. Please check again later.")}
								</h3>
							</IonLabel>

							<IonButton expand="block" onClick={() => history.push(`/`)}>
								{t(`Back To Home`)}
							</IonButton>
						</IonContent>
					</IonCard>
				)}
			</IonContent>
		</IonPage>
	);
};

export default TopupSuccess;
