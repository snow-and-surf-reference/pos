import {
	IonButton,
	IonCard,
	IonCardContent,
	IonCheckbox,
	IonCol,
	IonContent,
	IonFooter,
	IonGrid, IonIcon,
	IonItem,
	IonLabel,
	IonList,
	IonLoading, IonPage,
	IonRow, IonToolbar
} from "@ionic/react";
import * as TopupTypes from "@tsanghoilun/snow-n-surf-interface/types/topUp";
import {
	cartOutline, diamondOutline, walletOutline
} from "ionicons/icons";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router";
import { useAppSelector } from "../../app/hooks";
import { selectAuthUser } from "../../app/slices/authUserSlice";
import { selectTopupPackages } from "../../app/slices/topupPackages";
import Header from "../../components/Global/Header";
import PaymentFlexForm from "../../components/Payments/PaymentFlexForm";
import TopupFailedModal from "../../components/Topup/TopupFailedModal";
import TopupPackageSelection from "../../components/Topup/TopupPackageSelection";
import TopupTermsModal from "../../components/Topup/TopupTermsModal";
import "./TopupPage.scss";
export interface PaymentFields {
	name: string;
	exp: string;
	address1: string;
	address2: string;
	country: string;
}

const TopupPage: React.FC = () => {
	const { t } = useTranslation();
	const authUser = useAppSelector(selectAuthUser);
	const topupPackages = useAppSelector(selectTopupPackages);
	const [selectedPackage, setSelectedPackage] =
		useState<TopupTypes.TopUpPackages | null>(null);
	const [mostValuedPackage, setMostValuedPackage] =
		useState<TopupTypes.TopUpPackages | null>(null);
	const [paymentFields, setPaymentFields] = useState<PaymentFields>({
		name: "",
		exp: "",
		address1: "",
		address2: "",
		country: "HK",
	});
	const [termsAgreed, setTermsAgreed] = useState(false);
	const [showTermsModal, setShowTermsModal] = useState(false);
	const [disablePay, setDisablePay] = useState(true);
	const [sending, setSending] = useState(false);
	const [showPaymentFailedModal, setShowPaymentFailedModal] = useState(false);
	const history = useHistory();

	useEffect(() => {
		if (topupPackages.length < 2) {
			return;
		}
		const mostValued = [...topupPackages].sort((a, b) => {
			const aValue = a.receivedCredits - a.initCredits;
			const bValue = b.receivedCredits - b.initCredits;
			if (aValue > bValue) {
				return -1;
			} else if (bValue > aValue) {
				return 1;
			} else {
				return 0;
			}
		})[0];
		setMostValuedPackage(mostValued);
	}, [topupPackages]);

	// check can topup and pay or not
	useEffect(() => {
		const nameValid = paymentFields.name !== "";
		const expValid = /([0]{1}[1-9]{1})|([1]{1}[0-2]{1})\/[0-9]{2}/g.test(
			paymentFields.exp
		);
		const addressValid = paymentFields.address1 !== "";
		if (
			nameValid &&
			expValid &&
			addressValid &&
			!!selectedPackage &&
			!!authUser &&
			termsAgreed
		) {
			setDisablePay(false);
		} else {
			setDisablePay(true);
		}
	}, [paymentFields, selectedPackage, authUser, termsAgreed]);

	// make payment
	const handleSubmit = async () => {
		if (!selectedPackage || !authUser) {
			return;
		}

		setSending(true);
		console.clear();
		// ready payload
		const packageId = selectedPackage.id;
		let payload = { ...paymentFields, packageId, authUser };

		const res = await (window as any).attemptPay(payload);
		if (res && res.result === "AUTHORIZED") {
			console.log("Payment Success", res);
			setSending(false);
			// go to success page
			history.push(`/topupSuccess/${res.orderId}`);
		} else {
			console.log("Payment Not Authorised", res);
			setSending(false);
			// show error alert
			setShowPaymentFailedModal(true);
			return;
		}
	};

	return (
		<IonPage>
			<Header />
			<IonContent className="fluid">
				<IonGrid className="ion-text-center">
					<IonRow>
						<IonCol>
							<IonLabel>
								<h1>
									<b>{t(`Top Up`)}</b>
								</h1>
							</IonLabel>
						</IonCol>
					</IonRow>
				</IonGrid>
				{authUser && (
					<>
						<IonCard color="dark">
							<IonItem lines="none" color="dark">
								<IonIcon icon={walletOutline} slot="start" />
								<IonLabel>
									<h2>
										<b>{t(`My Wallet`)}</b>
									</h2>
								</IonLabel>
							</IonItem>
							<IonCardContent className="ion-no-padding">
								<IonList inset>
									<IonItem color={`warning`}>
										<IonIcon icon={diamondOutline} slot="start" />
										<IonLabel>
											<h1>
												<b>
													{Number(authUser?.wallet.balance).toLocaleString()}
												</b>
											</h1>
										</IonLabel>
									</IonItem>
								</IonList>
							</IonCardContent>
						</IonCard>
						
						<TopupPackageSelection
							topupPackages={topupPackages}
							selectedPackage={selectedPackage}
							mostValuedPackage={mostValuedPackage}
							setSelectedPackage={setSelectedPackage}
						/>

						<PaymentFlexForm
							fields={paymentFields}
							setFields={(newFields) => {
								setPaymentFields(newFields);
							}}
						/>

						<IonCard
							color={termsAgreed ? "warning" : "light"}
							style={{
								marginBottom: "4rem",
							}}
						>
							<IonItem color={termsAgreed ? "warning" : "light"} lines="none">
								<IonCheckbox
									checked={termsAgreed}
									onIonChange={(e) => {
										setTermsAgreed(e.detail.checked);
									}}
									slot="start"
								/>
								<IonLabel>
									<h3>{t(`I agree to the Terms & Conditions`)}</h3>
								</IonLabel>
								<IonButton
									onClick={(e) => {
										e.preventDefault();
										e.stopPropagation();
										setShowTermsModal(true);
									}}
								>{t(`view`)}</IonButton>
							</IonItem>
						</IonCard>
					</>
				)}
			</IonContent>
			<IonFooter color="dark" className="dark">
				<IonToolbar
					color="dark"
					style={{ padding: `0.7rem` }}
					className="fluid"
				>
					<IonLabel slot="start">
						<h2>
							<b>{t(`total`)}:</b>
						</h2>
						<h1>
							{`HKD `}
							<b>$ {Number(selectedPackage?.price || 0).toLocaleString()}</b>
						</h1>
					</IonLabel>
					<IonButton
						slot="end"
						color={`warning`}
						disabled={disablePay}
						onClick={() => handleSubmit()}
					>
						<IonIcon icon={cartOutline} slot="start" />
						<IonLabel>{t(`PAY NOW`)}</IonLabel>
					</IonButton>
				</IonToolbar>
			</IonFooter>
			<IonLoading isOpen={sending} message={t(`Topping Up. DO NOT REFRESH!`)} />
			<TopupTermsModal
				showTermsModal={showTermsModal}
				setShowTermsModal={setShowTermsModal}
			/>

			<TopupFailedModal
				showPaymentFailedModal={showPaymentFailedModal}
				setShowPaymentFailedModal={setShowPaymentFailedModal}
			/>
		</IonPage>
	);
};

export default TopupPage;
