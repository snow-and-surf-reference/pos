import { IonButtons, IonCard, IonCardContent, IonChip, IonCol, IonContent, IonGrid, IonItem, IonLabel, IonList, IonPage, IonRow } from "@ionic/react";
import * as TopupTypes from '@tsanghoilun/snow-n-surf-interface/types/topUp';
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from 'dayjs/plugin/utc';
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router";
import { useAppSelector } from "../../app/hooks";
import { selectTopupOrders } from "../../app/slices/authUserSlice";
import Header from "../../components/Global/Header";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);


const MyTopups: React.FC = () => {
    const { t } = useTranslation();
    const orders: TopupTypes.TopupOrder[] = useAppSelector(selectTopupOrders);
    const history = useHistory();
    return (
        <IonPage>
            <Header />
            <IonContent className="fluid">
                <IonGrid>
                    <IonRow><IonCol></IonCol></IonRow>
                    <IonRow>
                        <IonCol>
                            <IonLabel><b>{t(`myTopUpHistory`)}</b></IonLabel>
                        </IonCol>
                    </IonRow>
                </IonGrid>
                <IonCard color={`light`}>
                    <IonCardContent>
                        <IonList color={`light`}>
                            {
                                [...orders]
                                    .sort((a, b) => a.createdAt.valueOf() > b.createdAt.valueOf() ? -1 : 1)
                                    .map(s => (
                                    <IonItem key={`topupOrder-${s.id}`}
                                        color={`light`}
                                        button
                                        onClick={() => history.push(`/topupSuccess/${s.id}`, {from: `/myTopups`})}
                                    >
                                        {/* <IonIcon icon={diamondOutline} slot='start' /> */}
                                        <IonLabel>
                                            <h3><b>{`${s.topupPackage ? s.topupPackage.receivedCredits.toLocaleString() : s.exactAmount!} ${t(`credits`)}`}</b></h3>
                                            <p>{dayjs(s.createdAt).format(`hh:mmA`)}</p>
                                            <p>{dayjs(s.createdAt).format(`DD MMM YYYY`)}</p>
                                        </IonLabel>
                                        <IonButtons slot="end">
                                            <IonChip>
                                                <IonLabel>{`HKD $${s.topupPackage ? s.topupPackage.receivedCredits.toLocaleString() : s.exactAmount!.toLocaleString()}`}</IonLabel>
                                            </IonChip>
                                        </IonButtons>
                                    </IonItem>
                                ))
                            }
                        </IonList>
                    </IonCardContent>
                </IonCard>
            </IonContent>
        </IonPage>
    )
}

export default MyTopups;
