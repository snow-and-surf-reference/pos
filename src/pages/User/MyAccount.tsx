import {
	IonButton,
	IonButtons,
	IonCard,
	IonCardContent,
	IonChip,
	IonCol,
	IonContent, IonGrid, IonIcon,
	IonInput,
	IonItem,
	IonLabel,
	IonList, IonPage,
	IonRow,
	IonSpinner
} from "@ionic/react";
import * as GearTypes from "@tsanghoilun/snow-n-surf-interface/types/gear";
import * as UserTypes from "@tsanghoilun/snow-n-surf-interface/types/user";
import dayjs from "dayjs";
import {
	arrowForwardCircleOutline,
	closeCircleOutline,
	createOutline,
	diamondOutline,
	saveOutline
} from "ionicons/icons";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router";
import { updateEContact } from "../../app/firebase/global";
import { useAppSelector } from "../../app/hooks";
import useBreakpoints from "../../app/hooks/useBreakpoints";
import {
	selectAuthUser,
	selectAuthUserIsInit,
	selectAuthUserStorages
} from "../../app/slices/authUserSlice";
import { selectCustomerClasses } from "../../app/slices/customerClassesSlice";
import Avatar from "../../components/Global/Avatar";
import Header from "../../components/Global/Header";
import i18n from "../../i18n";
import CustomerClassHelpModal from "./CustomerClassHelpModal";

const newEContact: UserTypes.EmergencyContact = {
	name: "",
	phone: "",
};

interface ItemProps {
	label: string;
	value: string;
	breakpoint: string;
}
const DetailItems: React.FC<ItemProps> = (props) => {
	const { label, value, breakpoint } = props;
	return (
		<>
			{breakpoint === "xs" || breakpoint === "sm" ? (
				<IonItem lines="none">
					<IonLabel>
						<p>{label.toUpperCase()}</p>
						<h3>{value}</h3>
					</IonLabel>
				</IonItem>
			) : (
				<IonItem lines="none">
					<IonLabel position="fixed">
						<p>{label.toUpperCase()}</p>
					</IonLabel>
					<IonLabel className="ion-text-end">{value}</IonLabel>
				</IonItem>
			)}
		</>
	);
};

const MyAccount: React.FC = () => {
	const { t } = useTranslation();
	const authUser: UserTypes.Customer | null = useAppSelector(selectAuthUser);
	const authUserIsInit: Boolean = useAppSelector(selectAuthUserIsInit);
	const storages: GearTypes.GearStorage[] = useAppSelector(
		selectAuthUserStorages
	);
	const [eContactEditing, setEContactEditing] = useState("idle");
	const [eContact, setEContact] =
		useState<UserTypes.EmergencyContact>(newEContact);
	const { active } = useBreakpoints();
	const history = useHistory();

	const classes: UserTypes.CustomerClass[] = useAppSelector(
		selectCustomerClasses
	);
	const [currentClass, setCurrentClass] =
		useState<UserTypes.CustomerClass | null>(null);

	const [showClassModal, setShowClassModal] = useState(false);

	useEffect(() => {
		if (!authUser || classes.length <= 0) {
			return;
		}
		const tempClasses: UserTypes.CustomerClass[] = classes
			.filter((x) => x.creditTier <= authUser.wallet.spent)
			.sort((a, b) => a.creditTier - b.creditTier);
		const highestClass: UserTypes.CustomerClass =
			tempClasses[tempClasses.length - 1];
		// console.log(highestClass);
		setCurrentClass({ ...highestClass });
	}, [classes, authUser]);

	useEffect(() => {
		if (authUser && authUser.emergencyContact) {
			setEContact(authUser.emergencyContact);
		}
	}, [authUser]);

	useEffect(() => {
		if (eContactEditing === "updating") {
			setEContact(newEContact);
		}
	}, [eContactEditing]);

	const handlechangeEContact = async () => {
		if (!authUser || !eContact.name || !eContact.phone) {
			return;
		}
		setEContactEditing("updating");
		await updateEContact(authUser, eContact);
		setEContactEditing("idle");
	};

	return (
		<IonPage>
			<Header />
			<IonContent class="fluid">
				{!authUserIsInit || !authUser ? (
					<>
						<IonCard className="ion-text-center" color={`dark`}>
							<IonCardContent>
								<IonLabel>
									<h2>{t(`loading`)}</h2>
								</IonLabel>
								<IonSpinner name="dots" />
							</IonCardContent>
						</IonCard>
					</>
				) : (
					<IonGrid>
						<IonRow>
							<IonCol
								sizeXs="12"
								sizeSm="12"
								sizeMd="12"
								sizeLg="4"
								sizeXl="4"
								pushLg="8"
								pushXl="8"
							>
								<IonCard color={`dark`}>
									<IonItem lines="none" color={"dark"}>
										<IonLabel>
												<h1>{t(`My Wallet`)}</h1>
										</IonLabel>
									</IonItem>

									<IonCardContent>
										<IonItem lines="none" color={"dark"}>
											<IonIcon
												icon={diamondOutline}
												size="medium"
												slot="start"
											/>
											<IonLabel>
												<h1>
													<b>{authUser.wallet.balance.toLocaleString()}</b>
												</h1>
											</IonLabel>
										</IonItem>
										<IonButton
											color={"warning"}
											expand="block"
											onClick={() => {
												history.push(`/topup`);
											}}
										>
												<IonLabel>{t(`buyCredits`)}</IonLabel>
											<IonIcon icon={arrowForwardCircleOutline} slot="end" />
										</IonButton>
									</IonCardContent>
								</IonCard>
							</IonCol>
							<IonCol
								sizeXs="12"
								sizeSm="12"
								sizeMd="12"
								sizeLg="8"
								sizeXl="8"
								pullLg="4"
								pullXl="4"
							>
								<IonCard>
									<IonItem lines="none">
										<IonLabel>
												<h1>{t(`myProfile`)}</h1>
										</IonLabel>
									</IonItem>
									<IonCardContent>
										<IonGrid>
											<IonRow>
												<IonCol
													size="auto"
													sizeXs="12"
													sizeSm="12"
													sizeMd="auto"
													sizeLg="auto"
													sizeXl="auto"
													className="ion-justify-content-center ion-text-center"
													style={{ display: `flex` }}
												>
													<Avatar usrname={authUser.usrname} size={64} />
												</IonCol>
												<IonCol>
													<IonList lines="none">
														<IonItem>
															{active === "xs" || active === "sm" ? (
																<>
																	<IonLabel className="ion-text-center">
																		<h1>
																			<b>{authUser.usrname}</b>
																		</h1>
																			<h3>{t(`Member Number`)}: #{authUser.memberId}</h3>
																	</IonLabel>
																</>
															) : (
																<>
																	<IonLabel className="ion-text-start">
																		<h1>
																			<b>{authUser.usrname}</b>
																		</h1>
																		<h3>{t(`Member Number`)}: #{authUser.memberId}</h3>
																	</IonLabel>
																</>
															)}
														</IonItem>
													</IonList>
												</IonCol>
											</IonRow>
											{currentClass && (
												<IonRow>
													<IonCol
														style={{
															display: "flex",
															flexDirection:
																active === "xs" || active === "sm"
																	? "column"
																	: "row",
														}}
														className={`ion-align-items-center ${
															active === "xs" || active === "sm"
																? "ion-text-center"
																: "ion-text-start"
														}`}
													>
														<IonChip color={`secondary`}>
														{i18n.language === 'en' ? currentClass.name.en : (currentClass.name.zh || currentClass.name.en) }
														</IonChip>
														<IonButton
															fill="clear"
															color={`medium`}
															onClick={() => setShowClassModal(true)}
														>
																<p>{t(`What's this?`)}</p>
														</IonButton>
													</IonCol>
												</IonRow>
											)}
										</IonGrid>
									</IonCardContent>
								</IonCard>
								<IonCard>
									<IonItem lines="none">
										<IonLabel>
												<h1>{t(`My Details`)}</h1>
										</IonLabel>
									</IonItem>
									<IonCardContent>
										<IonList lines="full">
											<DetailItems
												label={t(`Email`)}
												value={authUser.email}
												breakpoint={active}
											/>
											<DetailItems
												label={t(`Phone`)}
												value={authUser.phone}
												breakpoint={active}
											/>
											<DetailItems
												label={t(`DOB`)}
												value={dayjs(authUser.dob).format("DD MMM YYYY")}
												breakpoint={active}
											/>
										</IonList>
									</IonCardContent>
								</IonCard>

								<IonCard color={"light"}>
									<IonItem lines="none" color={"light"}>
										<IonLabel>
												<h1>{t(`Emergency Contacts`)}</h1>
										</IonLabel>
									</IonItem>
									<IonCardContent>
										{eContactEditing === "idle" ? (
											<>
												{authUser.emergencyContact ? (
													<>
														<IonItem lines="none" color={`light`}>
															<IonLabel>
																<h3>
																	<b>{authUser.emergencyContact.name}</b>
																</h3>
																	<p>{t(`Phone`)}: {authUser.emergencyContact.phone}</p>
															</IonLabel>
														</IonItem>
														<IonItem lines="none" color={"light"}>
															<IonButtons slot="start">
																<IonChip
																	onClick={() => {
																		setEContactEditing("editing");
																	}}
																>
																	<IonIcon icon={createOutline} />
																	<IonLabel>{t(`edit`)}</IonLabel>
																</IonChip>
															</IonButtons>
														</IonItem>
													</>
												) : (
													<IonGrid color={"light"}>
														<IonRow>
															<IonCol>
																<IonItem lines="none" color={"light"}>
																	<IonLabel>
																		<h3>
																			{t(`You have not saved any Emergency Contacts.`)}
																		</h3>
																	</IonLabel>
																</IonItem>

																<IonItem lines="none" color={"light"}>
																	<IonButtons slot="start">
																		<IonChip
																			onClick={() => {
																				setEContactEditing("editing");
																			}}
																		>
																			<IonIcon icon={createOutline} />
																					<IonLabel>{t(`edit`)}</IonLabel>
																		</IonChip>
																	</IonButtons>
																</IonItem>
															</IonCol>
														</IonRow>
													</IonGrid>
												)}
											</>
										) : eContactEditing === "editing" ? (
											<>
												<IonList inset>
													<IonItem>
														<IonLabel position="stacked">
															{t(`Name of Contact`)}
														</IonLabel>
														<IonInput
															type="text"
															placeholder="Chan Tai Man"
															value={eContact.name}
															onIonChange={(e) => {
																setEContact(
																	Object.assign(
																		{ ...eContact },
																		{
																			name: e.detail.value!,
																		}
																	)
																);
															}}
														/>
													</IonItem>
													<IonItem>
																<IonLabel position="stacked">{t(`Phone Number`)}</IonLabel>
														<IonInput
															type="text"
															placeholder="90123456"
															value={eContact.phone}
															onIonChange={(e) => {
																setEContact(
																	Object.assign(
																		{ ...eContact },
																		{
																			phone: e.detail.value!,
																		}
																	)
																);
															}}
														/>
													</IonItem>
													<IonItem
														color={`warning`}
														button
														disabled={
															!eContact.name ||
															eContact.phone.length < 8 ||
															eContact.phone.length > 8
														}
														onClick={() => {
															handlechangeEContact();
														}}
													>
														<IonIcon
															slot="start"
															icon={saveOutline}
															size="small"
														/>
																<IonLabel>{t(`save`)}</IonLabel>
													</IonItem>
												</IonList>
												<IonButton
													expand="full"
													fill="clear"
													onClick={() => {
														setEContactEditing("idle");
													}}
												>
													<IonIcon icon={closeCircleOutline} slot="start" />
															<IonLabel>{t(`cancel`)}</IonLabel>
												</IonButton>
											</>
										) : eContactEditing === "updating" ? (
											<>
												<IonGrid color={"light"}>
													<IonRow>
														<IonCol>
															<IonItem color={"light"} lines="none">
																<IonSpinner slot="start" name="dots" />
																			<IonLabel>{t(`updating`)}...</IonLabel>
															</IonItem>
														</IonCol>
													</IonRow>
												</IonGrid>
											</>
										) : null}
									</IonCardContent>
								</IonCard>

								<IonCard>
									<IonItem lines="none">
										<IonLabel>
												<h1>{t(`My Gear Storages`)}</h1>
										</IonLabel>
									</IonItem>
									<IonCardContent>
										{storages.length <= 0 ? (
											<IonItem lines="none">
												<IonLabel>
													<h3>
															<b>{t(`You Don't have any Gear stored with us.`)}</b>
													</h3>
														<p>{t(`Visit us on site to enquire about gear storage.`)}</p>
												</IonLabel>
											</IonItem>
										) : (
											<>
												<IonList>
													{storages.map((x) => (
														<IonItem key={`storage-item-${x.id}`}>
															<IonLabel>
																<h2>
																	{t(`storage`)} #<b>{x.storageNum}</b>
																</h2>
																<p>
																	{t(`Valid until`)}:{" "}
																	{dayjs.tz(x.validTo).format(`DD MMM YYYY`)}
																</p>
															</IonLabel>
														</IonItem>
													))}
												</IonList>
											</>
										)}
									</IonCardContent>
								</IonCard>
							</IonCol>
						</IonRow>
					</IonGrid>
				)}
			</IonContent>
			<CustomerClassHelpModal
				isOpen={showClassModal}
				onDismiss={() => setShowClassModal(false)}
			/>
		</IonPage>
	);
};

export default MyAccount;
