import {
	IonModal,
	IonHeader,
	IonToolbar,
	IonTitle,
	IonContent,
	IonFooter,
	IonButton,
	IonGrid,
	IonRow,
	IonCol,
} from "@ionic/react";
import { useTranslation } from "react-i18next";
import { useAppSelector } from "../../app/hooks";
import { selectAllOtherContents } from "../../app/slices/otherContentsSlice";
import i18n from "../../i18n";

interface Props {
	isOpen: boolean;
	onDismiss: () => void;
}

const CustomerClassHelpModal: React.FC<Props> = (props) => {
	const { t } = useTranslation();
	const { isOpen, onDismiss } = props;
	const otherContent = useAppSelector(selectAllOtherContents);

	return (
		<IonModal isOpen={isOpen} onDidDismiss={() => onDismiss()}>
			<IonHeader>
				<IonToolbar color={`dark`}>
					<IonTitle>{t(`Types of Memberships`)}</IonTitle>
				</IonToolbar>
			</IonHeader>
			<IonContent>
				<IonGrid>
					<IonRow>
						<IonCol>
							<div
								className="ion-padding"
								dangerouslySetInnerHTML={{
									__html:
										i18n.language === "en"
											? String(
													otherContent.find(
														(x) => x.slug === "membership_modal_content"
													)?.content.en
											  )
											: String(
													otherContent.find(
														(x) => x.slug === "membership_modal_content"
													)?.content.zh
											  ) ||
											  String(
													otherContent.find(
														(x) => x.slug === "membership_modal_content"
													)?.content.en
											  ),
								}}
							></div>
						</IonCol>
					</IonRow>
				</IonGrid>
			</IonContent>
			<IonFooter>
				<IonToolbar color={"dark"}>
					<IonButton expand="block" onClick={() => onDismiss()}>
						{t(`close`)}
					</IonButton>
				</IonToolbar>
			</IonFooter>
		</IonModal>
	);
};

export default CustomerClassHelpModal;
