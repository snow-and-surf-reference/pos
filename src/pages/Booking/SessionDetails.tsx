import {
	IonCard,
	IonCardContent,
	IonContent, IonLabel, IonPage,
	IonSpinner
} from "@ionic/react";
import * as SessionType from "@tsanghoilun/snow-n-surf-interface/types/session";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import { useEffect, useState } from "react";
import { useParams } from "react-router";
import { getSessionById } from "../../app/firebase/global";
import { useAppSelector } from "../../app/hooks";
import { selectAuthUser } from "../../app/slices/authUserSlice";
import Header from "../../components/Global/Header";
import SessionDetailsContent from "./SessionDetailsContent";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

const SessionDetails: React.FC = () => {
	const { sessionId } = useParams<{ sessionId: string }>();
	const [session, setSession] = useState<
		SessionType.Session | undefined | null
	>(undefined);
	const authUser = useAppSelector(selectAuthUser);

	useEffect(() => {
		if (!sessionId) {
			return;
		}
		console.log(sessionId);
		const getSession = async () => {
			const res = await getSessionById(sessionId);
			setSession(res);
		};
		getSession();
	}, [sessionId]);

	console.log(session, authUser);

	return (
		<IonPage>
			<Header />
			<IonContent className="fluid">
				{session === undefined || !authUser ? (
					<>
						<IonCard className="ion-text-center" color={"dark"}>
							<IonCardContent>
								<IonLabel>
									<h3>Loading...</h3>
								</IonLabel>
								<IonSpinner name="dots" />
							</IonCardContent>
						</IonCard>
					</>
				) : !session ? null : (
						<SessionDetailsContent
							session={session}
							authUser={authUser}
						/>
				)}
			</IonContent>
		</IonPage>
	);
};

export default SessionDetails;
