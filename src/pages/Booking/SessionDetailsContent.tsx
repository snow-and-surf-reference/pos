import {
	IonButton,
	IonButtons,
	IonCard,
	IonCardContent,
	IonIcon,
	IonItem,
	IonLabel,
	IonList,
} from "@ionic/react";
import * as SessionType from "@tsanghoilun/snow-n-surf-interface/types/session";
import { Customer } from "@tsanghoilun/snow-n-surf-interface/types/user";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import { printOutline, shareSocialOutline } from "ionicons/icons";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

interface Props {
	session: SessionType.Session | undefined | null;
	authUser: Customer | null;
}

const SessionDetailsContent: React.FC<Props> = (props) => {
	const { t } = useTranslation();
	const { session, authUser } = props;
	const history = useHistory();

	return !session || !authUser ? null : (
		<>
			<IonCard>
				<IonCardContent>
					<div className="ion-margin ion-text-center">
						<p>{t(`Order# (Use This to Check In!)`)}</p>
						<div className="large-order-no">{session.bookingNumber}</div>
					</div>

					<IonLabel className="ion-margin ion-text-center">
						<p>
							{t(`Thank You for your booking! Please see below for the details of your order.`)}
							<br />
							{t(`An Email has also been sent to`)}{" "}
							<IonLabel color="tertiary">
								<b>{authUser.email}</b>
							</IonLabel>{" "}
							{t(`containing the booking, order and payment details.`)}
						</p>
					</IonLabel>
					<br />
					<IonList lines="none">
						<IonItem>
							<IonLabel>
								<p>{t(`Order Number`)}</p>
							</IonLabel>
							<IonLabel slot="end">
								<h3>
									<b>{session.bookingNumber}</b>
								</h3>
							</IonLabel>
						</IonItem>
						<IonItem>
							<IonLabel>
								<p>{t(`Order Time`)}</p>
							</IonLabel>
							<IonLabel slot="end">
								<h3>
									<b>
										{dayjs
											.tz(Number(session.confirmedAt))
											.format(`DD MMM YYYY, hh:mmA`)}
									</b>
								</h3>
							</IonLabel>
						</IonItem>
						<IonItem>
							<IonLabel>
								<p>{t(`booking`)}</p>
							</IonLabel>
							<IonLabel slot="end" className="ion-text-end">
								<h3>
									{session.playType === "ski"
										? t(`Ski`)
										: session.playType === 'sb' ? t(`sb`)
											: session.playType === 'snow' ? t(`skiAndSnowboard`)
												: session.playType === 'all' ? t(`snowNSurf`)
										: t(`surf`)}
								</h3>
								<h4>
									{dayjs.tz(Number(session.start)).format(`DD MMM YY, hh:mmA`)}
								</h4>
								<h4>
									{Math.abs(
										dayjs
											.tz(Number(session.start))
											.diff(dayjs.tz(Number(session.end)), "hours")
									)}{" "}
									{t(`hours`)}
								</h4>
							</IonLabel>
						</IonItem>
						<IonItem>
							<IonLabel>
								<p>{t(`Payment`)}</p>
							</IonLabel>
							<IonLabel slot="end" className="ion-text-end">
								<h3>{session.totalCredits.toLocaleString()} {t(`credits`)}</h3>
							</IonLabel>
						</IonItem>
						<br />
						<IonItem>
							<IonButtons slot="end">
								<IonButton color="warning">
									<IonIcon icon={printOutline} slot="icon-only" />
								</IonButton>
								<IonButton color="secondary">
									<IonIcon icon={shareSocialOutline} slot="icon-only" />
								</IonButton>
							</IonButtons>
						</IonItem>
						<br />
						<IonButton
							color={`light`}
							expand="block"
							onClick={() => history.push(`/`)}
						>
							{t(`Back To Home`)}
						</IonButton>
					</IonList>
				</IonCardContent>
			</IonCard>
		</>
	);
};

export default SessionDetailsContent;
