import {
	IonAlert,
	IonButton,
	IonCard,
	IonCardContent,
	IonCardHeader,
	IonCardSubtitle,
	IonCardTitle,
	IonCheckbox,
	IonChip,
	IonCol,
	IonContent,
	IonFab,
	IonGrid,
	IonIcon,
	IonItem,
	IonLabel,
	IonList,
	IonListHeader,
	IonLoading,
	IonPage,
	IonRow,
	IonSpinner,
} from "@ionic/react";
import * as SessionType from "@tsanghoilun/snow-n-surf-interface/types/session";
import * as TopupTypes from "@tsanghoilun/snow-n-surf-interface/types/topUp";
import dayjs from "dayjs";
import duration from "dayjs/plugin/duration";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import {
	alertCircleOutline,
	arrowForwardCircleOutline,
	checkmarkCircle,
	closeCircleOutline,
	diamondOutline,
	ellipseOutline,
	ticketOutline,
	timeOutline,
	walletOutline,
} from "ionicons/icons";
import { useCallback, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useHistory, useParams } from "react-router";
import {
	cancelLockedSession,
	getSessionById,
	paySession,
} from "../../app/firebase/global";
import { useAppSelector } from "../../app/hooks";
import useBreakpoints from "../../app/hooks/useBreakpoints";
import { selectAuthUser } from "../../app/slices/authUserSlice";
import { selectTopupPackages } from "../../app/slices/topupPackages";
import Header from "../../components/Global/Header";
import PaymentFlexForm from "../../components/Payments/PaymentFlexForm";
import TopupFailedModal from "../../components/Topup/TopupFailedModal";
import TopupPackageSelection from "../../components/Topup/TopupPackageSelection";
import TopupTermsModal from "../../components/Topup/TopupTermsModal";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);
dayjs.extend(duration);

const NullSessionUI: React.FC = () => {
	const { t } = useTranslation();
	const history = useHistory();

	return (
		<IonCard className="ion-text-center" color={`dark`}>
			<IonCardHeader>
				<IonCardTitle>{t(`Oops!`)}</IonCardTitle>
			</IonCardHeader>
			<IonCardContent>
				<IonLabel>
					<br />
					<h3>
						<b>
							{t(
								`Looks like the session doesn't exist, or it is already expired.`
							)}
						</b>
					</h3>
					<br />
				</IonLabel>
				<IonButton
					color={"warning"}
					onClick={() => {
						history.push(`/newBooking/single`);
					}}
				>
					{t(`Start a New Booking`)}
				</IonButton>
			</IonCardContent>
		</IonCard>
	);
};
export interface PaymentFields {
	name: string;
	exp: string;
	address1: string;
	address2: string;
	country: string;
}

const Checkout: React.FC = () => {
	const { t } = useTranslation();
	const { sessionId } = useParams<{ sessionId: string }>();
	const [session, setSession] = useState<
		SessionType.Session | undefined | null
	>(undefined);
	const authUser = useAppSelector(selectAuthUser);
	const [paying, setPaying] = useState(false);
	const [disable, setDisable] = useState(true);
	const history = useHistory();
	const [timer, setTimer] = useState<string | null>(null);
	const [cancelTime, setCancelTime] = useState<number | null>(null);
	const { isSm, isMd, isXs } = useBreakpoints();
	const [showCancelAlert, setShowCancelAlert] = useState(false);
	const [showLoading, setShowLoading] = useState(false);
	const topupPackages = useAppSelector(selectTopupPackages);
	const [selectedPackage, setSelectedPackage] =
		useState<TopupTypes.TopUpPackages | null>(null);
	const [mostValuedPackage, setMostValuedPackage] =
		useState<TopupTypes.TopUpPackages | null>(null);
	const [paymentFields, setPaymentFields] = useState<PaymentFields>({
		name: "",
		exp: "",
		address1: "",
		address2: "",
		country: "HK",
	});
	const [disableTopupPay, setDisableTopupPay] = useState(true);
	const [termsAgreed, setTermsAgreed] = useState(false);
	const [showTermsModal, setShowTermsModal] = useState(false);
	const [toppingUp, setToppingUp] = useState(false);
	const [showPaymentFailedModal, setShowPaymentFailedModal] = useState(false);
	const [topupExact, setTopupExact] = useState(false);

	const goToOrderDetails = useCallback(
		(id: string) => {
			history.replace(`/sessionDetails/${id}`);
		},
		[history]
	);

	useEffect(() => {
		if (!sessionId) {
			return;
		}
		const getSession = async () => {
			const res = await getSessionById(sessionId);
			setSession(res);
			if (res && res.status === `confirmed`) {
				goToOrderDetails(res.id);
			}
			if (res && res.lockedAt && res.lockedAt.valueOf() > Date.now()) {
				setCancelTime(dayjs(res.lockedAt).valueOf());
			}
		};
		getSession();
	}, [sessionId, goToOrderDetails]);

	useEffect(() => {
		if (!cancelTime) {
			return;
		}
		setInterval(() => {
			setTimer(dayjs.duration(dayjs(cancelTime).diff(dayjs())).format(`mm:ss`));
		}, 1000);
	}, [cancelTime]);

	const refreshPage = useCallback(() => {
		history.go(0);
	}, [history]);

	useEffect(() => {
		if (!cancelTime || !timer) {
			return;
		}
		const timeLeft = dayjs
			.duration(dayjs(cancelTime).diff(dayjs()))
			.asSeconds();
		if (timeLeft <= 0) {
			refreshPage();
		}
	}, [timer, cancelTime, refreshPage]);

	useEffect(() => {
		if (!authUser || !session) {
			setDisable(true);
		} else {
			setDisable(false);
		}
	}, [session, authUser]);

	useEffect(() => {
		if (!session) {
			return;
		}
	}, [session]);

	// check can topup and pay or not
	useEffect(() => {
		if (selectedPackage) {
			setTopupExact(false);
		}

		const nameValid = paymentFields.name !== "";
		const expValid = /([0]{1}[1-9]{1})|([1]{1}[0-2]{1})\/[0-9]{2}/g.test(
			paymentFields.exp
		);
		const addressValid = paymentFields.address1 !== "";
		if (
			nameValid &&
			expValid &&
			addressValid &&
			(!!selectedPackage || topupExact) &&
			!!authUser &&
			termsAgreed
		) {
			setDisableTopupPay(false);
		} else {
			setDisableTopupPay(true);
		}
	}, [paymentFields, selectedPackage, authUser, termsAgreed, topupExact]);

	// update most value package
	useEffect(() => {
		if (topupPackages.length < 2) {
			return;
		}
		const mostValued = [...topupPackages].sort((a, b) => {
			const aValue = a.receivedCredits - a.initCredits;
			const bValue = b.receivedCredits - b.initCredits;
			if (aValue > bValue) {
				return -1;
			} else if (bValue > aValue) {
				return 1;
			} else {
				return 0;
			}
		})[0];
		setMostValuedPackage(mostValued);
	}, [topupPackages]);

	// topupPay
	const handleTopupPay = async () => {
		if ((!selectedPackage && !topupExact) || !authUser) {
			return;
		}

		setToppingUp(true);
		console.clear();
		// ready payload
		const packageId = selectedPackage?.id || "";
		let payload = {
			...paymentFields,
			packageId,
			authUser,
			topupExact,
			sessionId: session?.id,
		};

		const res = await (window as any).attemptPay(payload);
		if (res && res.result === "AUTHORIZED") {
			console.log("Payment Success", res);
			setToppingUp(false);
			//Do Book Session
			handlePay();
		} else {
			console.log("Payment Not Authorised", res);
			setToppingUp(false);
			// show error alert
			setShowPaymentFailedModal(true);
			return;
		}
	};

	const handlePay = async () => {
		setPaying(true);
		if (!authUser || !session) {
			return;
		}
		const customerId = authUser.id;
		const res = await paySession(session, customerId);
		if (res === "success") {
			setPaying(false);
			history.push(`/orderCompleted/${session.id}`);
		} else {
			setPaying(false);
		}
	};

	return (
		<IonPage>
			<Header />
			<IonContent className="fluid">
				<IonCardHeader className="ion-text-center">
					<IonCardTitle>{t(`checkout`)}</IonCardTitle>
				</IonCardHeader>
				{session === undefined ? (
					<IonCard color={`dark`}>
						<IonCardContent className="ion-text-center">
							<IonLabel>
								<h3>
									<b>{t(`Loading Your Order`)}</b>
								</h3>
							</IonLabel>
							<IonSpinner name="dots" />
						</IonCardContent>
					</IonCard>
				) : session === null ||
				  authUser === null ||
				  Number(session.lockedAt) < Date.now() ? (
					<NullSessionUI />
				) : (
					<>
						<IonCard>
							<IonCardHeader>
								<IonCardTitle>
									<b>{t(`Order Details`)}</b>
								</IonCardTitle>
							</IonCardHeader>
							<IonCardContent>
								<IonList className="ion-no-padding">
									<IonItem className="ion-no-padding" lines="none">
										<IonLabel>
											<h3>
												{session.pax} x {t(`parkEntrance`)}
											</h3>
											<p>
												{dayjs.tz(session.start).format(`DD MMM YYYY, HH:mm`)} -{" "}
												{dayjs.tz(session.end).format(`HH:mm`)}
												{` (${dayjs
													.tz(session.end)
													.diff(dayjs.tz(session.start), "hours")} ${
													dayjs
														.tz(session.end)
														.diff(dayjs.tz(session.start), "hours") > 1
														? t(`hours`)
														: t(`hour`)
												})`}
											</p>
										</IonLabel>
										<IonChip color="dark">
											<IonIcon icon={diamondOutline} size="small" />
											<IonLabel>
												<h3>
													<b>{session.subTotals.entrances.toLocaleString()}</b>
												</h3>
											</IonLabel>
										</IonChip>
									</IonItem>
									{session.subTotals.gears > 0 ? (
										<IonItem className="ion-no-padding" lines="none">
											<IonLabel>
												<h3>
													{session.gears.length} x {t(`gearRental`)}
												</h3>
											</IonLabel>
											<IonChip color="dark">
												<IonIcon icon={diamondOutline} size="small" />
												<IonLabel>
													<h3>
														<b>{session.subTotals.gears.toLocaleString()}</b>
													</h3>
												</IonLabel>
											</IonChip>
										</IonItem>
									) : null}

									{session.subTotals.coach > 0 ? (
										<IonItem className="ion-no-padding" lines="none">
											<IonLabel>
												<h3>{t(`Coaching Fee`)}</h3>
												<p>
													{session.sessionType === "groupClass"
														? t(`groupClass`)
														: t(`privateTraining`)}
												</p>
											</IonLabel>
											<IonChip color="dark">
												<IonIcon icon={diamondOutline} size="small" />
												<IonLabel>
													<h3>
														<b>{session.subTotals.coach.toLocaleString()}</b>
													</h3>
												</IonLabel>
											</IonChip>
										</IonItem>
									) : null}

									{session.subTotals.privateHire > 0 ? (
										<IonItem className="ion-no-padding" lines="none">
											<IonLabel>
												<h3>
													{session.sessionType === "privateBelt"
														? t(`privateSnowBelt`)
														: session.sessionType === "privateLane"
														? t(`privateSurfLane`)
														: session.sessionType === "fullSnow"
														? t(`Private Snow Venue`)
														: session.sessionType === "fullSurf"
														? t(`Private Surf Venue`)
														: t(`Private Park Hire`)}
												</h3>
												<p>
													{" "}
													{dayjs(session.end).diff(
														dayjs(session.start),
														"hours"
													)}{" "}
													{dayjs(session.end).diff(
														dayjs(session.start),
														"hours"
													) > 1
														? t(`hours`)
														: t(`hour`)}
												</p>
											</IonLabel>
											<IonChip color="dark">
												<IonIcon icon={diamondOutline} size="small" />
												<IonLabel>
													<h3>
														<b>
															{session.subTotals.privateHire.toLocaleString()}
														</b>
													</h3>
												</IonLabel>
											</IonChip>
										</IonItem>
									) : null}

									{session.memberDiscount && !session.usedCoupon ? (
										<IonItem className="ion-no-padding" lines="none">
											<IonLabel>
												<h3>{t(`Member Discount`)}</h3>
											</IonLabel>
											<IonChip color="danger">
												<IonIcon icon={diamondOutline} size="small" />
												<IonLabel>
													<h3>
														<b>
															{Math.ceil(
																Number(
																	0 -
																		Object.values(session.subTotals).reduce(
																			(p, c) => p + c,
																			0
																		) *
																			session.memberDiscount
																)
															)}
														</b>
													</h3>
												</IonLabel>
											</IonChip>
												</IonItem>
											
									) : null}

									{session.usedCoupon ? (
										<>
											<IonItem className="ion-no-padding" lines="none">
												<IonLabel>
													<h3>{t(`coupon`)}</h3>
													<p>{session.usedCoupon.code}</p>
												</IonLabel>
												<IonChip color="danger">
													<IonIcon icon={diamondOutline} size="small" />
													<IonLabel>
														<h3>
															<b>
																-
																{session.usedCoupon.unit === "credit"
																	? session.usedCoupon.discount
																	: Math.ceil(
																			Object.values(session.subTotals).reduce(
																				(a, b) => a + b,
																				0
																			) *
																				(session.usedCoupon.discount / 100)
																	  )}
															</b>
														</h3>
													</IonLabel>
												</IonChip>
											</IonItem>
										</>
									) : null}
								</IonList>
							</IonCardContent>
						</IonCard>
						{authUser.wallet.balance < session.totalCredits ? (
							<>
								<IonCard color={"warning"}>
									<IonCardHeader>
										<IonCardTitle>
											<b>{t(`Top Up & Pay`)}</b>
										</IonCardTitle>
										<IonCardSubtitle>
											<IonLabel color={"tertiary"}>
												{t(
													`You do not have enough credits in your wallet, please choose a topup package to topup + PAY for your booking.`
												)}
											</IonLabel>
										</IonCardSubtitle>
									</IonCardHeader>
									<IonCardContent className="ion-no-padding">
										<IonList inset>
											<IonListHeader>{t(`orderTotal`)}:</IonListHeader>
											<IonItem>
												<IonIcon
													icon={diamondOutline}
													size="small"
													slot="start"
												/>
												<IonLabel>
													<h1>
														<b>{session.totalCredits.toLocaleString()}</b>
													</h1>
												</IonLabel>
											</IonItem>
										</IonList>
										<IonList inset color="dark">
											<IonListHeader color={"dark"}>
												<IonIcon icon={walletOutline} size="large" />
												{` ${t(`My Wallet`)}`}
											</IonListHeader>
											<IonItem color={"dark"}>
												<IonIcon
													icon={diamondOutline}
													size="small"
													slot="start"
												/>
												<IonLabel>
													<h1>
														<b>{authUser.wallet.balance.toLocaleString()}</b>
													</h1>
													{session.totalCredits < authUser.wallet.balance && (
														<IonLabel color="warning">
															<p>
																{t(`Afterward`)}
																{": "}
																{(
																	authUser.wallet.balance - session.totalCredits
																).toLocaleString()}
															</p>
														</IonLabel>
													)}
												</IonLabel>
											</IonItem>
										</IonList>
									</IonCardContent>
								</IonCard>

								<TopupPackageSelection
									topupPackages={topupPackages}
									selectedPackage={selectedPackage}
									mostValuedPackage={mostValuedPackage}
									setSelectedPackage={setSelectedPackage}
									minimumToOrder={
										session.totalCredits - authUser.wallet.balance
									}
								/>

								<IonCard color={topupExact ? `warning` : `white`}>
									<IonItem
										lines="none"
										color={topupExact ? `warning` : `white`}
										onClick={() => {
											// handleChange()
											setSelectedPackage(null);
											setTopupExact(true);
										}}
										style={{
											cursor: "pointer",
										}}
									>
										<IonIcon
											icon={diamondOutline}
											slot="start"
											color="tertiary"
										/>
										<IonLabel>
											<h3>
												<b>{t(`Or Topup Exact Outstanding Amount`)}</b>
											</h3>
											<h1>
												<b>{session.totalCredits - authUser.wallet.balance}</b>
											</h1>
											<p>HKD $1 = 1 {t(`credits`)}</p>
										</IonLabel>
										<IonIcon
											icon={topupExact ? checkmarkCircle : ellipseOutline}
											slot="end"
											color={topupExact ? "dark" : "dark"}
										/>
									</IonItem>
								</IonCard>

								<PaymentFlexForm
									fields={paymentFields}
									setFields={(newFields) => {
										setPaymentFields(newFields);
									}}
								/>

								<IonCard
									color={termsAgreed ? "warning" : "light"}
									style={{
										marginBottom: "4rem",
									}}
								>
									<IonItem
										color={termsAgreed ? "warning" : "light"}
										lines="none"
									>
										<IonCheckbox
											checked={termsAgreed}
											onIonChange={(e) => {
												setTermsAgreed(e.detail.checked);
											}}
											slot="start"
										/>
										<IonLabel>
											<h3>{t(`I agree to the Terms & Conditions`)}</h3>
										</IonLabel>
										<IonButton
											onClick={(e) => {
												e.preventDefault();
												e.stopPropagation();
												setShowTermsModal(true);
											}}
										>{`View`}</IonButton>
									</IonItem>
								</IonCard>

								<IonGrid>
									<IonRow>
										<IonCol className="ion-text-center ion-padding">
											<IonButton
												color="tertiary"
												size="large"
												disabled={disableTopupPay}
												onClick={() => {
													handleTopupPay();
												}}
											>
												<IonIcon icon={ticketOutline} slot="start" />
												<IonLabel>
													<h1>
														<b>{t(`Top Up & Pay`)}</b>
													</h1>
												</IonLabel>
											</IonButton>
										</IonCol>
									</IonRow>
								</IonGrid>
							</>
						) : (
							<IonCard color={`warning`}>
								<IonCardHeader>
									<IonCardTitle>
										<b>{t(`Payment`)}</b>
									</IonCardTitle>
									<IonCardSubtitle>
										<IonLabel color={"tertiary"}>
											{t(`Pay By Credits`)}
										</IonLabel>
									</IonCardSubtitle>
								</IonCardHeader>
								<IonCardContent className="ion-no-padding">
									<IonGrid className="ion-no-padding">
										<IonRow>
											<IonCol>
												<IonList inset color="dark">
													<IonListHeader color={"dark"}>
														<IonIcon icon={walletOutline} size="large" />
														&nbsp;{t(`My Wallet`)}
													</IonListHeader>
													<IonItem color={"dark"}>
														<IonIcon
															icon={diamondOutline}
															size="small"
															slot="start"
														/>
														<IonLabel>
															<h1>
																<b>
																	{authUser.wallet.balance.toLocaleString()}
																</b>
															</h1>
															{session.totalCredits <
																authUser.wallet.balance && (
																<IonLabel color="warning">
																	<p>
																		{t(`Afterward`)}:{" "}
																		{(
																			authUser.wallet.balance -
																			session.totalCredits
																		).toLocaleString()}
																	</p>
																</IonLabel>
															)}
														</IonLabel>
													</IonItem>
												</IonList>
												{session.totalCredits > authUser.wallet.balance ? (
													<IonItem color={"warning"} lines="none">
														<IonIcon
															icon={alertCircleOutline}
															slot="start"
															color="danger"
														/>
														<IonLabel color="danger">
															<p>You Do not have enough credits</p>
														</IonLabel>
													</IonItem>
												) : null}
											</IonCol>
											<IonCol
												sizeXs="12"
												sizeSm="12"
												sizeMd="1"
												sizeLg="1"
												sizeXl="1"
												class="ion-justify-content-center ion-align-items-center"
												style={{ display: "flex" }}
											>
												<IonIcon
													icon={arrowForwardCircleOutline}
													size="large"
												/>
											</IonCol>
											<IonCol>
												<IonList inset>
													<IonListHeader>{t(`orderTotal`)}</IonListHeader>
													<IonItem>
														<IonIcon
															icon={diamondOutline}
															size="small"
															slot="start"
														/>
														<IonLabel>
															<h1>
																<b>{session.totalCredits.toLocaleString()}</b>
															</h1>
														</IonLabel>
													</IonItem>
												</IonList>
											</IonCol>
										</IonRow>
										<IonRow>
											<IonCol className="ion-text-center ion-padding">
												<IonButton
													color="tertiary"
													size="large"
													disabled={
														session.totalCredits > authUser.wallet.balance ||
														paying ||
														disable
													}
													onClick={() => handlePay()}
												>
													{paying ? (
														<>
															<IonSpinner />
														</>
													) : (
														<>
															<IonIcon icon={ticketOutline} slot="start" />
															<IonLabel>
																<h1>
																	<b>{t(`PAY NOW`)}</b>
																</h1>
															</IonLabel>
														</>
													)}
												</IonButton>
											</IonCol>
										</IonRow>
									</IonGrid>
								</IonCardContent>
							</IonCard>
						)}

						<IonGrid className="ion-text-end">
							<IonRow>
								<IonCol>
									{isXs || isSm || isMd ? (
										<IonButton
											color={`light`}
											fill="outline"
											expand="block"
											onClick={() => setShowCancelAlert(true)}
										>
											<IonIcon
												icon={closeCircleOutline}
												slot="start"
												color="medium"
											/>
											<IonLabel color={`medium`}>
												<h3>
													<b>{t(`Cancel Booking`)}</b>
												</h3>
											</IonLabel>
										</IonButton>
									) : (
										<IonButton
											color={`light`}
											fill="outline"
											onClick={() => setShowCancelAlert(true)}
										>
											<IonIcon
												icon={closeCircleOutline}
												slot="start"
												color="medium"
											/>
											<IonLabel color={`medium`}>
												<h3>
													<b>{t(`Cancel Booking`)}</b>
												</h3>
											</IonLabel>
										</IonButton>
									)}
								</IonCol>
							</IonRow>
						</IonGrid>

						{timer && (
							<IonFab vertical="top" horizontal="end" slot="fixed">
								<IonChip className="solid-bg-chip">
									<IonIcon icon={timeOutline} />
									<IonLabel>{timer}</IonLabel>
								</IonChip>
							</IonFab>
						)}
					</>
				)}
			</IonContent>
			<IonAlert
				isOpen={showCancelAlert}
				onDidDismiss={() => {
					setShowCancelAlert(false);
				}}
				header={`${t(`Cancel Booking`)}`}
				message={`${t(`Are you sure?`)}`}
				buttons={[
					{
						text: `${t(`confirm`)}`,
						role: "confirm",
						handler: async () => {
							setShowLoading(true);
							if (session) {
								const res = await cancelLockedSession(session);
								if (res === "success") {
									setShowLoading(false);
									setShowCancelAlert(false);
									history.push(`/newBooking`);
								}
							} else {
								setShowLoading(false);
								setShowCancelAlert(false);
								history.push(`/newBooking`);
							}
						},
					},
					{
						text: `${t(`cancel`)}`,
						role: "cancel",
					},
				]}
			/>
			<IonLoading
				isOpen={toppingUp}
				message={t(`Topping Up. DO NOT REFRESH!`)}
			/>
			<IonLoading
				isOpen={paying}
				message={t(`Booking Your Session. DO NOT REFRESH!`)}
			/>
			<IonLoading isOpen={showLoading} message={t(`Cancelling Your Booking`)} />

			<TopupTermsModal
				showTermsModal={showTermsModal}
				setShowTermsModal={setShowTermsModal}
			/>

			<TopupFailedModal
				showPaymentFailedModal={showPaymentFailedModal}
				setShowPaymentFailedModal={setShowPaymentFailedModal}
			/>
		</IonPage>
	);
};

export default Checkout;
