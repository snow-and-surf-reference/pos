import { IonCard, IonCardContent, IonChip, IonCol, IonContent, IonGrid, IonItem, IonLabel, IonList, IonPage, IonRow } from "@ionic/react"
import Header from "../../components/Global/Header";
import * as SessionTypes from '@tsanghoilun/snow-n-surf-interface/types/session'
import { useAppSelector } from "../../app/hooks";
import { selectSessions } from "../../app/slices/sessionsSclice";
import dayjs from "dayjs";
import utc from 'dayjs/plugin/utc';
import timezone from "dayjs/plugin/timezone";
import { useHistory } from "react-router";
import { useTranslation } from "react-i18next";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);


const MyBookings: React.FC = () => {
    const { t } = useTranslation();
    const sessions: SessionTypes.Session[] = useAppSelector(selectSessions);
    const history = useHistory();
    return (
        <IonPage>
            <Header />
            <IonContent className="fluid">
                <IonGrid>
                    <IonRow><IonCol></IonCol></IonRow>
                    <IonRow>
                        <IonCol>
                            <IonLabel><b>{t(`myBookings`)}</b></IonLabel>
                        </IonCol>
                    </IonRow>
                </IonGrid>
                <IonCard color={`light`}>
                    <IonCardContent>
                        <IonList color={`light`}>
                            {
                                sessions.map(s => (
                                    <IonItem key={`session-${s.id}`}
                                        color={`light`}
                                        button={s.status === 'confirmed' || s.status === 'locked'}
                                        onClick={() =>
                                            s.status === 'locked' ?
                                                history.push(`/checkout/${s.id}`)
                                                :
                                                s.status === 'confirmed' ?
                                                    history.push(`/sessionDetails/${s.id}`)
                                                    : {}
                                        }
                                    >
                                        <IonChip
                                            outline
                                            color={`primary`}
                                            slot='end'
                                            className="ion-hide-md-down"
                                        >{s.status === 'locked' ? t(`Pending Checkout`) : t(s.status)}</IonChip>
                                        <IonLabel>
                                            <h2><b>{dayjs.tz(Number(s.start)).format(`DD MMM YY`)}</b></h2>
                                            <h3><b>{dayjs.tz(Number(s.start)).format(`hh:mmA`)}-{dayjs.tz(Number(s.end)).format(`hh:mmA`)}</b></h3>
                                            {s.status === 'confirmed' ?
                                                <><br /><p>{t(`booking`)} # {s.bookingNumber}</p></>
                                                :
                                                <><br /><p>{s.status === 'locked' ? t(`Pending Checkout`) : t(s.status)}</p></>
                                            }
                                        </IonLabel>
                                    </IonItem>
                                ))
                            }
                        </IonList>
                    </IonCardContent>
                </IonCard>
            </IonContent>
        </IonPage>
    )
}

export default MyBookings;
