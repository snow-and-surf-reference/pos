import {
	IonAlert,
	IonButton,
	IonButtons,
	IonCard,
	IonCardContent,
	IonCardHeader,
	IonCardTitle,
	IonChip,
	IonCol,
	IonContent,
	IonGrid,
	IonIcon,
	IonInput,
	IonItem,
	IonLabel,
	IonList,
	IonModal,
	IonPage,
	IonRow,
	IonSpinner
} from "@ionic/react";
import * as SessionType from "@tsanghoilun/snow-n-surf-interface/types/session";
import * as UserTypes from "@tsanghoilun/snow-n-surf-interface/types/user";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import {
	arrowForwardCircle,
	bagAddOutline,
	cartOutline,
	diamondOutline
} from "ionicons/icons";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { MdError } from "react-icons/md";
import { useHistory } from "react-router";
import {
	createLockedSession,
	getCouponByCode
} from "../../app/firebase/global";
import {
	checkCoupon,
	checkCouponParams
} from "../../app/functions/checkCoupon";
import { useAppSelector } from "../../app/hooks";
import { selectAuthUser } from "../../app/slices/authUserSlice";
import { selectCustomerClasses } from "../../app/slices/customerClassesSlice";
import { selectPricing } from "../../app/slices/pricingSlice";
import {
	clearTempSession,
	selectTempSession,
	setTempSession
} from "../../app/slices/sessionsSclice";
import { store } from "../../app/store";
import CenteredContent from "../../components/Global/CenteredContent";
import Header from "../../components/Global/Header";
import i18n from "../../i18n";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

const OrderPreview: React.FC = () => {
	const { t } = useTranslation();
	const authUser: UserTypes.Customer | null = useAppSelector(selectAuthUser);
	const session = useAppSelector<SessionType.Session>(selectTempSession);
	const history = useHistory();
	const [loading, setLoading] = useState(false);
	const [showCancelAlert, setShowCancelAlert] = useState(false);
	const [showFailModal, setShowFailModal] = useState(false);
	const [failMessage, setFailMessage] = useState<string | null>(null);
	const classes: UserTypes.CustomerClass[] = useAppSelector(
		selectCustomerClasses
	);
	const [currentClass, setCurrentClass] =
		useState<UserTypes.CustomerClass | null>(null);
	const [isPartner, setIsPartner] = useState(false);

	// states for coupons
	const [couponCode, setCouponCode] = useState("");
	const [couponStatus, setCouponStatus] = useState("idle");
	const [appliedCoupon, setAppliedCoupon] = useState<null | SessionType.Coupon>(
		null
	);
	const [couponError, setCouponError] = useState("");
	const [disabledSubmit, setDisabledSubmit] = useState(true);

	// get global pricing
	const prices = useAppSelector(selectPricing);

	useEffect(() => {
		if (!session || !authUser || !session.start || !session.end) {
			setDisabledSubmit(true);
		} else {
			setDisabledSubmit(false);
		}
	}, [session, authUser]);

	useEffect(() => {
		if (!authUser || classes.length <= 0) {
			return;
		}
		const tempClasses: UserTypes.CustomerClass[] = classes
			.filter((x) => x.creditTier <= authUser.wallet.spent)
			.sort((a, b) => a.creditTier - b.creditTier);
		const highestClass: UserTypes.CustomerClass =
			tempClasses[tempClasses.length - 1];
		// console.log(highestClass);
		setCurrentClass({ ...highestClass });
		setIsPartner(authUser.isPartner ? true : false);
	}, [classes, authUser]);

	// effect to update discounts from customer class
	useEffect(() => {
		if (!session.totalCredits) {
			return;
		}
		if (!currentClass) {
			return;
		}
		if (session.memberDiscount !== null) {
			return;
		}
		const tempSession = Object.assign(
			{ ...session },
			{
				memberDiscount: currentClass.discount,
				totalCredits: Math.ceil(
					session.totalCredits * (1 - currentClass.discount)
				),
			}
		);

		store.dispatch(setTempSession(tempSession));
	}, [currentClass, session]);

	const handleSubmit = async () => {
		setLoading(true);
		const res = await createLockedSession(session);
		setLoading(false);
		if (!res || !res.sessionId) {
			setShowFailModal(true);
			res.message && setFailMessage(res.message);
			return;
		}
		history.push(`/checkout/${res.sessionId}`);
	};

	// Effect to update discount from applied coupon
	useEffect(() => {
		if (!session.totalCredits) {
			return;
		}
		if (appliedCoupon) {
			let tempSession = { ...session };
			// get current subtotals
			const rawTotals = Object.values(tempSession.subTotals).reduce(
				(a, b) => a + b,
				0
			);
			const couponDiscount = !appliedCoupon
				? 0
				: appliedCoupon.unit === "credit"
					? appliedCoupon.discount
					: Math.floor(rawTotals * (appliedCoupon.discount / 100));
			const couponedTotal = rawTotals - couponDiscount;
			if (couponedTotal === tempSession.totalCredits) {
				return;
			}

			Object.assign(tempSession, {
				totalCredits: couponedTotal,
				usedCoupon: appliedCoupon,
			});

			store.dispatch(setTempSession(tempSession));
		} else {
			let tempSession = { ...session };
			const rawTotals = Object.values(tempSession.subTotals).reduce(
				(a, b) => a + b,
				0
			);
			if (isPartner) {
				const partnerPrice = Math.floor(rawTotals * 0.1);
				if (partnerPrice !== tempSession.totalCredits) {
					Object.assign(tempSession, { totalCredits: partnerPrice });
					store.dispatch(setTempSession(tempSession));
				}
				return;
			}
			if (currentClass) {
				const classPrice = Math.floor(rawTotals * (1 - currentClass.discount));
				if (classPrice !== tempSession.totalCredits) {
					Object.assign(tempSession, { totalCredits: classPrice });
					store.dispatch(setTempSession(tempSession));
				}
			}
		}
	}, [session, appliedCoupon, currentClass, isPartner]);

	const applyCoupon = async () => {
		if (!couponCode) {
			return;
		}
		setCouponStatus("applying");
		const res = await getCouponByCode(couponCode);
		if (res) {
			// check coupon before applying
			// console.log(res);
			if (!authUser) {
				return;
			}
			const params: checkCouponParams = {
				coupon: res,
				authUser: authUser,
				session,
			};
			const isValid = checkCoupon(params);
			if (isValid && isValid.result) {
				// apply coupon
				setAppliedCoupon(res);
				setCouponCode("");
			} else {
				console.log(isValid.message);
				setCouponError(isValid.message);
			}
		} else {
			// no coupon found, show error message
		}
		setCouponStatus("idle");
	};

	return (
		<IonPage>
			<Header />
			<IonContent className="fluid">
				<br />
				<div color="transparent" className="ion-text-center">
					<IonCardHeader>
						<IonCardTitle>{t(`Confirm Your Order`)}</IonCardTitle>
					</IonCardHeader>
				</div>
				<IonGrid>
					<IonRow>
						<IonCol sizeLg="8" sizeMd="12" sizeSm="12" sizeXs="12">
							<IonCard>
								<IonCardContent>
									<IonItem lines="none">
										<IonLabel>
											<h2>
												<b>{t(`Your Session`)}</b>
											</h2>
										</IonLabel>
									</IonItem>
									{
										disabledSubmit ?
											<IonGrid>
												<IonRow>
													<IonCol>
														<IonLabel>
															<h3>The session details no long available, please cancel the booking and restart</h3>
														</IonLabel>
													</IonCol>
												</IonRow>
											</IonGrid>
											:
											<IonList inset>
												<IonItem>
													<IonLabel>
														<p>{t(`date`)}</p>
													</IonLabel>
													<IonLabel slot="end">
														<h3>
															<b>
																{dayjs
																	.tz(session.sessionDate)
																	.format(`DD MMM YYYY`)}
															</b>
														</h3>
													</IonLabel>
												</IonItem>
												<IonItem>
													<IonLabel>
														<p>{t(`time`)}</p>
													</IonLabel>
													<IonLabel slot="end">
														<h3>
															<b>{`${dayjs
																.tz(session.start)
																.format("HH:mm")} - ${dayjs
																	.tz(session.end)
																	.format("HH:mm")}`}</b>
														</h3>
													</IonLabel>
												</IonItem>
												<IonItem>
													<IonLabel>
														<p>{t(`type`)}</p>
													</IonLabel>
													<IonLabel slot="end">
														<h3>
															<b>
																{session.playType === `ski` ||
																	session.playType === `sb` ||
																	session.playType === `snow`
																	? t(`skiAndSnowboard`)
																	: t(`surf`)}
															</b>
														</h3>
													</IonLabel>
												</IonItem>
												<IonItem>
													<IonLabel>
														<p>{t(`people`)}</p>
													</IonLabel>
													<IonLabel slot="end">
														<h3>
															<b>
																{session.pax}{" "}
																{session.pax > 1 ? t(`persons`) : t(`person`)}
															</b>
														</h3>
													</IonLabel>
												</IonItem>
											</IonList>
									}
									<IonItem lines="none">
										<IonButtons>
											<IonLabel>
												<h2>
													<b>{t(`subtotal`)}: </b>
												</h2>
											</IonLabel>
											<IonChip>
												<IonIcon icon={diamondOutline} />
												<IonLabel>
													<b>{session.subTotals.entrances.toLocaleString()}</b>
												</IonLabel>
											</IonChip>
										</IonButtons>
									</IonItem>
								</IonCardContent>
							</IonCard>
							{session.start && session.end && session.subTotals.gears ? (
								<IonCard>
									<IonCardContent>
										<IonItem lines="none">
											<IonLabel>
												<h2>
													<b>{t(`gearRental`)}</b>
												</h2>
											</IonLabel>
										</IonItem>
										<IonList inset>
											{session.gears.length <= 0 ||
												session.playType === `surf` ? (
												<IonLabel>
													<h2>
														<b>{t(`noGearRented`)}</b>
													</h2>
												</IonLabel>
											) : (
												session.gears.map((gear, index) => (
													<IonItem
														key={`gear-${index}-id-${gear.gear.id}-size-${gear.size}-${gear.class}`}
													>
														<IonLabel>
															<p>
																<b>
																	{gear.gear.name} <br />
																	{gear.class.toUpperCase()}
																	{` `}
																	{`${gear.size} (${gear.gear.unit.name})`}
																</b>
															</p>
														</IonLabel>
													</IonItem>
												))
											)}
										</IonList>
										<IonItem lines="none">
											<IonButtons>
												<IonLabel>
													<h2>
														<b>{t(`subtotal`)}: </b>
													</h2>
												</IonLabel>
												<IonChip>
													<IonIcon icon={diamondOutline} />
													<IonLabel>
														<b>{session.subTotals.gears.toLocaleString()}</b>
													</IonLabel>
												</IonChip>
											</IonButtons>
										</IonItem>
									</IonCardContent>
								</IonCard>
							) : null}

							{session.start && session.end && (session.subTotals.coach || session.groupClass) ? (
								<IonCard id="coach-details">
									<IonCardContent>
										<IonItem lines="none">
											<IonLabel>
												<h2>
													<b>{session.sessionType === 'groupClass' ? t(`groupClass`) : t(`privateTraining`)}</b>
												</h2>
											</IonLabel>
										</IonItem>
										<IonList inset lines="none">
											{session.sessionType === "groupClass" ? (
												<IonItem>
													<IonButtons slot="start">
														<IonLabel slot="start">
															<h3>
																<b>{t(`Group Class Fee`)} - {prices?.group_class.price} {t(`credits`)}</b>
															</h3>
															<p>{t(`per pax / hour`)}</p>
														</IonLabel>
													</IonButtons>
												</IonItem>
											) : session.sessionType === "privateClass" ? (
												<IonItem>
													<IonButtons slot="start">
														<IonLabel slot="start">
															<h3>
																<b>{t(`Private Training Fee`)} - {prices?.personal_training.price} {t(`credits`)}</b>
															</h3>
															<p>{t(`per group / hour`)}</p>
														</IonLabel>
													</IonButtons>
												</IonItem>
											) : (
												<IonItem>
													<IonButtons slot="start">
														<IonLabel slot="start">
															<h3>
																<b>
																	{t(session.sessionType === 'privateBelt' ? `privateSnowBelt` : `privateSurfLane`)} -
																	{session.sessionType === 'privateBelt' ? prices?.default_snow_belt.price : prices?.default_surf_lane.price}
																	{t(`credits`)}
																</b>
															</h3>
															<p>{t(`per hour`)}</p>
														</IonLabel>
													</IonButtons>
												</IonItem>
											)}
										</IonList>
										<IonItem lines="none">
											<IonButtons slot="start">
												<IonLabel slot="start">
													<h2>
														<b>{t(`subtotal`)}: </b>
													</h2>
												</IonLabel>
												<IonChip slot="start">
													<IonIcon icon={diamondOutline} />
													<IonLabel>
														<b>{session.subTotals.coach.toLocaleString()}</b>
													</IonLabel>
												</IonChip>
											</IonButtons>
										</IonItem>
									</IonCardContent>
								</IonCard>
							) : null}

							{session.start && session.end && session.subTotals.privateHire ? (
								<>
									<IonCard color={"dark"}>
										<IonCardContent>
											<IonItem lines="none" color={"dark"}>
												<IonLabel>
													<h2>
														<b>{t(`Private Hiring`)}</b>
													</h2>
												</IonLabel>
											</IonItem>
											<IonList inset color={"dark"}>
												<IonItem>
													<IonLabel>
														<h2>
															<b>
																{session.sessionType === "privateBelt"
																	? t(`privateSnowBelt`)
																	: session.sessionType === "privateLane"
																		? t(`privateSurfLane`)
																		: session.sessionType === "fullSnow"
																			? t(`Private Snow Venue`)
																			: session.sessionType === "fullSurf"
																				? t(`Private Surf Venue`)
																				: t(`Private Park Hire`)}
															</b>
														</h2>
														<p>
															{dayjs(session.end).diff(
																dayjs(session.start),
																"hours"
															)}{" "}

															{dayjs(session.end).diff(
																dayjs(session.start),
																"hours"
															) > 1
																? t(`hours`)
																: t(`hour`)}
														</p>
													</IonLabel>
												</IonItem>
											</IonList>
											<IonItem lines="none" color={"dark"}>
												<IonButtons>
													<IonLabel>
														<h2>
															<b>{t(`subtotal`)}: </b>
														</h2>
													</IonLabel>
													<IonChip color={"warning"}>
														<IonIcon icon={diamondOutline} />
														<IonLabel>
															<b>
																{session.subTotals.privateHire.toLocaleString()}
															</b>
														</IonLabel>
													</IonChip>
												</IonButtons>
											</IonItem>
										</IonCardContent>
									</IonCard>
								</>
							) : null}
						</IonCol>
						<IonCol sizeLg="4" sizeMd="12" sizeSm="12" sizeXs="12">
							<IonCard color="medium">
								<IonCardHeader>
									<IonCardTitle>
										<h3>
											<b>{t(`Summary (Credit)`)}</b>
										</h3>
									</IonCardTitle>
								</IonCardHeader>
								<IonCardContent color="medium">
									<IonList className="ion-no-padding" color="medium">
										{session.subTotals.entrances > 0 && (
											<IonItem
												className="ion-no-padding"
												color="medium"
												lines="full"
											>
												<IonLabel>
													<h3>{session.pax} x {t(`parkEntrance`)}</h3>
													<p>
														(
														{dayjs
															.tz(session.end)
															.diff(dayjs.tz(session.start), "hours")}{" "}
														{t(`hours`)})
													</p>
												</IonLabel>
												<IonChip color="dark">
													<IonIcon icon={diamondOutline} size="small" />
													<IonLabel>
														<h3>
															{session.subTotals.entrances.toLocaleString()}
														</h3>
													</IonLabel>
												</IonChip>
											</IonItem>
										)}

										{session.subTotals.gears > 0 && (
											<IonItem
												className="ion-no-padding"
												color="medium"
												lines="full"
											>
												<IonLabel>
													<h3>{session.gears.length} x {t(`gearRental`)}</h3>
												</IonLabel>
												<IonChip color="dark">
													<IonIcon icon={diamondOutline} size="small" />
													<IonLabel>
														<h3>{session.subTotals.gears.toLocaleString()}</h3>
													</IonLabel>
												</IonChip>
											</IonItem>
										)}

										{session.subTotals.coach > 0 && (
											<IonItem
												className="ion-no-padding"
												color="medium"
												lines="full"
											>
												<IonLabel>
													<h3>{t(`Coaching Fee`)}</h3>
													<p>
														{session.sessionType === "groupClass"
															? `${session.pax} X ${t(`Group Class Fee`)}`
															: t(`privateTraining`)}
													</p>
												</IonLabel>
												<IonChip color="dark">
													<IonIcon icon={diamondOutline} size="small" />
													<IonLabel>
														<h3>{session.subTotals.coach.toLocaleString()}</h3>
													</IonLabel>
												</IonChip>
											</IonItem>
										)}

										{session.subTotals.privateHire > 0 && (
											<IonItem
												className="ion-no-padding"
												color="medium"
												lines="full"
											>
												<IonLabel>
													<h3>
														{session.sessionType === "privateBelt"
															? t(`privateSnowBelt`)
															: session.sessionType === "privateLane"
																? t(`privateSurfLane`)
																: session.sessionType === "fullSnow"
																	? t(`Private Snow Venue`)
																	: session.sessionType === "fullSurf"
																		? t(`Private Surf Venue`)
																		: t(`Private Park Hire`)}
													</h3>
													<p>
														{dayjs(session.end).diff(
															dayjs(session.start),
															"hours"
														)}{" "}

														{dayjs(session.end).diff(
															dayjs(session.start),
															"hours"
														) > 1
															? t(`hours`)
															: t(`hour`)}
													</p>
												</IonLabel>
												<IonChip color="dark">
													<IonIcon icon={diamondOutline} size="small" />
													<IonLabel>
														<h3>
															{session.subTotals.privateHire.toLocaleString()}
														</h3>
													</IonLabel>
												</IonChip>
											</IonItem>
										)}

										{session.memberDiscount &&
											session.memberDiscount > 0 &&
											currentClass &&
											!appliedCoupon &&
											!authUser?.isPartner ? (
											<IonItem
												className="ion-no-padding"
												color="medium"
												lines="full"
											>
												<IonLabel>
													<h3>{i18n.language === 'en' ? currentClass.name.en : (currentClass.name.zh || currentClass.name.en)}</h3>
													<p>{currentClass.discount * 100}% {t(`discount`)}</p>
												</IonLabel>
												<IonChip color="danger">
													<IonIcon icon={diamondOutline} size="small" />
													<IonLabel>
														<h3>
															{Math.ceil(Number(0 -
																Object.values(session.subTotals).reduce(
																	(p, c) => p + c,
																	0
																) *
																currentClass.discount))}
														</h3>
													</IonLabel>
												</IonChip>
											</IonItem>
										) : null}
									</IonList>
									<br />
									{/* Coupon field */}
									{appliedCoupon ? (
										<>
											<IonLabel>
												<h4>
													<b>{t(`coupon`)}</b>
												</h4>
											</IonLabel>
											<IonItem
												className="ion-no-padding"
												color="medium"
												lines="full"
											>
												<IonLabel>
													<h3>{appliedCoupon.code}</h3>
													<p>{`- ${appliedCoupon.discount}${appliedCoupon.unit === "perc" ? "%" : ` ${t(`credits`)}`
														}`}</p>
													<span
														className="action-link"
														style={{
															fontSize: `0.7rem`,
															color: `red`,
															textDecoration: `underline`,
															cursor: "pointer",
														}}
														onClick={() => {
															setAppliedCoupon(null);
														}}
													>
														{t(`remove`)}
													</span>
												</IonLabel>
												<IonChip color="danger">
													<IonIcon icon={diamondOutline} size="small" />
													<IonLabel>
														<h3>
															{appliedCoupon.unit === "credit"
																? -appliedCoupon.discount
																: 0 -
																Object.values(session.subTotals).reduce(
																	(p, c) => p + c,
																	0
																) *
																(appliedCoupon.discount / 100)}
														</h3>
													</IonLabel>
												</IonChip>
											</IonItem>
										</>
									) : (
										<>
											{couponStatus === "idle" && !authUser?.isPartner ? (
												<>
													<IonLabel>
														<h4 style={{ paddingBottom: `0.5rem` }}>
															{t(`Got Coupon?`)}
														</h4>
													</IonLabel>
													<IonCard className="ion-no-margin" color={"light"}>
														<IonItem color="light" lines="full">
															<IonInput
																type="text"
																placeholder={t("Enter coupon code")}
																onIonChange={(e) => {
																	setCouponCode(
																		String(e.detail.value!)
																	);
																}}
																onIonFocus={() => {
																	setCouponError("");
																}}
															/>
															<IonButton
																slot="end"
																color={"secondary"}
																fill="clear"
																disabled={couponCode === ""}
																onClick={() => applyCoupon()}
															>
																<IonIcon
																	slot="icon-only"
																	icon={arrowForwardCircle}
																/>
															</IonButton>
														</IonItem>
													</IonCard>
													{couponError ? (
														<div
															style={{
																display: "flex",
																justifyContent: "flex-start",
																alignItems: "center",
																color: `darkred`,
																margin: `0.5rem 0`,
															}}
														>
															<MdError size={32} />
															<h4>{t(couponError)}</h4>
														</div>
													) : null}
												</>
											) : couponStatus === "applying" ? (
												<IonSpinner name="dots" />
											) : null}
										</>
									)}
									{authUser?.isPartner ? (
										<>
											<IonItem
												className="ion-no-padding"
												color="medium"
												lines="full"
											>
												<IonLabel>
													<h3>Freelance Customer discount</h3>
													<p>90% {t(`discount`)}</p>
												</IonLabel>
												<IonChip color="danger">
													<IonIcon icon={diamondOutline} size="small" />
													<IonLabel>
														<h3>
															{Math.ceil(0 -
																Object.values(session.subTotals).reduce(
																	(p, c) => p + c,
																	0
																) *
																0.9)}
														</h3>
													</IonLabel>
												</IonChip>
											</IonItem>
										</>
									) : null}
									<br />
									{/* End of Coupon field */}
									<IonItem class="ion-no-padding" color="medium" lines="none">
										<IonLabel>
											<h1>
												<b>{t(`total`)}:</b>
											</h1>
										</IonLabel>
										<IonButton color="tertiary" size="default">
											<IonIcon icon={diamondOutline} slot="start" />
											<IonLabel>
												<h2>
													<b>{session.totalCredits.toLocaleString()}</b>
												</h2>
											</IonLabel>
										</IonButton>
									</IonItem>
								</IonCardContent>
							</IonCard>
							<IonButton
								expand="block"
								className="ion-margin"
								color="warning"
								disabled={loading || disabledSubmit}
								onClick={() => {
									handleSubmit();
								}}
							>
								{loading ? (
									<>
										<IonSpinner />
									</>
								) : (
									<>
										<IonIcon icon={cartOutline} slot="start" />
										<IonLabel>{t(`checkout`)}</IonLabel>
									</>
								)}
							</IonButton>
							<IonButton
								className="ion-margin"
								expand="block"
								color={`light`}
								onClick={() => {
									setShowCancelAlert(true);
								}}
							>
								<IonLabel>{t(`Cancel Booking`)}</IonLabel>
							</IonButton>
						</IonCol>
					</IonRow>
				</IonGrid>
				<div className="content-bottom"></div>
			</IonContent>
			<IonAlert
				isOpen={showCancelAlert}
				onDidDismiss={() => {
					setShowCancelAlert(false);
				}}
				header={t(`Cancel Booking`)}
				message={`Are you sure?`}
				buttons={[
					{
						text: `${t(`confirm`)}`,
						role: "confirm",
						handler: () => {
							setShowCancelAlert(false);
							store.dispatch(clearTempSession());
							history.push(`/newBooking`);
						},
					},
					{
						text: `${t(`cancel`)}`,
						role: "cancel",
					},
				]}
			/>
			<IonModal
				isOpen={showFailModal}
				onDidDismiss={() => {
					setShowFailModal(false);
					history.replace(`/newBooking`);
				}}
			>
				<CenteredContent>
					<IonLabel color={`danger`}>
						<h1>
							<b>{t(`Oops!`)}</b>
						</h1>
					</IonLabel>
					<IonLabel className="ion-text-center ion-padding ion-margin">
						<p>
							{t(`The time slots or gears for your booking is no longer available.
							Please book another time slot instead.`)}
						</p>
						{failMessage ? <p>Reason: {t(failMessage)}</p> : null}
					</IonLabel>
					<IonButton
						onClick={async () => {
							setShowFailModal(false);
							setTimeout(() => {
								history.replace(`/newBooking`);
							}, 300);
						}}
					>
						<IonIcon icon={bagAddOutline} slot="start" />
						<IonLabel>{t(`Start a New Booking`)}</IonLabel>
					</IonButton>
				</CenteredContent>
			</IonModal>
		</IonPage>
	);
};

export default OrderPreview;
