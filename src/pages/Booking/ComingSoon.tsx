import {
	IonButton,
	IonCard,
	IonCardContent,
	IonCardHeader,
	IonCardSubtitle,
	IonCardTitle,
	IonCol,
	IonContent,
	IonGrid,
	IonLabel,
	IonPage,
	IonRow,
} from "@ionic/react";
import Header from "../../components/Global/Header";

const ComingSoon: React.FC = () => {
	return (
		<IonPage>
			<Header />
			<IonContent className="fluid">
				<br />
				<IonCard color={`light`}>
					<IonCardHeader>
						<IonCardTitle>系統正在準備中～</IonCardTitle>
						<br />
						<IonCardSubtitle>Thanks for your interest</IonCardSubtitle>
					</IonCardHeader>
					<IonCardContent>
						<IonGrid>
							<IonRow>
								<IonCol>
									<p>我們的網上預約系統暫時關閉，請直接聯絡我們作預約：</p>
									<br />
									<p>
										Our online booking system will be available soon. In the
										meantime please contact us directly for booking:
									</p>
									<br />
									<br />
									<IonButton
										color={"success"}
										href="https://api.whatsapp.com/send?phone=85296373322&text=Hello%20from%20Snow%20N%20Surf!%20How%20can%20I%20help%20you%20today?"
										target="_blank"
									>
										<IonLabel>
											<h3>
												<b>WhatsApp (96373322)</b>
											</h3>
										</IonLabel>
									</IonButton>
									<IonButton
										color="tertiary"
										href="https://m.me/snownsurfhk"
										target="_blank"
									>
										<IonLabel>
											<h3>
												<b>FB (snownsurfhk)</b>
											</h3>
										</IonLabel>
									</IonButton>
									<IonButton
										color="danger"
										href="https://ig.me/m/snownsurfhk"
										target="_blank"
									>
										<IonLabel>
											<h3>
												<b>IG (snownsurfhk)</b>
											</h3>
										</IonLabel>
									</IonButton>
								</IonCol>
							</IonRow>
						</IonGrid>
					</IonCardContent>
				</IonCard>
			</IonContent>
		</IonPage>
	);
};

export default ComingSoon;
