import {
  IonAccordion,
  IonAccordionGroup,
  IonButton,
  IonButtons,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardTitle,
  IonChip,
  IonCol,
  IonContent,
  IonFooter,
  IonGrid,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonRow,
  IonSelect,
  IonSelectOption,
  IonSpinner,
  IonToolbar,
} from "@ionic/react";
import * as SessionTypes from "@tsanghoilun/snow-n-surf-interface/types/session";
import Holidays from "date-holidays";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import {
  addCircleOutline,
  arrowForwardCircleOutline,
  arrowForwardOutline,
  bagCheckOutline,
  callOutline,
  checkmarkCircleOutline,
  closeCircleOutline,
  diamondOutline,
  removeCircleOutline,
  ticketOutline,
} from "ionicons/icons";
import { useEffect, useRef, useState } from "react";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";
import { useTranslation } from "react-i18next";
import { MdOutlineDownhillSkiing } from "react-icons/md";
import { useHistory, useParams } from "react-router";
import { getHkph } from "../../app/functions/misc";
import { useAppSelector } from "../../app/hooks";
import useBreakpoints from "../../app/hooks/useBreakpoints";
import { selectAuthUser } from "../../app/slices/authUserSlice";
import {
  selectPricing,
  selectPricingIsInit,
} from "../../app/slices/pricingSlice";
import {
  selectTempSession,
  setTempSession,
} from "../../app/slices/sessionsSclice";
import { selectCapacities } from "../../app/slices/venueSlice";
import { store } from "../../app/store";
import GearSizeModal from "../../components/Booking/GearSizeModal";
import GroupClassSlotModal from "../../components/Booking/GroupClassSlotModal";
import TimeSlotModal from "../../components/Booking/TimeSlotModal";
import Header from "../../components/Global/Header";
import i18n from "../../i18n";
import "../../theme/calendar.scss";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

var hd = new Holidays();
hd.init(`HK`);

// this indicates the available times and slots on the selected date
type DayStatus =
  | "Checking..."
  | "Very Busy"
  | "Available"
  | "Limited Available"
  | "Sold Out";

export interface PhSource {
  date: string;
  start: Date;
  end: Date;
  name: string;
  rule: string;
  type: string;
}
export interface GearsByPax {
  idx: number;
  type: SessionTypes.PlayType;
  gears: SessionTypes.BookingGear[];
  selected: boolean;
}

interface Props {
  playType?: SessionTypes.PlayType;
}

const NewBooking: React.FC<Props> = (props) => {
  const { t } = useTranslation();
  const history = useHistory();
  const { playType } = props;
  const { presetType } = useParams<{ presetType: string }>();
  const [showTimeSlotModal, setShowTimeSlotModal] = useState(false);
  const [calAccValue, setCalAccValue] = useState<string | undefined>("cal");
  const [showSizeModal, setShowSizeModal] = useState(false);
  const existTempSession = useAppSelector(selectTempSession);
  const [session, setSession] = useState(existTempSession);
  const caps = useAppSelector(selectCapacities);
  const [dayStatus, setDayStatus] = useState<DayStatus>("Checking...");
  const [disabledSubmit, setDisableSubmit] = useState(true);
  const [gearByPaxList, setGearByPaxList] = useState<GearsByPax[]>([
    {
      idx: 0,
      type: `ski`,
      gears: [],
      selected: false,
    },
  ]);
  const authUser = useAppSelector(selectAuthUser);
  const [daySlots, setDaySlots] = useState<number | "loading">("loading");
  const { isXs, isSm, isMd } = useBreakpoints();
  const calAccRef = useRef<null | HTMLIonAccordionGroupElement>(null);
  const isStaff = false;
  const [isHalfPast, setIsHalfPast] = useState(false);

  // states for group class
  const [showGroupClassSlotModal, setShowGroupClassSlotModal] = useState(false);
  const [groupClassDayStatus, setGroupClassDayStatus] =
    useState<DayStatus>("Checking...");
  const [groupClassDaySlots, setGroupClassDaySlots] = useState<
    number | "loading"
  >("loading");

  // get global pricing
  const prices = useAppSelector(selectPricing);
  const pricesIsInit = useAppSelector(selectPricingIsInit);

  // get hk ph
  const ph2 = getHkph();

  // Effect to update redux temp session from state
  useEffect(() => {
    store.dispatch(setTempSession(session));
  }, [session]);

  // effect to put authUser into booking
  useEffect(() => {
    if (!authUser) {
      return;
    }
    setSession((s) => Object.assign({ ...s }, { bookingUser: authUser }));
  }, [authUser]);

  // effect to reset the play type and the pax max
  useEffect(() => {
    const tempSessionType: SessionTypes.SessionType = session.sessionType;
    const tempOldType: SessionTypes.PlayType = session.playType;
    const tempOldPax: number = session.pax;
    let newPlayType: SessionTypes.PlayType | null = null;
    let newPax: number = tempOldPax;

    if (
      tempSessionType === "groupClass" &&
      tempOldPax <= 5 &&
      (tempOldType === "ski" || tempOldType === "sb")
    ) {
      return;
    }

    if (!caps) {
      return;
    }
    const Slope1Max =
      caps.capacities.find((x) => x.venue.name === "Slope 1")?.max || 0;
    const Slope2Max =
      caps.capacities.find((x) => x.venue.name === "Slope 2")?.max || 0;
    const Lane1Max =
      caps.capacities.find((x) => x.venue.name === "Surf Lane 1")?.max || 0;
    const Lane2Max =
      caps.capacities.find((x) => x.venue.name === "Surf Lane 2")?.max || 0;
    const SnowMax = Slope1Max + Slope2Max;
    const SurfMax = Lane1Max + Lane2Max;
    const ParkMax = SnowMax + SurfMax;
    const privateBeltCustomMax = 4;

    if (tempSessionType === "single") {
      if (tempOldType === "snow" || tempOldType === "surf") {
        newPlayType = tempOldType;
      } else {
        newPlayType = "snow";
      }
      tempOldPax > SnowMax && (newPax = SnowMax);
    } else if (
      tempSessionType === "groupClass" ||
      tempSessionType === "privateClass"
    ) {
      if (tempOldType === "ski" || tempOldType === "sb") {
        newPlayType = tempOldType;
      } else {
        newPlayType = "ski";
      }
      if (tempSessionType === "groupClass") {
        tempOldPax > 4 && (newPax = 4);
      } else {
        tempOldPax > 2 && (newPax = 2);
      }
    } else if (
      tempSessionType === "privateBelt" ||
      tempSessionType === "fullSnow"
    ) {
      if (tempOldType === "snow") {
        newPlayType = tempOldType;
      } else {
        newPlayType = "snow";
      }
      if (tempSessionType === "privateBelt") {
        newPax = privateBeltCustomMax;
      } else {
        newPax = SnowMax;
      }
    } else if (
      tempSessionType === "privateLane" ||
      tempSessionType === "fullSurf"
    ) {
      if (tempOldType === "surf") {
        newPlayType = tempOldType;
      } else {
        newPlayType = `surf`;
      }
      if (tempSessionType === "privateLane") {
        newPax = Lane1Max;
      } else {
        newPax = SurfMax;
      }
    } else if (tempSessionType === "fullPark") {
      newPlayType = "all";
      newPax = ParkMax;
    }

    setSession((s) => {
      return Object.assign(
        { ...s },
        {
          playType: newPlayType,
          pax: newPax,
        }
      );
    });

    setCalAccValue("cal");
  }, [session.sessionType, session.playType, session.pax, caps]);

  // effect to preset the session type from the url
  useEffect(() => {
    let newPlayType: SessionTypes.PlayType = "snow";
    let tempPresetType: SessionTypes.SessionType =
      presetType as SessionTypes.SessionType;

    if (playType) {
      newPlayType = playType;
    }

    const legitSessionType: SessionTypes.SessionType[] = [
      "single",
      "groupClass",
      "privateClass",
      "privateBelt",
      "privateLane",
      "fullSnow",
      "fullSurf",
      "fullPark",
    ];
    if (
      !presetType ||
      !legitSessionType.includes(presetType as SessionTypes.SessionType)
    ) {
      tempPresetType = "single";
    }

    if (!isStaff) {
      const allowedTypes: SessionTypes.SessionType[] = [
        "single",
        "groupClass",
        "privateClass",
        "privateBelt",
        "privateLane",
      ];
      if (!allowedTypes.includes(tempPresetType)) {
        tempPresetType = "single";
      }
    }
    setSession((ss) => {
      return Object.assign(
        { ...ss },
        {
          sessionType: tempPresetType,
          playType: newPlayType,
        }
      );
    });
  }, [presetType, isStaff, playType]);

  // effect to remove group class if session type changed
  useEffect(() => {
    const newSessionType = session.sessionType;
    if (newSessionType !== "groupClass") {
      setSession((s) =>
        Object.assign(
          { ...s },
          {
            groupClass: null,
          }
        )
      );
    }
  }, [session.sessionType]);

  // Effect when session date is changed
  useEffect(() => {
    if (!caps || session.sessionType === "groupClass") {
      return;
    }

    setSession((s) =>
      Object.assign(
        { ...s },
        {
          start: null,
          end: null,
        }
      )
    );

    setDayStatus(
      daySlots === "loading"
        ? "Checking..."
        : daySlots <= 0
        ? `Sold Out`
        : daySlots <= 0.3
        ? `Very Busy`
        : daySlots <= 0.6
        ? `Limited Available`
        : `Available`
    );
  }, [
    session.sessionDate,
    session.playType,
    session.pax,
    caps,
    daySlots,
    session.sessionType,
  ]);

  // Effect when session date is changed (group class)
  useEffect(() => {
    if (!caps) {
      return;
    }

    setSession((s) =>
      Object.assign(
        { ...s },
        {
          start: null,
          end: null,
        }
      )
    );

    setGroupClassDayStatus(
      groupClassDaySlots === "loading"
        ? "Checking..."
        : groupClassDaySlots <= 0
        ? `Sold Out`
        : groupClassDaySlots <= 0.3
        ? `Very Busy`
        : groupClassDaySlots <= 0.6
        ? `Limited Available`
        : `Available`
    );
  }, [session.sessionDate, session.pax, caps, groupClassDaySlots]);

  // effect for gearByPax reseting
  useEffect(() => {
    const tempGearByPaxList: GearsByPax[] = [];
    [...Array(session.pax)].forEach((x, idx) => {
      const type =
        session.playType === "snow" ||
        session.playType === "ski" ||
        session.playType === "all"
          ? "ski"
          : "sb";
      const newGearByPax: GearsByPax = {
        idx,
        type,
        gears: [],
        selected: false,
      };
      tempGearByPaxList.push(newGearByPax);
    });
    setGearByPaxList(tempGearByPaxList);
  }, [
    session.pax,
    session.sessionDate,
    session.start,
    session.end,
    session.playType,
  ]);

  // effect to toggle submit and update price
  useEffect(() => {
    if (!pricesIsInit || !prices) {
      return;
    }
    if (!session.start || !session.end) {
      setDisableSubmit(true);
    } else {
      setDisableSubmit(false);
    }

    // recal entrence cost
    const playHours =
      Math.abs(dayjs.tz(session.start).diff(dayjs.tz(session.end), "hours")) ||
      0;
    const sessionUnitPrice = prices.enterance.price;
    const pax = session.pax;
    const paxTotal =
      session.sessionType === "single" ||
      session.sessionType === "groupClass" ||
      session.sessionType === "privateClass"
        ? sessionUnitPrice * playHours * pax
        : 0;

    // recal gears
    let gearTotal = 0;
    const gearStandardPrice = prices.gear_standard.price;
    const gearPremiumPrice = prices.gear_premium.price;
    let bookedGears = {
      standard: {
        shoe: 0,
        board: 0,
      },
      premium: {
        shoe: 0,
        board: 0,
      },
    };
    session.gears.forEach((sg) => {
      // gearTotal += sg.gear.prices.find((x) => x.gearClass === sg.class)?.unitPrice || 0;
      const usage = sg.gear.gearUsage;
      const gearClass = sg.class;
      bookedGears[gearClass][usage]++;
    });

    gearTotal =
      Math.max(
        bookedGears["standard"]["shoe"],
        bookedGears["standard"]["board"]
      ) *
        gearStandardPrice +
      Math.max(
        bookedGears["premium"]["shoe"],
        bookedGears["premium"]["board"]
      ) *
        gearPremiumPrice;

    console.log(`gear subtotal after recal:`, gearTotal);

    // recal coach
    let coachTotal = 0;
    let privateHirePrice = 0;
    if (session.pax) {
      switch (session.sessionType) {
        case `groupClass`:
          coachTotal = pax * prices.group_class.price * playHours;
          break;
        case `privateClass`:
          coachTotal = prices.personal_training.price * playHours;
          break;
        case `privateBelt`:
          privateHirePrice = prices.default_snow_belt.price * playHours;
          break;
        case `privateLane`:
          privateHirePrice = prices.default_surf_lane.price * playHours;
          break;
        case `fullSnow`:
          privateHirePrice = prices.default_snow_venue.price * playHours;
          break;
        case `fullSurf`:
          privateHirePrice = prices.default_surf_venue.price * playHours;
          break;
        case `fullPark`:
          privateHirePrice = prices.default_full_park.price * playHours;
          break;

        default:
          break;
      }
    }

    let subTotals: SessionTypes.SubTotals = {
      entrances: paxTotal,
      gears: gearTotal,
      coach: coachTotal,
      privateHire: privateHirePrice,
    };

    setSession((s) =>
      Object.assign(
        { ...s },
        {
          totalCredits: Object.values(subTotals).reduce((a, b) => a + b, 0),
          subTotals,
        }
      )
    );
  }, [
    session.start,
    session.end,
    session.pax,
    session.gears,
    session.sessionType,
    prices,
    pricesIsInit,
  ]);

  // effect to update the gear list in session, from the updated gear by persons list
  useEffect(() => {
    let tempGearList: SessionTypes.BookingGear[] = [];
    gearByPaxList.forEach((x) => {
      x.gears
        .filter((y) => y.size)
        .forEach((z) => {
          const tempGearSize: SessionTypes.BookingGear = {
            size: z.size,
            class: z.class,
            gear: z.gear,
          };
          tempGearList.push(tempGearSize);
        });
    });
    setSession((s) =>
      Object.assign(
        { ...s },
        {
          gears: tempGearList,
        }
      )
    );
  }, [gearByPaxList]);

  // effect to update the 'gear by persons' state
  const updateGearRentals = (gearsByPax: GearsByPax) => {
    let TempGearByPaxList = [...gearByPaxList];
    let tempNewGearByPax = Object.assign(
      { ...gearsByPax },
      { selected: false }
    );
    TempGearByPaxList.forEach((x) => {
      x.selected = false;
    });
    TempGearByPaxList[gearsByPax.idx] = tempNewGearByPax;
    setGearByPaxList(TempGearByPaxList);
  };

  return (
    <>
      <IonPage>
        <Header />
        <IonContent className="fluid">
          <br />
          <div color="transparent" className="ion-hide-md-down">
            <IonCardHeader>
              <IonCardTitle>{t(`bookYourNextAdventure`)}</IonCardTitle>
            </IonCardHeader>
          </div>

          <IonGrid className="ion-no-padding">
            <IonRow>
              <IonCol>
                <IonCard
                  className={`ion-no-margin ion-margin-start ion-margin-end ${
                    isXs || isSm || isMd ? `ion-margin-bottom` : ``
                  }`}
                >
                  <IonItem lines="none" color={"tertiary"}>
                    <IonLabel
                      className="ion-justify-content-start ion-align-items-center"
                      style={{ display: "flex" }}
                      position="stacked"
                    >
                      <IonIcon
                        icon={ticketOutline}
                        size="small"
                        className="ion-margin-end"
                      />
                      <b>{t(`sessionType`)}</b>
                    </IonLabel>
                    <IonSelect
                      value={session.sessionType}
                      interface="action-sheet"
                      onIonChange={(e) => {
                        //set session type
                        setSession((ss) =>
                          Object.assign(
                            { ...ss },
                            {
                              sessionType: e.detail
                                .value! as SessionTypes.SessionType,
                            }
                          )
                        );
                      }}
                    >
                      <IonSelectOption value={"single"}>
                        {t(`Park Entrance Only`)}
                      </IonSelectOption>
                      {!authUser?.isPartner ? (
                        <>
                          <IonSelectOption value={"groupClass"}>
                            {t(`groupClass`)}
                          </IonSelectOption>
                          <IonSelectOption value={"privateClass"}>
                            {t(`privateTraining`)}
                          </IonSelectOption>
                        </>
                      ) : null}

                      {isStaff ? (
                        <>
                          <IonSelectOption value={"fullSnow"}>
                            {t(`Private Snow Venue`)}
                          </IonSelectOption>
                          <IonSelectOption value={"fullSurf"}>
                            {t(`Private Surf Venue`)}
                          </IonSelectOption>
                          <IonSelectOption value={"fullPark"}>
                            {t(`Private Park Hire`)}
                          </IonSelectOption>
                        </>
                      ) : !authUser?.isPartner ? (
                        <>
                          <IonSelectOption value={"privateBelt"}>
                            {t(`privateSnowBelt`)}
                          </IonSelectOption>
                          <IonSelectOption value={"privateLane"}>
                            {t(`privateSurfLane`)}
                          </IonSelectOption>
                        </>
                      ) : null}
                    </IonSelect>
                  </IonItem>
                </IonCard>
              </IonCol>
              {session.sessionType === `groupClass` ? null : (
                <IonCol>
                  <IonCard
                    className={`ion-no-margin ion-margin-start ion-margin-end`}
                  >
                    <IonItem lines="none" color={"secondary"}>
                      <IonLabel
                        className="ion-justify-content-start ion-align-items-center"
                        style={{ display: "flex" }}
                        position="stacked"
                      >
                        <MdOutlineDownhillSkiing
                          size={18}
                          className="ion-margin-end"
                        />
                        <b>{t(`gameType`)}</b>
                      </IonLabel>
                      <IonSelect
                        value={session.playType}
                        interface="action-sheet"
                        onIonChange={(e) => {
                          setSession((ss) =>
                            Object.assign(
                              { ...ss },
                              {
                                playType: e.detail
                                  .value! as SessionTypes.PlayType,
                              }
                            )
                          );
                        }}
                      >
                        {session.sessionType === "single" ||
                        session.sessionType === "privateBelt" ||
                        session.sessionType === "fullSnow" ? (
                          <IonSelectOption value={"snow"}>
                            {t(`skiAndSnowboard`)}
                          </IonSelectOption>
                        ) : null}

                        {session.sessionType === "single" ||
                        session.sessionType === "privateLane" ||
                        session.sessionType === "fullSurf" ? (
                          <IonSelectOption value={"surf"}>
                            {t(`surf`)}
                          </IonSelectOption>
                        ) : null}

                        {session.sessionType === "fullPark" ? (
                          <IonSelectOption value={"all"}>
                            {t(`snowNSurf`)}
                          </IonSelectOption>
                        ) : null}

                        {session.sessionType === "privateClass" ? (
                          <>
                            <IonSelectOption value={"ski"}>
                              {t(`ski`)}
                            </IonSelectOption>
                            <IonSelectOption value={"sb"}>
                              {t(`sb`)}
                            </IonSelectOption>
                          </>
                        ) : null}
                      </IonSelect>
                    </IonItem>
                  </IonCard>
                </IonCol>
              )}
            </IonRow>
          </IonGrid>
          {session.sessionType === "fullSnow" ||
          session.sessionType === "fullSurf" ||
          session.sessionType === "fullPark" ? (
            <>
              <IonCard>
                <IonCardContent>
                  <IonGrid>
                    <IonRow>
                      <IonCol>
                        <IonCardTitle>
                          Looking for Private Venue Hiring?
                        </IonCardTitle>
                        <br />
                      </IonCol>
                    </IonRow>
                    <IonRow>
                      <IonCol>
                        <IonLabel>
                          {`With this option, you (and up to 4 friends) will be entitled to an hour long private access to our state of the art “needle mushroom” snow belt machine for a focused training in the technique you with to improve most. Our snow belt machine realistically simulate the experience of skiing/snowboarding, allowing you to learn or brush up your skills before embarking your next ski holiday. The snow belt will be operated by our experienced staff.`}
                          <br />
                          <br />
                          <h3>TEL: 91233211</h3>
                        </IonLabel>
                      </IonCol>
                    </IonRow>
                    <IonRow>
                      <IonCol>
                        <IonButton color={"warning"} href="tel:+85296541282">
                          <IonIcon icon={callOutline} slot="start" />
                          <IonLabel>
                            <b>Call Us NOW</b>
                          </IonLabel>
                        </IonButton>
                      </IonCol>
                    </IonRow>
                  </IonGrid>
                </IonCardContent>
              </IonCard>
            </>
          ) : (
            <>
              {!authUser?.isPartner ? (
                <IonCard className="">
                  <IonItem lines="none">
                    <IonLabel>
                      <h2>
                        <b>
                          {t(`for`)}
                          <span className="ion-hide-md-down">
                            {t(`howManyPerson`)}
                          </span>
                        </b>
                      </h2>
                    </IonLabel>
                    <IonButtons slot="end">
                      <IonButton
                        disabled={
                          session.pax <= 1 ||
                          session.sessionType === "privateBelt" ||
                          session.sessionType === "privateLane"
                        }
                        onClick={() => {
                          setSession(
                            Object.assign(
                              { ...session },
                              {
                                pax: session.pax - 1,
                              }
                            )
                          );
                        }}
                      >
                        <IonIcon slot="icon-only" icon={removeCircleOutline} />
                      </IonButton>
                      <IonChip>
                        {session.pax}{" "}
                        {`${session.pax > 1 ? t(`persons`) : t(`person`)}`}
                      </IonChip>
                      <IonButton
                        disabled={
                          session.sessionType === "single"
                            ? session.pax >= 6
                            : session.sessionType === "groupClass"
                            ? session.pax >= 4
                            : session.sessionType === "privateClass"
                            ? session.pax >= 2
                            : session.pax >= 16 ||
                              session.sessionType === "privateBelt" ||
                              session.sessionType === "privateLane"
                        }
                        onClick={() => {
                          setSession(
                            Object.assign(
                              { ...session },
                              {
                                pax: session.pax + 1,
                              }
                            )
                          );
                        }}
                      >
                        <IonIcon slot="icon-only" icon={addCircleOutline} />
                      </IonButton>
                    </IonButtons>
                  </IonItem>
                </IonCard>
              ) : null}

              <IonCard className="fluid">
                <IonCardContent>
                  <IonAccordionGroup value={calAccValue} ref={calAccRef}>
                    <IonAccordion value="cal">
                      <IonItem
                        slot="header"
                        onClick={() => {
                          if (calAccValue) {
                            setCalAccValue(undefined);
                          } else {
                            setCalAccValue("cal");
                          }
                        }}
                      >
                        <IonLabel>
                          <h2>
                            <b>
                              {calAccValue
                                ? t(`selectYourDate`)
                                : dayjs
                                    .tz(session.sessionDate)
                                    .format(`DD MMM YY`)}
                            </b>
                          </h2>
                        </IonLabel>
                      </IonItem>
                      <div slot="content">
                        <Calendar
                          onChange={(value: Date) => {
                            const newDateString = dayjs(value)
                              .startOf("day")
                              .format(`YYYY-MM-DD`);
                            const newDateTz = dayjs
                              .tz(`${newDateString}T00:00:00`)
                              .toDate();
                            setSession(
                              Object.assign(
                                { ...session },
                                { sessionDate: newDateTz }
                              )
                            );
                          }}
                          value={dayjs(
                            `${dayjs
                              .tz(session.sessionDate)
                              .format(`YYYY-MM-DD`)}T00:00:00`
                          ).toDate()}
                          minDate={dayjs
                            .tz(Date.now())
                            // .endOf("day")
                            // .add(1, "day")
                            .toDate()}
                          maxDate={dayjs
                            .tz(Date.now())
                            .endOf("day")
                            .add(60, "day")
                            .toDate()}
                          tileClassName={({ activeStartDate, date, view }) => {
                            const isPh =
                              ph2.filter(
                                (x) =>
                                  date.valueOf() >= x.start.valueOf() &&
                                  date.valueOf() < x.end.valueOf()
                              ).length > 0;
                            if (isPh) {
                              return "calendar-ph-tile";
                            } else {
                              return "";
                            }
                          }}
                        />
                      </div>
                    </IonAccordion>
                  </IonAccordionGroup>
                  {!session.start && !session.end ? (
                    <IonItem lines="none">
                      <IonLabel>
                        <h2>
                          <b>{t(`selectYourTime`)}</b>
                        </h2>
                      </IonLabel>
                    </IonItem>
                  ) : null}

                  {session.sessionType !== "groupClass" ? (
                    <IonList inset>
                      {!session.start || !session.end ? (
                        <IonItem
                          style={{
                            cursor: "pointer",
                          }}
                          disabled={
                            dayStatus === `Checking...` ||
                            dayStatus === `Sold Out`
                          }
                          color={
                            dayStatus === `Sold Out`
                              ? `medium`
                              : dayStatus === `Very Busy`
                              ? `danger`
                              : dayStatus === `Limited Available`
                              ? `warning`
                              : dayStatus === `Available`
                              ? `success`
                              : `dark`
                          }
                          onClick={() => {
                            setCalAccValue("cal");
                            setShowTimeSlotModal(true);
                          }}
                        >
                          <IonLabel>
                            <h3>{t(dayStatus)}</h3>
                          </IonLabel>
                          {dayStatus === `Checking...` ? (
                            <IonSpinner slot="end" />
                          ) : (
                            <IonIcon
                              icon={
                                dayStatus === `Sold Out`
                                  ? closeCircleOutline
                                  : arrowForwardCircleOutline
                              }
                              slot="end"
                            />
                          )}
                        </IonItem>
                      ) : (
                        <IonItem
                          color="secondary"
                          button
                          detail={false}
                          onClick={() => setShowTimeSlotModal(true)}
                        >
                          <IonLabel>
                            <h3>
                              {dayjs.tz(session.start).format(`HH:mm`)}-
                              {dayjs.tz(session.end).format(`HH:mm`)}
                              {` (${dayjs
                                .tz(session.end)
                                .diff(dayjs.tz(session.start), "hours")} Hour${
                                dayjs
                                  .tz(session.end)
                                  .diff(dayjs.tz(session.start), "hours") > 1
                                  ? `s`
                                  : ``
                              })`}
                            </h3>
                          </IonLabel>
                          <IonIcon icon={checkmarkCircleOutline} slot="end" />
                        </IonItem>
                      )}
                    </IonList>
                  ) : (
                    /** this is for group class */
                    <IonList inset>
                      {!session.start || !session.end ? (
                        <IonItem
                          style={{
                            cursor: "pointer",
                          }}
                          disabled={
                            groupClassDayStatus === `Checking...` ||
                            groupClassDayStatus === `Sold Out`
                          }
                          color={
                            groupClassDayStatus === `Sold Out`
                              ? `medium`
                              : groupClassDayStatus === `Very Busy`
                              ? `danger`
                              : groupClassDayStatus === `Limited Available`
                              ? `warning`
                              : groupClassDayStatus === `Available`
                              ? `success`
                              : `dark`
                          }
                          onClick={() => {
                            setCalAccValue("cal");
                            setShowGroupClassSlotModal(true);
                          }}
                        >
                          <IonLabel>
                            <h3>{t(groupClassDayStatus)}</h3>
                          </IonLabel>
                          {groupClassDayStatus === `Checking...` ? (
                            <IonSpinner slot="end" />
                          ) : (
                            <IonIcon
                              icon={
                                groupClassDayStatus === `Sold Out`
                                  ? closeCircleOutline
                                  : arrowForwardCircleOutline
                              }
                              slot="end"
                            />
                          )}
                        </IonItem>
                      ) : (
                        <IonItem
                          color="secondary"
                          button
                          detail={false}
                          onClick={() => setShowGroupClassSlotModal(true)}
                        >
                          {session.groupClass ? (
                            <IonLabel>
                              <h3>
                                {dayjs
                                  .tz(session.sessionDate)
                                  .format(`DD MMM YYYY`)}
                              </h3>
                              <h4>{`${dayjs
                                .tz(session.start)
                                .format(`HH:mm`)} - ${dayjs
                                .tz(session.end)
                                .format(`HH:mm`)}`}</h4>
                              <h4>
                                <b>
                                  {i18n.language === "en"
                                    ? session.groupClass.classPreset.name.en
                                    : session.groupClass.classPreset.name.zh ||
                                      session.groupClass.classPreset.name.en}
                                </b>
                              </h4>
                            </IonLabel>
                          ) : (
                            <IonLabel>{t(`Please select a class`)}</IonLabel>
                          )}
                          <IonIcon icon={checkmarkCircleOutline} slot="end" />
                        </IonItem>
                      )}
                    </IonList>
                  )}

                  <IonItem lines="none">
                    <IonButtons slot="start">
                      <IonLabel slot="start">
                        <h2>
                          <b>{t(`parkEntrance`)}: </b>
                        </h2>
                      </IonLabel>
                      <IonChip slot="start">
                        <IonIcon icon={diamondOutline} />
                        <IonLabel>
                          <b>{session.subTotals.entrances.toLocaleString()}</b>
                        </IonLabel>
                      </IonChip>
                    </IonButtons>
                  </IonItem>
                </IonCardContent>
              </IonCard>
              {session.start &&
                session.end &&
                (session.sessionType === "groupClass" ||
                  session.sessionType === "privateClass") && (
                  <IonCard id="coach-details">
                    <IonCardContent>
                      <IonAccordionGroup value={"coach"}>
                        <IonAccordion value="coach">
                          <IonItem slot="header">
                            <IonLabel>
                              <h2>
                                <b>
                                  {session.sessionType === "groupClass"
                                    ? t(`groupClass`)
                                    : t(`privateTraining`)}
                                </b>
                              </h2>
                            </IonLabel>
                          </IonItem>
                          <IonList slot="content" lines="none">
                            {session.sessionType === "groupClass" ? (
                              <IonItem>
                                <IonButtons slot="start">
                                  <IonLabel slot="start">
                                    <h3>
                                      <b>{t(`groupClass`)}</b>
                                      {" - "}
                                      {session.playType === "ski"
                                        ? t(`ski`)
                                        : session.playType === "sb"
                                        ? t(`sb`)
                                        : t(`surf`)}
                                    </h3>
                                    <p>
                                      {prices?.group_class.price} {t(`credits`)}{" "}
                                      {t(`per pax / hour`)}
                                    </p>
                                  </IonLabel>
                                </IonButtons>
                              </IonItem>
                            ) : (
                              <IonItem>
                                <IonButtons slot="start">
                                  <IonLabel slot="start">
                                    <h3>
                                      <b>{t(`privateTraining`)}</b>
                                      {" - "}
                                      {session.playType === "ski"
                                        ? t(`ski`)
                                        : session.playType === "sb"
                                        ? t(`sb`)
                                        : t(`surf`)}
                                    </h3>
                                    <p>
                                      {prices?.personal_training.price}{" "}
                                      {t(`credits`)} {t(`per group / hour`)}
                                    </p>
                                  </IonLabel>
                                </IonButtons>
                              </IonItem>
                            )}
                          </IonList>
                        </IonAccordion>
                      </IonAccordionGroup>
                      <br />
                      <IonItem lines="none">
                        <IonButtons slot="start">
                          <IonLabel slot="start">
                            <h2>
                              <b>{t(`subtotal`)}: </b>
                            </h2>
                          </IonLabel>
                          <IonChip slot="start">
                            <IonIcon icon={diamondOutline} />
                            <IonLabel>
                              <b>{session.subTotals.coach.toLocaleString()}</b>
                            </IonLabel>
                          </IonChip>
                        </IonButtons>
                      </IonItem>
                    </IonCardContent>
                  </IonCard>
                )}

              {session.start &&
                session.end &&
                session.playType !== "surf" &&
                !authUser?.isPartner && (
                  <>
                    <IonCard>
                      <IonCardContent>
                        <IonAccordionGroup value={"gears"}>
                          <IonAccordion value="gears">
                            <IonItem slot="header">
                              <IonLabel>
                                <h2>
                                  <b>{t(`gearRental`)}</b>
                                </h2>
                              </IonLabel>
                            </IonItem>
                            <IonList slot="content" lines="none">
                              {gearByPaxList.map((item, idx) => (
                                <IonItem key={`gear-person-item-${idx}`}>
                                  <IonLabel>
                                    <h3>
                                      <b>{`${t(`player`)} ${idx + 1}`}</b>
                                    </h3>
                                    {item.gears.filter((x) => x.size).length <=
                                    0 ? (
                                      <p>{t(`noGearRented`)}</p>
                                    ) : (
                                      item.gears
                                        .filter((x) => x.size)
                                        .map((gg) => (
                                          <IonLabel
                                            key={`rented-gear-${gg.gear.id}-${gg.size}-${item.idx}`}
                                          >
                                            <p>
                                              {gg.gear.name}
                                              <br />
                                              {String(
                                                gg.class
                                              ).toUpperCase()}{" "}
                                              {`${gg.size}${gg.gear.unit.name}`}
                                              <br />
                                              <br />
                                            </p>
                                          </IonLabel>
                                        ))
                                    )}
                                  </IonLabel>
                                  <IonButton
                                    fill="solid"
                                    size="small"
                                    color={`secondary`}
                                    onClick={() => {
                                      let tempList = [...gearByPaxList];
                                      tempList.forEach(
                                        (x) => (x.selected = false)
                                      );
                                      Object.assign(tempList[idx], {
                                        selected: true,
                                      });
                                      setGearByPaxList(tempList);
                                      setShowSizeModal(true);
                                    }}
                                  >
                                    <IonIcon
                                      icon={arrowForwardOutline}
                                      // slot='icon-only'
                                      className="ion-hide-md-up"
                                    />
                                    <IonLabel className="ion-hide-md-down">
                                      <b>{t(`change`)}</b>
                                    </IonLabel>
                                  </IonButton>
                                </IonItem>
                              ))}
                            </IonList>
                          </IonAccordion>
                        </IonAccordionGroup>
                        <br />
                        <IonItem lines="none">
                          <IonButtons slot="start">
                            <IonLabel slot="start">
                              <h2>
                                <b>{t(`subtotal`)}: </b>
                              </h2>
                            </IonLabel>
                            <IonChip slot="start">
                              <IonIcon icon={diamondOutline} />
                              <IonLabel>
                                <b>
                                  {session.subTotals.gears.toLocaleString()}
                                </b>
                              </IonLabel>
                            </IonChip>
                          </IonButtons>
                        </IonItem>
                      </IonCardContent>
                    </IonCard>
                  </>
                )}
            </>
          )}
        </IonContent>
        <IonFooter color="dark" className="dark">
          <IonToolbar
            color="dark"
            className="fluid"
            style={{
              padding: `1rem`,
            }}
          >
            <IonLabel slot="start">
              <h2 className="ion-hide-md-down">
                <b>{t(`orderTotal`)}:</b>
              </h2>
            </IonLabel>
            <IonChip color="light">
              <IonIcon icon={diamondOutline} />
              <IonLabel>
                <b>
                  {session.totalCredits.toLocaleString()} {t(`credits`)}
                </b>
              </IonLabel>
            </IonChip>
            <IonButtons slot="end">
              <IonButton
                fill="solid"
                color={disabledSubmit ? "medium" : "warning"}
                size="large"
                disabled={disabledSubmit}
                onClick={() => {
                  history.push(`/orderPreview`);
                }}
              >
                <IonIcon slot="start" icon={bagCheckOutline} />
                <IonLabel>{t(`preview`)}</IonLabel>
              </IonButton>
            </IonButtons>
          </IonToolbar>
        </IonFooter>
        <TimeSlotModal
          isOpen={showTimeSlotModal}
          onClose={() => {
            setShowTimeSlotModal(false);
          }}
          onSubmit={(sessionClone) => {
            setShowTimeSlotModal(false);
            setSession(sessionClone);
            setCalAccValue(undefined);
          }}
          onUpdate={(daySlots) => {
            setDaySlots(daySlots);
          }}
          session={session}
          caps={caps}
          isHalfPast={isHalfPast}
          onChangeHalfPast={() => setIsHalfPast(!isHalfPast)}
        />
        <GroupClassSlotModal
          isOpen={showGroupClassSlotModal}
          onClose={() => {
            setShowGroupClassSlotModal(false);
          }}
          onSubmit={(sessionClone) => {
            setShowGroupClassSlotModal(false);
            setSession(sessionClone);
            setCalAccValue(undefined);
          }}
          onUpdate={(daySlots) => {
            setGroupClassDaySlots(daySlots);
          }}
          session={session}
          caps={caps}
          isHalfPast={isHalfPast}
          onChangeHalfPast={() => setIsHalfPast(!isHalfPast)}
        />
        <GearSizeModal
          isOpen={showSizeModal}
          onClose={() => {
            setShowSizeModal(false);
          }}
          onSubmit={(gearsByPax: GearsByPax) => {
            updateGearRentals(gearsByPax);
            setShowSizeModal(false);
          }}
          gearByPax={gearByPaxList.find((x) => x.selected)}
        />
      </IonPage>
    </>
  );
};

export default NewBooking;
