import {
	IonAccordion,
	IonAccordionGroup,
	IonCard,
	IonCardContent,
	IonCol,
	IonContent,
	IonGrid,
	IonItem,
	IonLabel,
	IonList,
	IonPage,
	IonRadio,
	IonRadioGroup,
	IonRow,
	IonSpinner,
} from "@ionic/react";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { FiPlus } from "react-icons/fi";
import { useAppSelector } from "../../app/hooks";
import {
	selectAllFaqCats,
	selectAllFaqQuestions,
} from "../../app/slices/faqSlice";
import Header from "../../components/Global/Header";
import i18n from "../../i18n";

const FaqList: React.FC = () => {
	const allCats = useAppSelector(selectAllFaqCats);
	const allQuestions = useAppSelector(selectAllFaqQuestions);
	const [selectedCat, setSelectedCat] = useState("");
	const translate = useTranslation();

	useEffect(() => {}, [translate]);

	return (
		<IonPage>
			<Header />
			<IonContent className="fluid">
				{allCats.length <= 0 ? (
					<IonCard color={"light"}>
						<IonCardContent className="ion-text-center">
							<IonSpinner name="dots" />
						</IonCardContent>
					</IonCard>
				) : (
					<>
						<br />
						<IonGrid>
							<IonRow>
								<IonCol>
									<IonLabel className="ion-margin-start">
										<b>Category</b>
									</IonLabel>
									<IonList inset color="light">
										<IonRadioGroup
											value={selectedCat}
											onIonChange={(e) => {
												setSelectedCat(e.detail.value!);
											}}
										>
											{allCats.map((x) => (
												<IonItem key={`faq-cat-${x.id}`} color="light">
													<IonRadio slot="start" value={x.id} />
													<IonLabel>
														{i18n.language === "en"
															? x.name.en
															: (x.name.zh || x.name.en)}
													</IonLabel>
												</IonItem>
											))}
										</IonRadioGroup>
									</IonList>
								</IonCol>
							</IonRow>
							<IonRow>
								<IonCol>
									{allQuestions
										.filter((x) => x.categoryId === selectedCat)
										.map((y) => (
											<IonCard key={`question-${y.id}`} color="dark">
												<IonAccordionGroup>
													<IonAccordion value={y.id}>
														<div
															slot="header"
															style={{
																backgroundColor: `var(--ion-color-dark)`,
																padding: `1rem 2rem`,
																display: `flex`,
																alignItems: `center`,
															}}
														>
															<p
																style={{
																	marginRight: "auto",
																}}
															>
																<b>
																	{i18n.language === "en"
																		? y.question.en
																		: y.question.zh || y.question.en}
																</b>
															</p>
															<div>
																<FiPlus size={24} />
															</div>
														</div>
														<div
															slot={`content`}
															className="ion-padding"
															style={{
																backgroundColor: `var(--ion-color-dark)`,
																padding: `1rem 2rem`,
															}}
															dangerouslySetInnerHTML={{
																__html:
																	i18n.language === "en"
																		? y.answer.en
																		: y.answer.zh || y.answer.en,
															}}
														></div>
													</IonAccordion>
												</IonAccordionGroup>
											</IonCard>
										))}
								</IonCol>
							</IonRow>
						</IonGrid>
					</>
				)}
			</IonContent>
		</IonPage>
	);
};

export default FaqList;
