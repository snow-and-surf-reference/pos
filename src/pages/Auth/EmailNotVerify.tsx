import {
	IonButton, IonCard,
	IonCardContent,
	IonCol,
	IonContent,
	IonGrid,
	IonIcon,
	IonLabel, IonPage,
	IonRow,
	IonSpinner
} from "@ionic/react";
import { arrowForwardCircle } from "ionicons/icons";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router";
import { resendVerifyLink } from "../../app/firebase/global";
import { useAppSelector } from "../../app/hooks";
import { selectAuthUser } from "../../app/slices/authUserSlice";
import Header from "../../components/Global/Header";
import VerifyEmailSent from "./VerifyEmailSent";

const EmailNotVerify: React.FC = () => {
	const { t } = useTranslation();
	const [status, setStatus] = useState("pending");
	const authUser = useAppSelector(selectAuthUser);
	const history = useHistory();

	useEffect(() => {
		return () => {
			// console.log("exit email verify page");
			setStatus("pending");
		};
	}, []);

	useEffect(() => {
		if (!authUser) {
			return;
		}
		if (authUser.emailVerified) {
			setStatus("verified");
		}
	}, [authUser]);

	const handleResend = async () => {
		setStatus("sending");
		const res = await resendVerifyLink();
		if (res === "success") {
			setStatus("sent");
		} else {
			setStatus("pending");
		}
	};

	return (
		<IonPage>
			<Header />
			<IonContent className="fluid">
				<IonGrid>
					<IonRow>
						<IonCol>
							<br />
							<IonLabel>
								<h1 className="banner-heading">
									{t(`Verify Your Email Yet?`)}
								</h1>
							</IonLabel>
						</IonCol>
					</IonRow>
				</IonGrid>
				{status === "pending" || status === 'sending' ? (
					<IonCard color={`dark`}>
						<IonCardContent>
							<IonLabel>
								<h1>
									<b>{t(`We love to have you on board!`)}</b>
								</h1>
								<br />
								<h2>
									{t(`But it is also important to protect your Privacy and Identity,
									therefore we humblely ask you to verify your email address
									before starting to make your first booking`)}
								</h2>
							</IonLabel>
							<br />
							<br />
							{status === "sending" ? (
								<IonSpinner name="dots" color={`warning`} />
							) : (
								<>
									<IonButton
										color={`warning`}
										fill="solid"
										onClick={() => {
											handleResend();
										}}
									>
										<IonIcon slot="end" icon={arrowForwardCircle} />
										<IonLabel>
												<b>{t(`Resend Verify Link`)}</b>
										</IonLabel>
									</IonButton>
									<IonButton color={`warning`} fill="clear">
											<IonLabel>{t(`I'll Do it Later`)}</IonLabel>
									</IonButton>
								</>
							)}
						</IonCardContent>
					</IonCard>
				) : status === "sent" ? (
					<VerifyEmailSent />
				) : status === 'verified' ? (
					<IonCard>
						<IonCardContent>
							<IonLabel>
								<h1>
											<b>{t(`Your Email is successfully verified!`)}</b>
								</h1>
							</IonLabel>
							<br />
							<br />
							<IonButton
								color={`warning`}
								fill="solid"
								onClick={() => {
									history.replace(`/`);
								}}
							>
								<IonLabel>
											<b>{t(`Back to Home`)}</b>
								</IonLabel>
							</IonButton>
						</IonCardContent>
					</IonCard>
				) : null}
			</IonContent>
		</IonPage>
	);
};

export default EmailNotVerify;
