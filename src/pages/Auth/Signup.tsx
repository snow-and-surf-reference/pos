import {
  IonButton,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCol,
  IonContent,
  IonGrid,
  IonIcon,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonRow,
  IonSpinner,
  IonToast,
} from "@ionic/react";
import { t } from "i18next";
import {
  alertCircleOutline,
  checkmarkCircle,
  closeCircleSharp,
  eye,
  eyeOffOutline,
  logInOutline,
} from "ionicons/icons";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router";
import { signup } from "../../app/firebase/global";
import { Field } from "../../app/types/signup";
import Header from "../../components/Global/Header";
import { defaultFields } from "./fields";

interface FieldProps {
  field: Field | undefined;
  showPw: boolean;
  setShowPw: (showPw: boolean) => void;
  setField: (value: any, field: Field) => void;
  readOnly: boolean;
}

const FieldItem: React.FC<FieldProps> = (props) => {
  const { field, showPw, setShowPw, setField, readOnly } = props;
  const handleDobValue = (v: string) => {
    let newV = v;
    if (newV.length > 8) {
      newV = newV.substring(0, 8);
    }
    if (v.length >= 2 && v.length < 4) {
      newV = `${newV.substring(0, 2)}-${newV.substring(2)}`;
    } else if (v.length >= 4) {
      newV = `${newV.substring(0, 2)}-${newV.substring(2, 4)}-${newV.substring(
        4
      )}`;
    }
    if (newV[newV.length - 1] === `-`) {
      newV = newV.substring(0, newV.length - 1);
    }
    return newV;
  };
  return (
    <>
      {field ? (
        <IonItem>
          <IonLabel>
            <p style={{ textTransform: `capitalize` }}>{t(field.label)}</p>
          </IonLabel>
          {field.type === "date" ? (
            <>
              {/* <IonDatetimeButton datetime="datetime"></IonDatetimeButton>

							<IonModal keepContentsMounted={true}>
								<IonDatetime
									id="datetime"
									presentation="date"
									onIonChange={(e) => setField(e.detail.value!, { ...field })}
								></IonDatetime>
							</IonModal> */}
              <IonInput
                type="text"
                inputmode={field.inputmode}
                placeholder={field.placeholder}
                value={field.value}
                onIonChange={(e) => {
                  const v = e.detail.value!.replaceAll(`-`, ``);
                  // set field
                  setField(handleDobValue(v), { ...field });
                }}
              />
              <IonIcon
                icon={field.isValid ? checkmarkCircle : closeCircleSharp}
                color={field.isValid ? `success` : `danger`}
                slot="end"
                size="small"
              />
            </>
          ) : (
            <>
              <IonInput
                type={
                  field.id === `password` || field.id === "retype"
                    ? showPw
                      ? `text`
                      : field.type
                    : field.type
                }
                autocomplete={field.autocomplete}
                placeholder={field.placeholder}
                inputmode={field.inputmode}
                required
                readonly={readOnly}
                color={
                  field.isValid
                    ? `dark`
                    : field.value.length === 0
                    ? `dark`
                    : `danger`
                }
                style={{
                  fontSize: "14px",
                }}
                value={field.value}
                onIonChange={(e) => setField(e.detail.value!, { ...field })}
              />
              {field.name === "password" ? (
                <IonButton
                  slot="end"
                  color={showPw ? `secondary` : `medium`}
                  fill="clear"
                  onClick={() => {
                    setShowPw(!showPw);
                  }}
                >
                  <IonIcon icon={showPw ? eye : eyeOffOutline} />
                </IonButton>
              ) : (
                <IonIcon
                  icon={field.isValid ? checkmarkCircle : closeCircleSharp}
                  color={field.isValid ? `success` : `danger`}
                  slot="end"
                  size="small"
                />
              )}
            </>
          )}
        </IonItem>
      ) : null}
    </>
  );
};

const Signup: React.FC = () => {
  const { t } = useTranslation();
  const [showPw, setShowPw] = useState(false);
  const [disableSubmit, setDisableSubmit] = useState(true);
  const [fields, setFields] = useState<Field[]>([...defaultFields]);
  const [saving, setSaving] = useState(false);
  const [showToast, setShowToast] = useState(false);
  const history = useHistory();

  const handleUpdate = (value: any, field: Field) => {
    // find the right field
    let tempFields = [...fields];
    let targetField = tempFields.find((x) => x.id === field.id);
    const pwField = tempFields.find((x) => x.id === `password`);
    if (!targetField || !pwField) {
      return;
    }
    targetField.value = value;
    targetField.isValid =
      targetField.id === `retype`
        ? targetField.validate.test(value) &&
          targetField.value === pwField.value
        : targetField.validate.test(value);
    setFields(tempFields);
  };

  useEffect(() => {
    setDisableSubmit(fields.filter((x) => !x.isValid).length > 0);

    return () => {
      setDisableSubmit(true);
    };
  }, [fields]);

  const handleSubmit = async () => {
    setSaving(true);

    let payload: any = {};
    fields.forEach((f) => {
      payload[`${f.id}`] = f.value;
    });
    const res = await signup(payload);
    if (res === "success") {
      setSaving(false);
      history.push(`/`);
      return;
    }
    if (String(res).includes(`auth/email-already-in-use`)) {
      setSaving(false);
      setShowToast(true);
    }
  };

  return (
    <>
      <IonPage>
        <Header />
        <IonContent className="ion-text-center fluid">
          <br />
          <br />
          <IonLabel>
            <h1>
              <b>{t(`Sign Up`)}</b>
            </h1>
          </IonLabel>
          <IonCard color={`light`} className="ion-text-start">
            <IonCardHeader>
              <IonCardSubtitle>
                <IonLabel color={`danger`}>
                  {t(`All Fields are required.`)}
                </IonLabel>
              </IonCardSubtitle>
            </IonCardHeader>
            <IonCardContent className="ion-no-padding">
              <IonList inset>
                <FieldItem
                  field={fields.find((x) => x.name === "email")}
                  showPw={showPw}
                  setShowPw={setShowPw}
                  setField={(value, field) => handleUpdate(value, field)}
                  readOnly={saving}
                />
              </IonList>

              <IonList inset>
                <FieldItem
                  field={fields.find((x) => x.name === "password")}
                  showPw={showPw}
                  setShowPw={setShowPw}
                  setField={(value, field) => handleUpdate(value, field)}
                  readOnly={saving}
                />
                <FieldItem
                  field={fields.find((x) => x.name === "retype")}
                  showPw={showPw}
                  setShowPw={setShowPw}
                  setField={(value, field) => handleUpdate(value, field)}
                  readOnly={saving}
                />
              </IonList>
              <p
                className="ion-padding-start ion-padding-end ion-padding-bottom ion-text-start"
                style={{
                  color:
                    fields.find((x) => x.id === "retype") &&
                    !fields.find((x) => x.id === "retype")?.isValid
                      ? `var(--ion-color-danger)`
                      : `var(--ion-color-medium)`,
                }}
              >
                {fields.find((x) => x.id === "password") &&
                !fields.find((x) => x.id === "password")?.isValid
                  ? `${t(
                      `passwordRestriction`
                    )} (${"!, @, #, $, %, &, *, ~, =, _ "})`
                  : fields.find((x) => x.id === "retype") &&
                    !fields.find((x) => x.id === "retype")?.isValid
                  ? t(`Retype Password again`)
                  : t(`Nice Password!`)}
              </p>

              <IonList inset>
                <FieldItem
                  field={fields.find((x) => x.name === "usrname")}
                  showPw={showPw}
                  setShowPw={setShowPw}
                  setField={(value, field) => handleUpdate(value, field)}
                  readOnly={saving}
                />

                <FieldItem
                  field={fields.find((x) => x.name === "phone")}
                  showPw={showPw}
                  setShowPw={setShowPw}
                  setField={(value, field) => handleUpdate(value, field)}
                  readOnly={saving}
                />
                <FieldItem
                  field={fields.find((x) => x.name === "dob")}
                  showPw={showPw}
                  setShowPw={setShowPw}
                  setField={(value, field) => handleUpdate(value, field)}
                  readOnly={saving}
                />
              </IonList>
              <IonGrid className="ion-text-center">
                <IonRow>
                  <IonCol>
                    <IonLabel color={"medium"}>
                      <p>
                        {t(
                          `We will need the above details to identify you when you visit us, so make sure they are correct!`
                        )}
                      </p>
                    </IonLabel>
                  </IonCol>
                </IonRow>
              </IonGrid>
              <IonButton
                className="ion-margin"
                color={disableSubmit ? `medium` : `success`}
                expand="block"
                disabled={disableSubmit || saving}
                onClick={() => {
                  handleSubmit();
                }}
              >
                {saving ? (
                  <IonSpinner />
                ) : (
                  <>
                    <IonIcon
                      icon={
                        disableSubmit ? alertCircleOutline : checkmarkCircle
                      }
                      slot="start"
                    />
                    <IonLabel>
                      <b>{t(`submit`)}</b>
                    </IonLabel>
                  </>
                )}
              </IonButton>
            </IonCardContent>
          </IonCard>
        </IonContent>
        <IonToast
          isOpen={showToast}
          onDidDismiss={() => {
            setShowToast(false);
          }}
          duration={8000}
          message={`${t(`Email already registered.`)}`}
          color="warning"
          buttons={[
            {
              side: "end",
              icon: logInOutline,
              text: `${t(`login`)}`,
              handler: () => {
                history.push(`/login`);
              },
            },
          ]}
        />
      </IonPage>
    </>
  );
};

export default Signup;
