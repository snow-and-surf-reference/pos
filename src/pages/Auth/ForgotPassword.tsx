import {
    IonButton,
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardSubtitle,
    IonCol,
    IonContent,
    IonGrid,
    IonIcon,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonPage,
    IonRow
} from "@ionic/react";
import { checkmarkCircleOutline, mailOpenOutline, mailOutline } from "ionicons/icons";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useLocation } from "react-router";
import { sendPwEmail } from "../../app/firebase/global";
import Header from "../../components/Global/Header";

const ForgotPassword: React.FC = () => {
    const { t } = useTranslation();
	const [status, setStatus] = useState<"idle" | "sending" | "sent">("idle");
    const [email, setEmail] = useState("");
    const [disabledSubmit, setDisableSubmit] = useState(true);
    const location = useLocation();

    useEffect(() => {
        const updateQuery = () => {
            const search = location.search;
            const email = new URLSearchParams(search).get('email');
            if (email) {
                setEmail(email);
            }
        }
        updateQuery();
    }, [location])

    useEffect(() => {
        setDisableSubmit(!/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(email));
    }, [email])

    const handleSubmit = async () => {
        setStatus('sending');
        const res = await sendPwEmail(email);
        if (res === 'success') {
            setStatus('sent');
            return
        }
    }
    
	return (
		<IonPage>
			<Header />
            <IonContent className="fluid">
                <br/>
				<IonGrid>
					<IonRow>
						<IonCol>
							<IonLabel className="ion-text-center">
								<h2>
                                    <b>{t(`Forgot Password`)}</b>
								</h2>
							</IonLabel>
						</IonCol>
					</IonRow>
                </IonGrid>
                {
                    status !== 'sent' ?
                        <>
                        				<IonCard color={"light"}>
					<IonCardHeader>
                                    <IonCardSubtitle>{t(`Please enter your email`)}:</IonCardSubtitle>
					</IonCardHeader>
					<IonCardContent className="ion-no-padding">
						<IonList inset>
							<IonItem>
								<IonIcon
									icon={mailOpenOutline}
									size="small"
									slot="start"
								></IonIcon>
                                <IonInput type="email" autocomplete="email" inputmode="email" value={email}
                                    onIonChange={(e) => {
                                        setEmail(e.detail.value!)
                                    }}
                                />
							</IonItem>
                        </IonList>
                        <IonButton
                            color={disabledSubmit ? `medium` : `success`}
                            disabled={disabledSubmit || status === 'sending'}
                            className='ion-margin'
                            expand="block"
                            onClick={() => {
                                handleSubmit();
                            }}
                        >
                            <IonIcon icon={mailOutline} size='small' slot='start' />
                                        <IonLabel><b>{t(`Send Link`)}</b></IonLabel>
                        </IonButton>
					</IonCardContent>
				</IonCard>
                        </>
                        :
                        <>
                            <IonCard color={`dark`}>
                                <IonCardContent>
                                    <IonList lines="none" color="clear" className="ion-no-padding">
                                        <IonItem color={`dark`} className="ion-no-padding">
                                            <IonIcon icon={checkmarkCircleOutline} size='large' slot="start" />
                                            <IonLabel>
                                                <h2><b>{t(`Sent!`)}</b></h2>
                                            </IonLabel>
                                        </IonItem>
                                    </IonList>
                                    <br/>
                                    <p>{t(`An email with reset password link has been sent to`)}{` `}<b>{email}</b>.</p>
                                    <br/>
                                    <p>{t(`Please check your inbox and spam folder.`)}</p>
                                </IonCardContent>
                            </IonCard>
                        </>
                }

			</IonContent>
		</IonPage>
	);
};

export default ForgotPassword;
