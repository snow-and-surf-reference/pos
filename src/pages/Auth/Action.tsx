import {
	IonButton,
	IonCard,
	IonCardContent,
	IonCardHeader,
	IonContent,
	IonIcon,
	IonInput,
	IonItem,
	IonLabel,
	IonList,
	IonPage,
	IonSpinner,
} from "@ionic/react";
import { eye, eyeOffOutline } from "ionicons/icons";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useHistory, useLocation } from "react-router";
import {
	checkResetPwCode,
	emailVerifying,
	logout,
	resetPassword,
} from "../../app/firebase/global";
import Header from "../../components/Global/Header";

const pwRegExp = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,24}$/;

const Action: React.FC = () => {
	const location = useLocation();
	const [mode, setMode] = useState<string>("");
	const [code, setCode] = useState<string>("");
	const [resetPwEmail, setResetPwEmail] = useState("");
	const [newPw, setNewPw] = useState("");
	const [retype, setRetype] = useState("");
	const [showPw, setShowPw] = useState(false);
	const [newPwDisableSubmit, setNewPwDisableSubmit] = useState(true);
	const [pwUpdateStatus, setPwUpdateStatus] = useState<
		"idle" | "updating" | "updated"
	>("idle");
	const [verified, setVerified] = useState<"verifying" | "success" | "failed">(
		"verifying"
	);
	const history = useHistory();

	useEffect(() => {
		const handleAction = async () => {
			const search = location.search;
			const mode = new URLSearchParams(search).get("mode");
			const code = new URLSearchParams(search).get("oobCode");

			if (!mode || !code) {
				return;
			}
			setMode(mode);
			setCode(code);
			if (mode === "verifyEmail") {
				const res = await emailVerifying(String(code));
				if (res === "success") {
					setVerified("success");
					setTimeout(() => {
						window.location.assign(String(process.env.REACT_APP_URL))
					}, 2000);
				} else {
					setVerified("failed");
				}
				return;
			}

			if (mode === "resetPassword") {
				const res: any = await checkResetPwCode(code);
				if (res.data.email) {
					setResetPwEmail(res.data.email);
				}
				return;
			}
			return;
		};
		handleAction();
	}, [location]);

	useEffect(() => {
		if (newPw && pwRegExp.test(newPw) && newPw === retype) {
			setNewPwDisableSubmit(false);
		} else {
			setNewPwDisableSubmit(true);
		}
	}, [newPw, retype]);

	const handleSubmitNewPw = async () => {
		setPwUpdateStatus("updating");
		const res = await resetPassword(code, newPw);
		if (res === "success") {
			setPwUpdateStatus("updated");
			setTimeout(async () => {
				await logout();
				history.push(`/login`);
			}, 4000);
		} else {
			console.log(res);
		}
	};

	const { t } = useTranslation();

	return (
		<IonPage>
			<Header />

			<IonContent className="fluid">
				{!mode ? (
					<IonCard color="light">
						<IonCardContent className="ion-text-center">
							<IonLabel>
								<h3>
									<b>{t(`loading`)}</b>
								</h3>
							</IonLabel>
							<IonSpinner name="dots" />
						</IonCardContent>
					</IonCard>
				) : mode === "verifyEmail" ? (
					<IonCard color="light">
						<IonCardContent>
							<IonLabel>
								<h1>
									<b>{t(`Email Verifying`)}</b>
								</h1>
								<br />
							</IonLabel>
							{verified === "verifying" ? (
								<IonSpinner />
							) : verified === "success" ? (
								<IonLabel>{t(`success`)}!!!</IonLabel>
							) : (
								<IonLabel>
									<b>{t(`Something is not right...`)}</b>
									{` `}
									{t(
										`Your link seems to be invalid! Please retry after your next loggin in.`
									)}
								</IonLabel>
							)}
						</IonCardContent>
					</IonCard>
				) : (
					<>
						{pwUpdateStatus !== "updated" ? (
							<IonCard color={"light"}>
								<IonCardHeader>
									<p>
										{t(`Reset password for`)} <b>{resetPwEmail}</b>
									</p>
								</IonCardHeader>
								<IonCardContent className="ion-no-padding">
									<IonList inset>
										<IonItem>
											<IonInput
												type={showPw ? "text" : "password"}
												value={newPw}
												inputmode="text"
												autocomplete="new-password"
												placeholder={t(`New Password`)}
												onIonChange={(e) => {
													setNewPw(e.detail.value!);
												}}
											/>
											<IonIcon
												icon={showPw ? eye : eyeOffOutline}
												color={showPw ? "secondary" : "medium"}
												size="small"
												onClick={() => {
													setShowPw(!showPw);
												}}
											/>
										</IonItem>
										<IonItem>
											<IonInput
												type={showPw ? "text" : "password"}
												value={retype}
												inputmode="text"
												autocomplete="new-password"
												placeholder={t(`Retype Password`)}
												onIonChange={(e) => {
													setRetype(e.detail.value!);
												}}
											/>
										</IonItem>
									</IonList>
									<p
										className="ion-padding-start ion-padding-end ion-padding-bottom ion-text-start"
										style={{ color: `var(--ion-color-danger)` }}
									>
										- {t(`Minimum 8 characters`)}
										<br />
										- 1 {t(`capital letter`)}
										<br />
										- 1 {t(`small letter`)}
										<br />
										- 1 {t(`number`)}
										<br />- 1 {t(`special character`)}
									</p>

									<IonButton
										expand="block"
										className="ion-margin"
										color={newPwDisableSubmit ? "medium" : "success"}
										disabled={newPwDisableSubmit}
										onClick={() => {
											handleSubmitNewPw();
										}}
									>
										{t(`submit`)}
									</IonButton>
								</IonCardContent>
							</IonCard>
						) : (
							<IonCard color={`light`}>
								<IonCardContent>
									<IonLabel>
										<h1>
														<b>{t(`Password Reset Successful!`)}</b>
										</h1>
													<p>{t(`Please login with the new password.`)}</p>
									</IonLabel>
								</IonCardContent>
							</IonCard>
						)}
					</>
				)}
			</IonContent>
		</IonPage>
	);
};

export default Action;
