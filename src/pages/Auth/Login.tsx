import {
	IonButton,
	IonCard,
	IonCardContent,
	IonCol,
	IonContent,
	IonGrid,
	IonIcon,
	IonInput,
	IonItem,
	IonLabel,
	IonList,
	IonPage,
	IonRow,
	IonToast
} from "@ionic/react";
import {
	createOutline,
	eye,
	eyeOffOutline,
	lockClosedOutline,
	logInOutline,
	mailOpenOutline
} from "ionicons/icons";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router";
import { login } from "../../app/firebase/global";
import Header from "../../components/Global/Header";

const emailRegExp = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;

const Login: React.FC = () => {
	const { t } = useTranslation();
	const [email, setEmail] = useState<string>("");
	const [password, setPassword] = useState<string>("");
	const [disableSubmit, setDisableSubmit] = useState<boolean>(true);
	const [showToast, setShowToast] = useState(false);
	const [showPw, setShowPw] = useState(false);
	const history = useHistory();

	useEffect(() => {
		if (
			email &&
			emailRegExp.test(email) &&
			password
		) {
			setDisableSubmit(false);
		} else {
			setDisableSubmit(true);
		}
	}, [email, password]);

	const handleLogin = async () => {
		const res = await login({ email, password });
		if (res === "success") {
			history.push("/");
		} else {
			setShowToast(true);
		}
	};

	return (
		<>
			<IonPage>
				<Header />
				<IonContent className="fluid">
					<br />
					<br />
					<IonLabel class="ion-text-center">
						<h1>
							<b>{t(`login`)}</b>
						</h1>
					</IonLabel>
					<IonCard color={`light`}>
						<IonCardContent class="ion-no-padding ion-text-center">
							<IonList inset>
								<IonItem>
									<IonIcon icon={mailOpenOutline} slot="start" size="small" />
									<IonInput
										type="email"
										placeholder={t(`Email`)}
										value={email}
										autocomplete="email"
										onIonChange={(e) => setEmail(e.detail.value!)}
									/>
								</IonItem>
								<IonItem>
									<IonIcon icon={lockClosedOutline} slot="start" size="small" />
									<IonInput
										type={showPw ? "text" : "password"}
										placeholder={t(`Password`)}
										value={password}
										autocomplete="current-password"
										onIonChange={(e) => setPassword(e.detail.value!)}
									/>
									<IonIcon
										slot="end"
										color={showPw ? "secondary" : "medium"}
										icon={showPw ? eye : eyeOffOutline}
										size="small"
										onClick={() => setShowPw(!showPw)}
									/>
								</IonItem>
							</IonList>
							<div className="ion-margin ion-text-start">
								<IonLabel
									color={`secondary`}
									onClick={() => {
										history.push(`/forgotPassword${emailRegExp.test(email) ? `?email=${email}` : ''}`)
									}}
									style={{ cursor: "pointer" }}
								>
									<span>{t(`Forgot Password`)}?</span>
								</IonLabel>
							</div>

							<IonButton
								class="ion-margin"
								color={disableSubmit ? `medium` : `success`}
								disabled={disableSubmit}
								onClick={() => {
									handleLogin();
								}}
							>
								<IonIcon icon={logInOutline} slot="start" />
								<IonLabel>
									<b>{t(`login`)}</b>
								</IonLabel>
							</IonButton>
						</IonCardContent>
					</IonCard>
					<IonGrid>
						<IonRow className="ion-justify-content-center ion-align-items-center">
							<IonCol className="ion-justify-content-center ion-align-items-center ion-text-center">
								<IonLabel className="ion-align-self-center ion-justify-self-center">
									<p>{t(`Don't have an Account?`)}</p>
								</IonLabel>
								<IonButton
									color={"warning"}
									fill="solid"
									className="ion-align-self-center ion-justify-self-center"
									onClick={() => {
										history.push(`/signup`);
									}}
								>
									<IonIcon icon={createOutline} slot="start" />
									<IonLabel>
										<b>{t(`Sign Up Now`)}</b>
									</IonLabel>
								</IonButton>
							</IonCol>
						</IonRow>
					</IonGrid>
				</IonContent>
				<IonToast
					isOpen={showToast}
					onDidDismiss={() => {
						setShowToast(false);
					}}
					duration={4000}
					message={`${t(`Something went wrong. Please retry.`)}`}
					color="danger"
				/>
			</IonPage>
		</>
	);
};

export default Login;
