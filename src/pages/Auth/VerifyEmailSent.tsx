import { IonButton, IonCard, IonCardContent, IonLabel } from "@ionic/react";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router";

const VerifyEmailSent: React.FC = () => {
	const { t } = useTranslation();
    const history = useHistory();

	return (
		<IonCard>
			<IonCardContent>
				<IonLabel>
					<h1>
						<b>{t(`Verify Email Sent!`)}</b>
					</h1>
					<br />
					<h2>
						{t(`Please Check your inbox, and make sure to check the 'Spam' folder as well!`)}
					</h2>
				</IonLabel>
				<br />
				<br />
                <IonButton color={`warning`} fill="solid"
                    onClick={() => {
                        history.replace(`/`);
                    }}
                >
					<IonLabel>
						<b>{t(`back`)}</b>
					</IonLabel>
				</IonButton>
			</IonCardContent>
		</IonCard>
	);
};

export default VerifyEmailSent;