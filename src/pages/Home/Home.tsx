import {
	IonContent, IonPage
} from "@ionic/react";
import randomColor from "randomcolor";
import { useEffect } from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router";
import { A11y, Autoplay, Navigation, Pagination, Parallax } from "swiper";
import "swiper/css";
import "swiper/css/autoplay";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/parallax";
import { Swiper, SwiperSlide } from "swiper/react";
import { useAppSelector } from "../../app/hooks";
import { selectPageContentByTitle } from "../../app/slices/pageContentsSlice";
import { selectHomepageSlides } from "../../app/slices/posContentSlice";
import Container from "../../components/Global/Container";
import Header from "../../components/Global/Header";
import LoadingCard from "../../components/Global/LoadingCard";
import SlideContainer from "../../components/Global/SlideContainer";
import i18n from "../../i18n";
import "./Home.scss";

const Home: React.FC = () => {
	const slides = useAppSelector(selectHomepageSlides);
	const history = useHistory();
	const translate = useTranslation();
	const pageContent = useAppSelector(selectPageContentByTitle("home"));
	useEffect(() => {}, [translate]);

	return (
		<IonPage>
			<Header />
			<IonContent>
				<Swiper
					modules={[Navigation, Pagination, Autoplay, Parallax, A11y]}
					spaceBetween={0}
					slidesPerView={1}
					pagination={{ clickable: true }}
					autoplay={{
						delay: 5000,
						// pauseOnMouseEnter: true,
						disableOnInteraction: true,
					}}
					parallax={true}
				>
					{[...slides]
						.sort((a, b) => a.index - b.index)
						.map((x, idx) => (
							<SwiperSlide
								key={`slide-${idx}`}
								className="slide"
								style={{
									backgroundColor: `${randomColor()}`,
									color: `#ffffff`,
									// backgroundImage: `url("https://picsum.photos/2560/1440?random=${idx}")`,
								}}
							>
								{x.mediaType === "contentImages" ? (
									<img
										className="slide-bg slide-bg-image"
										src={x.media}
										alt={`Snow & Surf`}
									/>
								) : (
									<video
										className="slide-bg slide-bg-video"
										loop
										autoPlay
										muted
										playsInline
									>
										<source src={x.media} type="video/mp4"></source>
									</video>
								)}
								<SlideContainer>
									<h1 className="title" data-swiper-parallax="-100">
										{i18n.language === "en"
											? x.title.en
											: x.title.zh || x.title.en}
									</h1>
									{x.subtitle.en ? (
										<h2 className="subtitle" data-swiper-parallax="-200">
											{i18n.language === "en"
												? x.subtitle.en
												: x.subtitle.zh || x.subtitle.en}
										</h2>
									) : null}
									{x.desc.en ? (
										<>
											<div
												className="text desc"
												data-swiper-parallax="-300"
												data-swiper-parallax-duration="600"
											>
												<p>
													{i18n.language === "en"
														? x.desc.en
														: x.desc.zh || x.desc.en}
												</p>
											</div>
										</>
									) : null}
									{x.buttonText ? (
										<button
											onClick={() => {
												x.isExternalLink
													? window.open(x.externalUrl)
													: x.externalUrl && history.push(x.externalUrl);
											}}
										>
											{i18n.language === "en"
												? x.buttonText.en
												: x.buttonText.zh || x.buttonText.en}
										</button>
									) : null}
								</SlideContainer>
							</SwiperSlide>
						))}
				</Swiper>
				{!pageContent ? (
					<LoadingCard />
				) : (
					<Container>
						<div
							className="ion-padding"
							dangerouslySetInnerHTML={{
								__html:
									i18n.language === "en"
										? pageContent?.content.en
										: pageContent?.content.zh || pageContent?.content.en,
							}}
						></div>
					</Container>
				)}
			</IonContent>
		</IonPage>
	);
};

export default Home;
