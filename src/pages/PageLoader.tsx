import { IonContent, IonPage } from "@ionic/react";
import { useEffect, useRef } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router";
import { useAppSelector } from "../app/hooks";
import { selectPageContentByTitle } from "../app/slices/pageContentsSlice";
import Container from "../components/Global/Container";
import Header from "../components/Global/Header";
import LoadingCard from "../components/Global/LoadingCard";
import "./PageLoader.scss";
import i18n from "../i18n";
import randomColor from "randomcolor";

const PageLoader: React.FC = () => {
	const { title } = useParams<{ title: string }>();
	const pageContent = useAppSelector(selectPageContentByTitle(title));
	const imgRef = useRef<HTMLImageElement | null>(null);
	const translate = useTranslation();
	useEffect(() => {}, [translate]);

	useEffect(() => {
		if (imgRef.current) {
			imgRef.current.classList.remove("show");
		}
	}, [pageContent]);

	const handleImgLoaded = (element: HTMLImageElement) => {
		element.classList.add("show");
	};

	return (
		<IonPage>
			<Header />
			<IonContent>
				{!pageContent ? (
					<LoadingCard />
				) : (
					<>
						<div
							className="page-banner"
							style={{
								backgroundColor: `${randomColor({luminosity: 'dark'})}`,
								color: `#ffffff`,
							}}
						>
							{pageContent.mediaType === "contentImages" ? (
								<img
									className="page-banner-bg-image"
									ref={imgRef}
									src={pageContent.media}
									alt={`Snow & Surf`}
									onLoad={(e) => {
										handleImgLoaded(e.currentTarget);
									}}
								/>
							) : pageContent.mediaType === "videos" ? (
								<video
									className="page-banner-bg-video show"
									loop
									autoPlay
									muted
									playsInline
								>
									<source
										src={pageContent.media}
										type="video/mp4"
									></source>
								</video>
									) : (
											<h1>{i18n.language === 'en' ? pageContent.title.en : (pageContent.title.zh || pageContent.title.en)}</h1>
							)}
						</div>
						<Container>
							<div
								className="ion-padding"
								dangerouslySetInnerHTML={{
									__html:
										i18n.language === "en"
											? pageContent?.content.en
											: pageContent?.content.zh || pageContent?.content.en,
								}}
							></div>
						</Container>
					</>
				)}
			</IonContent>
		</IonPage>
	);
};

export default PageLoader;
