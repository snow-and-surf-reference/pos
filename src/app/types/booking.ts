export interface Gear{
    name: string;
    unit: 'cm' | 'eu' | 'size';
    checked: boolean;
    size?: null | number | string;
}

export interface SessionGear {
	type: string;
	gears: Gear[];
}

export interface GearSize{
    gear: Gear;
    size: number | string;
}