import * as UserTypes from '@tsanghoilun/snow-n-surf-interface/types/user';
import Chance from 'chance';
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

var chance = new Chance();

export const coachDemo: UserTypes.Coach = {
    id: 'new',
    usrname: `${chance.first()} ${chance.last()}`,
    // dob: dayjs.tz(`1989-6-4`).toDate(),
    email: `${chance.email()}`,
    phone: `${chance.phone()}`,
    createdAt: dayjs().toDate(),
    enabled: true,
    // avatar: '',
    role: 'coach',
    coachType: [`ski`],
    pricingOffset: 0,
    allowance: 0,
    wallet: {
        balance: 0,
        spent: 0
    }
}

