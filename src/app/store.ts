import { configureStore } from "@reduxjs/toolkit";
import authUserSlice from "./slices/authUserSlice";
import customerClassesSlice from "./slices/customerClassesSlice";
import faqSlice from "./slices/faqSlice";
import gearSlice from "./slices/gearSlice";
import posContentSlice from "./slices/posContentSlice";
import sessionsSlice from "./slices/sessionsSclice";
import topupPackagesSlice from "./slices/topupPackages";
import venueSlice from "./slices/venueSlice";
import groupClassSlice from "./slices/groupClassesSlice";
import pricingSlice from "./slices/pricingSlice";
import pageContentsSlice from "./slices/pageContentsSlice";
import otherContentsSlice from "./slices/otherContentsSlice";

export const store = configureStore({
	reducer: {
		authUser: authUserSlice,
		sessions: sessionsSlice,
		venue: venueSlice,
		gears: gearSlice,
		faq: faqSlice,
		customerClasses: customerClassesSlice,
		topupPackages: topupPackagesSlice,
		posContent: posContentSlice,
		groupClass: groupClassSlice,
		pricing: pricingSlice,
		pageContents: pageContentsSlice,
		otherContents: otherContentsSlice
	},
	middleware: (getDefaultMiddleware) =>
		getDefaultMiddleware({
			serializableCheck: false,
		}),
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
