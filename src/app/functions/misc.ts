import dayjs from 'dayjs';
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";

// import json and type for ph
import hkph from './hkph.json';

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

export interface PhObj {
    start: Date;
    end: Date;
}

export const getHkph = () => {
    const ph2raw = hkph.vcalendar[0].vevent;
    let ph2: PhObj[] = [];
    ph2raw.forEach(x => {
        const start = dayjs.tz(dayjs(String(x.dtstart[0]))).toDate();
        const end = dayjs.tz(dayjs(String(x.dtend[0]))).toDate();
        ph2.push({ start, end } as PhObj);
    });

    return ph2;
}