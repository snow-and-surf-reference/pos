import * as CoachTypes from "@tsanghoilun/snow-n-surf-interface/types/coach";
import * as SessionsTypes from "@tsanghoilun/snow-n-surf-interface/types/session";
import * as VenueTypes from "@tsanghoilun/snow-n-surf-interface/types/venue";
import * as GearTypes from "@tsanghoilun/snow-n-surf-interface/types/gear";
import { GroupClassSlot } from "@tsanghoilun/snow-n-surf-interface/types/groupClass";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import { store } from "../store";
import Holidays from "date-holidays";
import { getHkph } from "./misc";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

var hd = new Holidays();
hd.init(`HK`);

export interface TimeSlot {
    time: number;
    slots: number;
    checked: boolean;
}
export interface SlotsRawMaterials {
    caps: VenueTypes.Capacities;
    counters: SessionsTypes.MonthCounters;
    coachSchedules: CoachTypes.CoachSchedule[];
    sessionType: SessionsTypes.SessionType;
    maintenences?: VenueTypes.MaintenanceSession[];
    playType: SessionsTypes.PlayType;
    sessionDate: Date;
    sessionStart: Date | null;
    sessionEnd: Date | null;
	isHalfPast: boolean;
	allClassSlots: GroupClassSlot[];
}

export interface GroupClassSlotsMaterials {
    caps: VenueTypes.Capacities;
    counters: SessionsTypes.MonthCounters;
    maintenences?: VenueTypes.MaintenanceSession[];
    groupClassSlots: GroupClassSlot[];
    recurringSlots: GroupClassSlot[];
    sessionDate: Date;
}

export interface SelectGroupClassSlot {
    paxSlots: number;
    class: GroupClassSlot;
}

export const handleSlots = (materials: SlotsRawMaterials) => {
    const { caps, counters, coachSchedules, sessionType, playType, sessionDate, sessionEnd, sessionStart, isHalfPast, maintenences, allClassSlots } = materials;

    // create slots based on opening hours and counters
    const day = dayjs.tz(sessionDate).day();

    // check if the target date is weekday or weeekend
    const isWeekend = day === 0 || day === 6;

    // get the holidays of the year of booking date
    const todayJs = dayjs.tz(sessionDate);
    // const ph = hd.getHolidays(todayJs.format(`YYYY`));
    // const isHoliday = ph.filter((x) => todayJs.valueOf() >= x.start.valueOf() && todayJs.valueOf() < x.end.valueOf()).length > 0;

    const ph2 = getHkph();
    const isHoliday = ph2.filter((x) => todayJs.valueOf() >= x.start.valueOf() && todayJs.valueOf() < x.end.valueOf()).length > 0;

    // get the right opening hours
    const openingHours = isWeekend || isHoliday ? { ...caps.hours.weekend } : { ...caps.hours.weekday };
    
    // get the right capacity of the day
    const capacity = caps.capacities
        .filter((x) =>
            playType === `snow` || playType === "ski" || playType === "sb" ? x.playType === "ski" : playType === "surf" ? x.playType === `surf` : true
        )
        .reduce((a, b) => a + b.max, 0);

    if (!capacity) {
        return;
    }

    // if existing selection already been made, checked them
    let existingStart = null,
        existingEnd = null;
    if (sessionStart || sessionEnd) {
        existingStart = dayjs.tz(sessionStart).valueOf();
        existingEnd = dayjs.tz(sessionEnd).valueOf();
    }

    // get the sessions logs from this month's counter
    const thisMonthSessions: SessionsTypes.MonthSession[] = counters.sessions;

    const tempSlots: TimeSlot[] = [];

    // console.log("--- start of 1 day schedule ---", session.sessionType);
    for (let i = openingHours.open; i < openingHours.close; i++) {
        const extraMin = isHalfPast ? 30 : 0;

        // get coach schedule where available for that hour
        const thisTime = dayjs.tz(sessionDate).startOf("day").add(i, "hours").add(extraMin, "minutes").valueOf();
        const thisTimeEnd = dayjs
            .tz(sessionDate)
            .startOf("day")
            .add(i + 1, "hours")
            .add(extraMin, "minutes")
            .subtract(1, "second")
			.valueOf();
		
		// remove the coaches that has been assigned to classes on the hour
		const hourClasses = allClassSlots.filter(x => {
			const classStart = dayjs.tz(`${dayjs.tz(sessionDate).format(`YYYY-MM-DD`)}T${x.start}`);
			const classEnd = classStart.add(1, `hour`).subtract(1, 'minute');
			return (thisTime >= classStart.valueOf() && thisTime <= classEnd.valueOf()) || (thisTimeEnd >= classStart.valueOf() && thisTimeEnd <= classEnd.valueOf());
		});

        // coach schedule needs to include both
        // start and end time hour in order to consider coach available
        const hourCoachSchedules: CoachTypes.CoachSchedule[] = coachSchedules.filter((x) => {
            const scheduleStart = dayjs.tz(`${dayjs.tz(sessionDate).format(`YYYY-MM-DD`)}T${x.start}`).valueOf();
            const scheduleEnd = dayjs.tz(`${dayjs.tz(sessionDate).format(`YYYY-MM-DD`)}T${x.end}`).valueOf();
			const isValid = thisTime >= scheduleStart && thisTimeEnd <= scheduleEnd;
			const isNotInClass = hourClasses.filter(c => c.coach?.id === x.coach.id).length <= 0;
			const targetPlayType = playType === 'all' || playType === 'snow' || playType === 'surf' || playType === 'ski' ? 'ski' : 'sb';
			const isRightType = sessionType === 'privateBelt' ? true : sessionType !== 'privateClass' ? false : x.coach.coachType.includes(targetPlayType);
            return isValid && isNotInClass && isRightType;
        });

        let totalCoach = hourCoachSchedules.length - 0;
        let slots: number = capacity - 0;
        let privateBelt = false;
        let privateLane = false;
        let fullSnow = false;
        let fullSurf = false;
        let fullPark = false;
        
        hourClasses.forEach(hc => {
            if (playType === 'surf') {
                return
            }
            slots -= hc.maxPax;
        })

        // filter out existing sessions with the right game types
        let hourSessions: SessionsTypes.MonthSession[] = thisMonthSessions.filter((x) => {

            //filter out gruop class session as it has its own deduction
            if (x.groupClass) { return false };

            const counterStart = dayjs.tz(x.start).valueOf();
            const counterEnd = dayjs.tz(x.end).subtract(1, "minute").valueOf();

            // counters needs to be included when either
            // session start or end are within the counter range
            const isValidTime = (thisTime >= counterStart && thisTime <= counterEnd) || (thisTimeEnd >= counterStart && thisTimeEnd <= counterEnd);
            const isSnow = x.playType === "snow" || x.playType === "ski" || x.playType === "sb" || x.playType === "all";
            const isSurf = x.playType === "surf" || x.playType === "all";
            const isVenue =
                playType === "snow" || playType === "ski" || playType === "sb"
                    ? isSnow
                    : playType === "surf"
                    ? isSurf
                    : (playType === "all") === true;
            return isValidTime && isVenue;
        });

        const privateBeltCap = caps.capacities.find((x) => x.venue.name === "Slope 1")?.max || 0;
        const privateLaneCap = caps.capacities.find((x) => x.venue.name === "Surf Lane 1")?.max || 0;

        // handle maintenences
        if (maintenences) {
            const purePlayType: SessionsTypes.PlayType = playType === "snow" ? "ski" : playType === "sb" ? "ski" : playType;
            const effectedMaintenences = maintenences.filter((x) => {
                if (!x.start || !x.end) {
                    return false;
                }
                let effectedPlayTypes: SessionsTypes.PlayType[] = [];
                x.venue.forEach((pt) => {
                    effectedPlayTypes.push(pt.playType);
                });
                const isInTime = thisTime >= x.start.valueOf() && thisTime <= x.end.valueOf();
                const isType = purePlayType === "all" ? true : effectedPlayTypes.includes(purePlayType);
                return isInTime && isType;
            });
            
            if (effectedMaintenences.length > 0) {
                effectedMaintenences.forEach(m => {
                    m.venue.forEach(v => {
                        const count = caps.capacities.find(cc => cc.venue.name === v.venue.name && cc.playType === purePlayType)?.max;

                        // to block private belt booking if slope 1 is in maintenance
                        const isSlope1 = m.venue.find(v => v.venue.name === 'Slope 1');
                        if (isSlope1) {
                            privateBelt = true;
                        }

                        if (!count) {
                            return
                        }
                        slots -= count;
                    })
                })
            }
        }

        // update total coach and slots accordingly
        hourSessions.forEach((hs) => {
            const sessionType = hs.sessionType;
            switch (sessionType) {
                case `single`:
                    // single should take out booking pax only
                    slots -= hs.pax;
                    break;
                case `groupClass`:
                    // group class needs to take out class booked pax and slots
                    slots -= hs.pax;
                    break;
                case `privateClass`:
                    // group class needs to take out 1 coach and pax for slots
                    slots -= hs.pax;
                    totalCoach -= 1;
                    break;
                case `privateBelt`:
                    // private snow belt should take out 6 slots, and marked
                    privateBelt = true;
                    slots -= privateBeltCap;
                    totalCoach -= 1;
                    break;
                case `privateLane`:
                    privateLane = true;
                    slots -= privateLaneCap;
                    break;
                case `fullSnow`:
                    fullSnow = true;
                    slots = 0;
                    break;
                case `fullSurf`:
                    fullSurf = true;
                    slots = 0;
                    break;
                case `fullPark`:
                    fullPark = true;
                    slots = 0;
                    break;

                default:
                    break;
            }
        });

        if (sessionType === "privateClass") {
            if (totalCoach <= 0) {
                slots = 0;
            }
        }

        if (sessionType === "privateBelt") {
            if (privateBelt || fullSnow || fullPark || slots < 4 || totalCoach <= 0
                // || hourClasses.length > 1
            ) {
                slots = 0;
            }
        }

        if (sessionType === "privateLane") {
            if (privateLane || fullSurf || fullPark || slots < 5) {
                slots = 0;
            }
        }

        if (sessionType === "fullSnow") {
            if (privateBelt || fullSnow || fullPark || slots < 6 || hourClasses.length) {
                slots = 0;
            }
        }

        if (sessionType === "fullSurf") {
            if (privateLane || fullSurf || fullPark || slots !== capacity) {
                slots = 0;
            }
        }

        if (sessionType === `fullPark`) {
            if (slots !== capacity || hourClasses.length) {
                slots = 0;
            }
        }

        if (!(isHalfPast && i === openingHours.close - 1)) {
            let tempSlot: TimeSlot = {
                time: thisTime,
                slots,
                checked: existingStart && existingEnd ? thisTime >= existingStart && thisTime < existingEnd && slots > 0 : false,
            };
            tempSlots.push(tempSlot);
        }
    }
    // console.log("--- end of 1 day schedule ---");

    const daySlots = tempSlots.reduce((a, b) => a + b.slots, 0) / (capacity * (openingHours.close - openingHours.open));

    return {
        tempSlots,
        daySlots,
    };
};

export const handleGearsStock = (allGears: GearTypes.Gear[], thisMonthCounter: SessionsTypes.MonthCounters, session: SessionsTypes.Session, isPreBook: boolean = true) => {
    // check if session has no start end time, return full gears;
    if (!session.start || !session.end) {
        return allGears;
    }

    let rentedBookingGears: SessionsTypes.BookingGear[] = [];
    const thisTimeSessions = thisMonthCounter.sessions.filter((x) => {
        const counterStart = dayjs.tz(x.start).valueOf();
        const counterEnd = dayjs.tz(x.end).add(29, "minutes").valueOf();
        const thisTime = dayjs.tz(session.start).valueOf();
        const thisTimeEnd = dayjs.tz(session.end).subtract(1, "minute").valueOf();

        // counters needs to be included when either
        // session start or end are within the counter range
        const isValidTime = (thisTime >= counterStart && thisTime <= counterEnd) || (thisTimeEnd >= counterStart && thisTimeEnd <= counterEnd);
        return isValidTime;
    });

    thisTimeSessions.forEach((x) => {
        x.gears.forEach((gg) => {
            rentedBookingGears.push(gg);
        });
    });
    let gearsFromCurrentSession = [...session.gears];

    let tempAllGears = [...allGears];

    tempAllGears.forEach((gear, idx) => {
        let checkedStock: GearTypes.GearStock[] = [];
        gear.stock.forEach((st) => {
            const qty = st.qty;
            const rentedCount = rentedBookingGears.filter((r) => r.class === st.class && r.size === st.size && r.gear.id === gear.id).length;
            const fromCurrentCount = gearsFromCurrentSession.filter(
                (r) => r.class === st.class && r.size === st.size && r.gear.id === gear.id
            ).length;
            
            const finalQty = qty - rentedCount - (isPreBook ? fromCurrentCount : 0);

            checkedStock.push({
                class: st.class,
                size: st.size,
                qty: finalQty,
            });
            // if (gear.name === 'Ski Boots' && st.size === 28) {
            //     console.log(`--- Check ---`);
            //     console.log(`qty`, qty);
            //     console.log(`rentedCount`, rentedCount);
            //     console.log(`fromCurrentCount`, fromCurrentCount);
            //     console.log(`finalQty`, finalQty);
            // }
        });

        const sizedGear = Object.assign({ ...gear }, { stock: checkedStock });

        tempAllGears[idx] = sizedGear;
    });

    return tempAllGears;
};

export const checkAvailability = (
    session: SessionsTypes.Session,
    monthCounters: SessionsTypes.MonthCounters,
	coachSchedules: CoachTypes.CoachSchedule[],
	groupClassSlots: GroupClassSlot[],
    recurringSlots: GroupClassSlot[],
    isPreBook: boolean = true
) => {
    const caps: VenueTypes.Capacities | undefined = store.getState().venue.capacities;
    if (!caps) {
        return {
            result: false,
            message: `Cannot Retreive latest availability`,
        };
	}
	
	let timeSlots;
	let classSlots;

	// session is not for group class, use the normal check
	const { sessionType, playType, sessionDate } = session;

    if (!session.groupClass) {
        // use the handle slot function to get the slots for the day first
        const allClassSlots = groupClassSlots.concat(recurringSlots).filter(x => {
            const rStartDate = x.startDate || dayjs.tz(dayjs()).startOf('day');
            const rEndDate = x.endDate || dayjs.tz(`2099-12-31`).startOf('day');
            const isValidDate = rStartDate.valueOf() <= session.sessionDate.valueOf() && rEndDate.valueOf() >= session.sessionDate.valueOf();
            return isValidDate;
        });
        const materials: SlotsRawMaterials = {
            caps,
            counters: monthCounters,
            coachSchedules,
            sessionType,
            playType,
            sessionDate,
            sessionStart: session.start,
            sessionEnd: session.end,
			isHalfPast: dayjs(session.start).format(`mm`) === "30",
			allClassSlots
        };

        const slots = handleSlots(materials);
        if (!slots) {
            return {
                result: false,
                message: `Cannot Retreive latest availability`,
            };
        }
        timeSlots = slots.tempSlots;

        if (!timeSlots) {
            return {
                result: false,
                message: `Cannot Retreive latest availability`,
            };
        }
    } else {
        // session is group class, check with grou p class check
		const material: GroupClassSlotsMaterials = {
			caps,
			counters: monthCounters,
			groupClassSlots,
			recurringSlots,
			sessionDate
		}
		const slots = handleGroupClassSlots(material);
		classSlots = slots?.filteredSlots;
    }

    // check gears
    const allGears: GearTypes.Gear[] = store.getState().gears.gears;
    let latestGearStocks = handleGearsStock(allGears, monthCounters, session, isPreBook);
    let thisSessionGears: { bookingGear: SessionsTypes.BookingGear; count: number }[] = [];
    
    session.gears.forEach((g) => {
        const idx = thisSessionGears.findIndex(
            (x) => x.bookingGear.class === g.class && x.bookingGear.gear.id === g.gear.id && x.bookingGear.size === g.size
        );
        if (idx >= 0) {
            // same gear is already there, just add the count on it
            thisSessionGears[idx] = Object.assign({ ...thisSessionGears[idx] }, { count: thisSessionGears[idx].count + 1 });
        } else {
            thisSessionGears.push({
                bookingGear: g,
                count: 1,
            });
        }
    });

    // console.log(`latestGearStocks`, latestGearStocks);
    // console.log(`thisSessionGears`, thisSessionGears);

    const notAvailableGears = thisSessionGears.filter((g) => {
        const stockedGear = latestGearStocks.find((x) => x.id === g.bookingGear.gear.id);
        const stockQty = stockedGear?.stock.find((x) => x.class === g.bookingGear.class && x.size === g.bookingGear.size)?.qty;
        // console.log(`gear ${g.bookingGear.gear.name}, size: ${g.bookingGear.size}, QTY: ${stockQty}`);
        if (!stockQty) {
            return true;
        }
        if (stockQty < g.count) {
            return true;
        }
        return false;
    });

    // console.log(`notAvailableGears`, notAvailableGears);

    if (notAvailableGears.length > 0) {
        return {
            result: false,
            message: `Gears no longer available`,
        };
    }
    /* end of check gear */

    // check if each hour's slots are still enough to provide for the booking pax
    let canBook = true;
    const start = dayjs.tz(session.start);
    const end = dayjs.tz(session.end);
    const duration = end.diff(start, "hours");

	if (!session.groupClass) {
		if (timeSlots) {
			console.log(`timeSlots`, timeSlots);
			for (let i = 0; i < duration; i++) {
				const hourSlot: TimeSlot | undefined = timeSlots.find((x) => {
					const isTime = x.time === start.add(i, "hours").valueOf();
					return isTime;
				});
				if (!hourSlot || hourSlot.slots < session.pax) {
					canBook = false;
				}
			}
		}
	} else {
        if (classSlots && session.groupClass) {
            const targetClass = classSlots.find(x => x.class.id === session.groupClass?.id && x.paxSlots >= session.pax);
            console.log('group class checked. Class slots: ', classSlots)
            console.log('group class checked. targetClass: ', targetClass)
			if (!targetClass) {
				canBook = false;
			}
		}
	}

    // if yes, return true, else false
    if (!canBook) {
        return {
            result: false,
            message: `Slots no longer available`,
        };
    } else {
        return {
            result: true,
            message: `success`,
        };
    }
};

export const handleGroupClassSlots = (materials: GroupClassSlotsMaterials) => {
    const { caps, counters, sessionDate, maintenences, groupClassSlots, recurringSlots } = materials;

    // get the right capacity of the day
    const capacity = caps.capacities.filter((x) => x.playType === "ski").reduce((a, b) => a + b.max, 0);

    if (!capacity) {
        return;
    }

    // get the sessions logs from this month's counter
    const thisMonthSessions: SessionsTypes.MonthSession[] = counters.sessions;

    // console.log(
    //   `Group Slots:`, groupClassSlots, recurringSlots
    // )

    let allSlots: GroupClassSlot[] = groupClassSlots.concat(recurringSlots);
    let daySlots = allSlots.reduce((p, v) => p + v.maxPax, 0);

    /** Start of class slots handler */
    // create array to hold selectable slots
    let filteredSlots: SelectGroupClassSlot[] = [];

    // deduct various availability out of all class slots
    allSlots.forEach((thisClass) => {
        // this let is for storing the available pax slot of the class
        let paxSlots = thisClass.maxPax;

        const slotStart = dayjs.tz(`${dayjs.tz(sessionDate).format(`YYYY-MM-DD`)}T${thisClass.start}`);
        const slotEnd = slotStart.add(1, "hour").subtract(1, 'minute');

        // check maintenences
        if (maintenences) {
            const purePlayType: SessionsTypes.PlayType = "ski";
            const effectedMaintenences = maintenences.filter((x) => {
                if (!x.start || !x.end) {
                    return false;
                }
                let effectedPlayTypes: SessionsTypes.PlayType[] = [];
                x.venue.forEach((pt) => {
                    effectedPlayTypes.push(pt.playType);
                });

                const isInTime =
                    (slotStart.valueOf() >= x.start.valueOf() && slotStart.valueOf() <= x.end.valueOf()) ||
                    (slotEnd.valueOf() >= x.start.valueOf() && slotEnd.valueOf() <= x.end.valueOf());
                const isType = effectedPlayTypes.includes(purePlayType);
                return isInTime && isType;
            });
            if (effectedMaintenences.length > 0) {
                effectedMaintenences.forEach(m => {
                    m.venue.forEach(v => {
                        const count = caps.capacities.find(cc => cc.venue.name === v.venue.name && cc.playType === purePlayType)?.max;
                        if (!count) {
                            return
                        }
                        const typedCaps = caps.capacities.filter(c => c.playType === purePlayType).reduce((p, v) => p + v.max, 0);
                        const remainingSlots = typedCaps - count;
                        paxSlots = remainingSlots >= paxSlots ? paxSlots : 0;
                    })
                })
            }
        }

        // filter out existing sessions with the right game types
        let hourSessions: SessionsTypes.MonthSession[] = thisMonthSessions.filter((x) => {
            const counterStart = dayjs.tz(x.start).valueOf();
            const counterEnd = dayjs.tz(x.end).subtract(1, "minute").valueOf();

            // counters needs to be included when either
            // session start or end are within the counter range
            const isValidTime =
                (slotStart.valueOf() >= counterStart && slotStart.valueOf() <= counterEnd) ||
                (slotEnd.valueOf() >= counterStart && slotEnd.valueOf() <= counterEnd);
            const isSnow = x.playType === "snow" || x.playType === "ski" || x.playType === "sb" || x.playType === "all";
            const isVenue = isSnow;
            return isValidTime && isVenue;
        });

        let venueSlots = capacity - hourSessions.reduce((p, v) => p + v.pax, 0);

        hourSessions.forEach((x) => {
            // find the pax from the target class and deduct it
            const isThisClass = x.groupClass ? x.groupClass.id === thisClass.id : false;
            if (isThisClass) {
                paxSlots = paxSlots - x.pax;
            }
        });

        // get the smaller number out of venue slots or classPaxSlot
        paxSlots = Math.min(venueSlots, paxSlots);

        // if paxSlot is smaller than 0, make it 0;
        paxSlots = paxSlots < 0 ? 0 : paxSlots;

        // push slot to array as unfiltered slots
        filteredSlots.push({ paxSlots, class: thisClass });
    });
    /** end of class slots handler */

    // handle final day slot count after all the class slots are checked
    const allClassSlots = filteredSlots.reduce((p, v) => p + v.paxSlots, 0);
    daySlots = allClassSlots;

    return {
        filteredSlots,
        daySlots,
    };
};
