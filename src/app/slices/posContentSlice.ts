import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import * as HomepageTypes from '@tsanghoilun/snow-n-surf-interface/types/homePage';
import { RootState } from "../../app/store";

interface PosContentState{
    homepageSlides: HomepageTypes.HomePageSlide[],
}

const initialState: PosContentState = {
    homepageSlides: []
}

export const posContentSlice = createSlice({
    name: 'posContent',
    initialState,
    reducers: {
        setHomepageSlides: (state: PosContentState, action: PayloadAction<HomepageTypes.HomePageSlide[]>) => {
            state.homepageSlides = action.payload;
        }
    }
});

export const selectHomepageSlides = (state: RootState) => state.posContent.homepageSlides;
export const selectHomepageSlideById = (id: string) => (state: RootState) => state.posContent.homepageSlides.find(x => x.id === id);

export const { setHomepageSlides } = posContentSlice.actions;

export default posContentSlice.reducer;