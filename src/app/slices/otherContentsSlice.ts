import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { OtherContent } from "@tsanghoilun/snow-n-surf-interface/types/page";
import { RootState } from "../store";

interface OtherContentsState {
  otherContents: OtherContent[]
}

const initialState: OtherContentsState = {
  otherContents: [],
};

export const otherContentsSlice = createSlice({
  name: "otherContents",
  initialState,
  reducers: {
    setAllOtherContents: (state: OtherContentsState, action: PayloadAction<OtherContent[]>) => {
      state.otherContents = action.payload;
    },
  },
});

export const selectAllOtherContents = (state: RootState) => state.otherContents.otherContents;

// Action creators are generated for each case reducer function
export const { setAllOtherContents } = otherContentsSlice.actions;

export default otherContentsSlice.reducer;
