import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import * as UserTypes from '@tsanghoilun/snow-n-surf-interface/types/user';
import { RootState } from "../../app/store";

interface CustomerClassesState {
  CustomerClasses: UserTypes.CustomerClass[];
}

const initialState: CustomerClassesState = {
  CustomerClasses: [],
};

export const CustomerClassesSlice = createSlice({
  name: "customerClasses",
  initialState,
  reducers: {
    setCustomerClasses: (state: CustomerClassesState, action: PayloadAction<UserTypes.CustomerClass[]>) => {
      state.CustomerClasses = action.payload;
    },
  },
});

export const selectCustomerClasses = (state: RootState) => state.customerClasses.CustomerClasses;

// Action creators are generated for each case reducer function
export const { setCustomerClasses } = CustomerClassesSlice.actions;

export default CustomerClassesSlice.reducer;
