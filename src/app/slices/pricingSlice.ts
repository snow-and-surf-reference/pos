import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../store";

export type PriceUnit = "pax" | "hour" | "session" | "day" | "storage" | "item";

export interface UnitPrice {
    units: PriceUnit[];
    price: number;
}

export interface Price {
    enterance: UnitPrice;
    group_class: UnitPrice;
    personal_training: UnitPrice;
    default_snow_belt: UnitPrice;
    default_surf_lane: UnitPrice;
    default_snow_venue: UnitPrice;
    default_surf_venue: UnitPrice;
    default_full_park: UnitPrice;
    storage: UnitPrice;
    gear_standard: UnitPrice;
    gear_premium: UnitPrice;
}

interface PricingState {
	pricing: Price | null;
	isInit: boolean;
}

const initialState: PricingState = {
	pricing: null,
	isInit: false
};

export const pricingSlice = createSlice({
    name: "pricing",
    initialState,
    reducers: {
        setPricing: (state: PricingState, action: PayloadAction<Price>) => {
            state.pricing = action.payload;
        },
        setPricingIsInit: (state: PricingState, action: PayloadAction<boolean>) => {
            state.isInit = action.payload;
        },
    },
});

export const selectPricing = (state: RootState) => state.pricing.pricing;
export const selectPricingIsInit = (state: RootState) => state.pricing.isInit;

// Action creators are generated for each case reducer function
export const { setPricing, setPricingIsInit } = pricingSlice.actions;

export default pricingSlice.reducer;
