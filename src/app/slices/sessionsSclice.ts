import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import { Session, MonthCounters } from '@tsanghoilun/snow-n-surf-interface/types/session';
import { RootState } from "../store";
import { defaultSingleSession } from "../../components/Booking/defaultSession";
import dayjs from "dayjs";
import timezone from 'dayjs/plugin/timezone';
import utc from 'dayjs/plugin/utc';

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

export interface SessionsState {
	isInit: boolean;
  sessions: Session[];
  tempSession: Session;
  monthCounters: MonthCounters[];
}

const initialState: SessionsState = {
	isInit: false,
  sessions: [],
  tempSession: defaultSingleSession,
  monthCounters: []
};

export const sessionsSlice = createSlice({
	name: "sessions",
	initialState,
	reducers: {
		setSessions: (
			state: SessionsState,
			action: PayloadAction<Session[]>
		) => {
			state.sessions = action.payload;
    },
    setTempSession: (state: SessionsState, action: PayloadAction<Session>) => {
      localStorage.setItem('temp-session', JSON.stringify(action.payload));
      state.tempSession = action.payload;
    },
    clearTempSession: (state: SessionsState) => {
      state.tempSession = defaultSingleSession;
      localStorage.removeItem('temp-session');
    },
    setSessionIsInit: (state: SessionsState, action: PayloadAction<boolean>) => {
      state.isInit = action.payload;
    },
    // setMonthCountersByMonth: (state: SessionsState, action: PayloadAction<MonthCounters>) => {
    //   const idx = state.monthCounters.findIndex(x => x.id === action.payload.id);
    //   if (idx < 0) {
    //     state.monthCounters.push(action.payload)
    //   } else {
    //     state.monthCounters[idx] = action.payload
    //   }
    // },
	},
});

export const selectSessions = (state: RootState) => state.sessions.sessions;
export const selectSessionsIsInit = (state: RootState) => state.sessions.isInit;
export const selectTempSession = (state: RootState) => state.sessions.tempSession;
// export const selectMonthCounters = (state: RootState) => state.sessions.monthCounters;
// export const selectMonthCountersByMonth = (date: Date) => (state: RootState) => { 
//   const startOfMonth = dayjs.tz(date).startOf('month');
//   const endOfMonth = dayjs.tz(date).endOf('month');
//   const counter: MonthCounters | undefined = state.sessions.monthCounters.find(x =>
//     (x.month.valueOf() >= startOfMonth.valueOf()) && (x.month.valueOf() <= endOfMonth.valueOf())
//   );
//   return counter;
// };

// Action creators are generated for each case reducer function
export const {
  setSessions,
  setTempSession,
  clearTempSession,
  setSessionIsInit,
  // setMonthCountersByMonth
} = sessionsSlice.actions;

export default sessionsSlice.reducer;
