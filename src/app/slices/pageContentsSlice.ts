import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { PageContent } from "@tsanghoilun/snow-n-surf-interface/types/page";
import { RootState } from "../store";

interface PageContentsState {
  pageContents: PageContent[];
}

const initialState: PageContentsState = {
  pageContents: []
};

export const pageContentsSlice = createSlice({
  name: "pageContents",
  initialState,
  reducers: {
    setAllPageContents: (state: PageContentsState, action: PayloadAction<PageContent[]>) => {
      state.pageContents = action.payload;
    }
  },
});

export const selectAllPageContents = (state: RootState) => state.pageContents.pageContents;
export const selectPageContentByTitle = (title: string) => (state: RootState) => state.pageContents.pageContents.find(x => x.title.en === title);

// Action creators are generated for each case reducer function
export const { setAllPageContents } = pageContentsSlice.actions;

export default pageContentsSlice.reducer;
