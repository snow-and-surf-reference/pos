import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import * as GroupClassTypes from "@tsanghoilun/snow-n-surf-interface/types/groupClass";
import { RootState } from "../store";

interface GroupClassState {
    groupClassPresets: GroupClassTypes.GroupClassPreset[];
}

const initialState: GroupClassState = {
    groupClassPresets: [],
};

export const groupClassSlice = createSlice({
    name: "groupClass",
    initialState,
    reducers: {
        setGroupClassPresets: (state: GroupClassState, action: PayloadAction<GroupClassTypes.GroupClassPreset[]>) => {
            state.groupClassPresets = action.payload;
        },
    },
});

export const selectGroupClassPresets = (state: RootState) => state.groupClass.groupClassPresets;

// Action creators are generated for each case reducer function
export const { setGroupClassPresets } = groupClassSlice.actions;

export default groupClassSlice.reducer;
