import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import * as TopupTypes from '@tsanghoilun/snow-n-surf-interface/types/topUp';
import { RootState } from "../store";

export interface TopupPackagesState {
	topupPackages: TopupTypes.TopUpPackages[];
}

const initialState: TopupPackagesState = {
	topupPackages: []
};

export const TopupPackagesSlice = createSlice({
	name: "topupPackages",
	initialState,
	reducers: {
		setTopupPackages: (
			state: TopupPackagesState,
			action: PayloadAction<TopupTypes.TopUpPackages[]>
		) => {
			state.topupPackages = action.payload;
		}
	},
});

export const selectTopupPackages = (state: RootState) => state.topupPackages.topupPackages;

// Action creators are generated for each case reducer function
export const { setTopupPackages } =
	TopupPackagesSlice.actions;

export default TopupPackagesSlice.reducer;
