import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../store";
import * as FaqTypes from "@tsanghoilun/snow-n-surf-interface/types/faq";

interface FaqState {
    faqCats: FaqTypes.FaqCat[];
    questions: FaqTypes.QNA[];
    isInit: boolean;
}

const initialState: FaqState = {
    faqCats: [],
    questions: [],
    isInit: false,
};

export const faqSlice = createSlice({
    name: "faq",
    initialState,
    reducers: {
        setFaqIsInit: (state: FaqState, action: PayloadAction<boolean>) => {
            state.isInit = action.payload;
        },
        setAllFaqCat: (state: FaqState, action: PayloadAction<FaqTypes.FaqCat[]>) => {
            state.faqCats = action.payload;
        },
        setAllFaqQuestion: (state: FaqState, action: PayloadAction<FaqTypes.QNA[]>) => {
            state.questions = action.payload;
        },
        setFaqCatById: (state: FaqState, action: PayloadAction<FaqTypes.FaqCat>) => {
            const idx = state.faqCats.findIndex((x) => x.id === action.payload.id);
            if (idx < 0) {
                state.faqCats.push(action.payload);
            } else {
                state.faqCats[idx] = action.payload;
            }
        },
        setQuestionById: (state: FaqState, action: PayloadAction<FaqTypes.QNA>) => {
            const idx = state.questions.findIndex((x) => x.id === action.payload.id);
            if (idx < 0) {
                state.questions.push(action.payload);
            } else {
                state.questions[idx] = action.payload;
            }
        },
    },
});

export const selectAllFaqCats = (state: RootState) => state.faq.faqCats;
export const selectAllFaqQuestions = (state: RootState) => state.faq.questions;
export const selectFaqIsInit = (state: RootState) => state.faq.isInit;

export const {
    setFaqIsInit,
    setAllFaqCat,
    setAllFaqQuestion,
    setFaqCatById,
    setQuestionById
} = faqSlice.actions;

export default faqSlice.reducer;
