import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import { Customer } from "@tsanghoilun/snow-n-surf-interface/types/user";
import * as GearTypes from "@tsanghoilun/snow-n-surf-interface/types/gear";
import * as TopupTypes from '@tsanghoilun/snow-n-surf-interface/types/topUp';
import { RootState } from "../store";

export interface AuthUserstate {
	isInit: boolean;
	authUser: Customer | null;
	subAccounts: Customer[];
	storages: GearTypes.GearStorage[];
	topupOrders: TopupTypes.TopupOrder[];
	loggingIn: boolean;
	maybeStaff: boolean;
}

const initialState: AuthUserstate = {
	isInit: false,
	authUser: null,
	subAccounts: [],
	storages: [],
	topupOrders: [],
	loggingIn: false,
	maybeStaff: false
};

export const authUserSlice = createSlice({
	name: "authUser",
	initialState,
	reducers: {
		setAuthUser: (
			state: AuthUserstate,
			action: PayloadAction<Customer | null>
		) => {
			state.authUser = action.payload;
		},
		setLoggingIn: (state: AuthUserstate, action: PayloadAction<boolean>) => {
			state.loggingIn = action.payload;
		},
		setMaybeStaff: (state: AuthUserstate, action: PayloadAction<boolean>) => {
			state.maybeStaff = action.payload;
		},
		setSubAcc: (state: AuthUserstate, action: PayloadAction<Customer[]>) => {
			state.subAccounts = action.payload;
		},
		setStorages: (state: AuthUserstate, action: PayloadAction<GearTypes.GearStorage[]>) => {
			state.storages = action.payload;
		},
		setAuthUserIsInit: (
			state: AuthUserstate,
			action: PayloadAction<boolean>
		) => {
			state.isInit = action.payload;
		},
		setTopupOrders: (
			state: AuthUserstate,
			action: PayloadAction<TopupTypes.TopupOrder[]>
		) => {
			state.topupOrders = action.payload;
		}
	},
});

export const selectAuthUser = (state: RootState) => state.authUser.authUser;
export const selectLoggingIn = (state: RootState) => state.authUser.loggingIn;
export const selectSubAccounts = (state: RootState) => state.authUser.subAccounts;
export const selectAuthUserStorages = (state: RootState) => state.authUser.storages;
export const selectAuthUserIsInit = (state: RootState) => state.authUser.isInit;
export const selectTopupOrders = (state: RootState) => state.authUser.topupOrders;
export const selectMaybeStaff = (state: RootState) => state.authUser.maybeStaff;

// Action creators are generated for each case reducer function
export const { setAuthUser, setAuthUserIsInit, setSubAcc, setStorages, setTopupOrders, setLoggingIn, setMaybeStaff } =
	authUserSlice.actions;

export default authUserSlice.reducer;
