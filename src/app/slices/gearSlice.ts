import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import * as GearTypes from '@tsanghoilun/snow-n-surf-interface/types/gear';
import { RootState } from "../store";

interface GearState{
    gears: GearTypes.Gear[],
    gearUnits: GearTypes.GearUnit[]
}

const initialState: GearState = {
    gears: [],
    gearUnits: []
}

const gearSlice = createSlice({
    name: 'gears',
    initialState,
    reducers: {
        setAllGears: (state: GearState, action: PayloadAction<GearTypes.Gear[]>) => {
            state.gears = action.payload
        }
    }
})

export const { setAllGears } = gearSlice.actions;

export const selectGears = (state: RootState) => state.gears.gears;

export default gearSlice.reducer; 