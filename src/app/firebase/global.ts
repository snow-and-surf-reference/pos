import * as CoachTypes from "@tsanghoilun/snow-n-surf-interface/types/coach";
import { CoachSchedule } from "@tsanghoilun/snow-n-surf-interface/types/coach";
import { FaqCat, QNA } from "@tsanghoilun/snow-n-surf-interface/types/faq";
import * as GearTypes from "@tsanghoilun/snow-n-surf-interface/types/gear";
import * as GroupClassTypes from "@tsanghoilun/snow-n-surf-interface/types/groupClass";
import { HomePageSlide } from "@tsanghoilun/snow-n-surf-interface/types/homePage";
import {
	OtherContent,
	PageContent,
} from "@tsanghoilun/snow-n-surf-interface/types/page";
import {
	Coupon,
	MonthCounters,
	MonthSession,
	Session,
} from "@tsanghoilun/snow-n-surf-interface/types/session";
import * as TopupTypes from "@tsanghoilun/snow-n-surf-interface/types/topUp";
import * as UserTypes from "@tsanghoilun/snow-n-surf-interface/types/user";
import { Customer } from "@tsanghoilun/snow-n-surf-interface/types/user";
import * as VenueTypes from "@tsanghoilun/snow-n-surf-interface/types/venue";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import { getApp, getApps, initializeApp } from "firebase/app";
import {
	applyActionCode,
	checkActionCode,
	confirmPasswordReset,
	createUserWithEmailAndPassword,
	getAuth,
	onAuthStateChanged,
	sendEmailVerification,
	sendPasswordResetEmail,
	signInWithEmailAndPassword,
} from "firebase/auth";
import {
	addDoc,
	arrayUnion,
	collection,
	deleteDoc,
	doc,
	DocumentData,
	getDoc,
	getDocs,
	getFirestore,
	increment,
	limit,
	onSnapshot,
	orderBy,
	query,
	QueryDocumentSnapshot,
	runTransaction,
	setDoc,
	Timestamp,
	where,
	writeBatch,
} from "firebase/firestore";
import randomize from "randomatic";
import { v4 as uuidv4 } from "uuid";
import { defaultSingleSession } from "../../components/Booking/defaultSession";
import { checkAvailability } from "../functions/handleSlots";
import { fromDB, toDB } from "../helpers";
import {
	setAuthUser,
	setAuthUserIsInit,
	setLoggingIn,
	setMaybeStaff,
	setStorages,
	setSubAcc,
	setTopupOrders,
} from "../slices/authUserSlice";
import { setCustomerClasses } from "../slices/customerClassesSlice";
import {
	setAllFaqCat,
	setAllFaqQuestion,
	setFaqIsInit,
} from "../slices/faqSlice";
import { setAllGears } from "../slices/gearSlice";
import { setGroupClassPresets } from "../slices/groupClassesSlice";
import { setAllOtherContents } from "../slices/otherContentsSlice";
import { setAllPageContents } from "../slices/pageContentsSlice";
import { setHomepageSlides } from "../slices/posContentSlice";
import { Price, setPricing, setPricingIsInit } from "../slices/pricingSlice";
import { setSessions, setTempSession } from "../slices/sessionsSclice";
import { setTopupPackages } from "../slices/topupPackages";
import { setCapacities } from "../slices/venueSlice";
import { store } from "../store";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

const firebaseConfig = {
	apiKey: process.env.REACT_APP_Firebase_apiKey,
	authDomain: process.env.REACT_APP_Firebase_authDomain,
	projectId: process.env.REACT_APP_Firebase_projectId,
	storageBucket: process.env.REACT_APP_Firebase_storageBucket,
	messagingSenderId: process.env.REACT_APP_Firebase_messagingSenderId,
	appId: process.env.REACT_APP_Firebase_appId,
	measurementId: process.env.REACT_APP_Firebase_measurementId,
};

let allUnSubs: Array<() => void> = [];
let publicUnSubs: Array<() => void> = [];

export const initFirebase = async () => {
	if (getApps().length <= 0) {
		const app = initializeApp(firebaseConfig);
		const auth = getAuth(app);
		onAuthStateChanged(auth, async (user) => {
			if (user) {
				// get user data from 'user' docs
				const uid = user.uid;
				const authEmailVerified = user.emailVerified;
				let docEmailVerify = false;
				const db = getFirestore();
				const docRef = doc(db, "customers", uid);
				const subAccountQ = query(
					collection(getFirestore(), `customers`),
					where("email", "==", user.email),
					where("isSub", "==", true)
				);
				const staffQ = query(
					collection(getFirestore(), `staffs`),
					where(`email`, "==", user.email)
				);

				const userDoc = await getDoc(docRef);

				// first check doc verified
				if (userDoc.exists()) {
					const userData = userDoc.data();
					docEmailVerify = userData.emailVerified;
					// if doc verified, no need to check auth verified
					// else, check auth verified
					if (!docEmailVerify) {
						// if auth verified but doc is not, update doc to verified
						if (authEmailVerified) {
							setDoc(
								doc(getFirestore(), `customers/${auth.currentUser?.uid}`),
								{ emailVerified: true },
								{ merge: true }
							);
						} else {
							// finally if both doc and auth not verified, treat as not verified
						}
					}
				}

				if (!userDoc.exists()) {
					const staffDocs = await getDocs(staffQ);
					if (!staffDocs.empty) {
						store.dispatch(setAuthUser(null));
						store.dispatch(setMaybeStaff(true));
						logout();
						return;
					}
				}

				const unSub = onSnapshot(docRef, async (ss) => {
					if (!ss.exists()) {
						store.dispatch(setAuthUser(null));
						logout();
						return;
					}

					const data = fromDB(ss.data());
					const authUser: Customer = {
						...data,
						emailVerified: docEmailVerify,
						id: ss.id,
					} as Customer;

					store.dispatch(setAuthUser(authUser));
					store.dispatch(setAuthUserIsInit(true));
					store.dispatch(setLoggingIn(false));
					TopupRecordsListener(authUser);
					myBookingsListener(authUser);
					storageListener(authUser);
				});

				// get sub accounts
				const unSub2 = onSnapshot(subAccountQ, async (sss) => {
					let subAccounts: Customer[] = [];
					sss.forEach((ss) => {
						const data = fromDB(ss.data());
						const id = ss.id;
						subAccounts.push({
							id,
							...data,
						} as Customer);
					});
					store.dispatch(setSubAcc(subAccounts));
				});
				allUnSubs.push(unSub);
				allUnSubs.push(unSub2);
				privateListeners();
			} else {
				if (allUnSubs.length) {
					// allUnSubs.forEach((x) => x);
				}
				store.dispatch(setAuthUser(null));
				store.dispatch(setAuthUserIsInit(true));
				store.dispatch(setLoggingIn(false));
			}
			publicListeners();
		});
	}
};

export const signup = async (payload: any) => {
	const { email, password, usrname, dob, phone } = payload;
	const auth = getAuth(getApp());
	let res;

	const genCode = async () => {
		let code = randomize(`000000`, 6);
		let isUnique = false;

		while (!isUnique) {
			const q = query(
				collection(getFirestore(), `customers`),
				where(`memberId`, `==`, String(code))
			);
			const codeDocs = await getDocs(q);
			if (codeDocs.size === 0) {
				isUnique = true;
			} else {
				code = randomize(`000000`, 6);
			}
		}
		return code;
	};

	const newCode = await genCode();

	await createUserWithEmailAndPassword(auth, email, password)
		.then(async (userCredential) => {
			const userData: Omit<Customer, "id"> = {
				memberId: newCode,
				realName: "",
				realImage: "",
				usrname,
				dob: dayjs(String(dob).split("-").reverse().join("-")).toDate(),
				email: String(email).toLowerCase(),
				phone,
				createdAt: dayjs().toDate(),
				wallet: { balance: 0, spent: 0 },
				isVerified: false,
				emailVerified: false,
				enabled: true,
				isSub: false,
			};
			const db = getFirestore();

			// update the send verify email without awaiting create doc
			sendEmailVerification(userCredential.user);
			await setDoc(
				doc(db, `customers`, userCredential.user.uid),
				toDB(userData)
			);
			res = "success";
		})
		.catch((error) => {
			console.error(error);
			res = error.message;
		});
	return res;
};

export const resendVerifyLink = async () => {
	const auth = getAuth(getApp());
	const user = auth.currentUser;
	if (!user) {
		return;
	}
	await sendEmailVerification(user);
	return "success";
};

export const emailVerifying = async (code: string) => {
	const auth = getAuth(getApp());
	try {
		await checkActionCode(auth, code);
		await applyActionCode(auth, code);
		return "success";
	} catch (error) {
		return error;
	}
};

export const sendPwEmail = async (email: string) => {
	const auth = getAuth(getApp());
	try {
		await sendPasswordResetEmail(auth, email);
		return "success";
	} catch (error) {
		return error;
	}
};

export const checkResetPwCode = async (code: string) => {
	const auth = getAuth(getApp());
	try {
		const res = await checkActionCode(auth, code);
		return res;
	} catch (error) {
		return error;
	}
};

export const resetPassword = async (code: string, newPassword: string) => {
	const auth = getAuth(getApp());
	try {
		await confirmPasswordReset(auth, code, newPassword);
		return "success";
	} catch (error) {
		return error;
	}
};

export const login = async (payload: any) => {
	const auth = getAuth(getApp());
	store.dispatch(setLoggingIn(true));
	try {
		await signInWithEmailAndPassword(auth, payload.email, payload.password);
		return "success";
	} catch (error) {
		store.dispatch(setLoggingIn(false));
		console.log(error);
		return error;
	}
};

export const logout = async () => {
	const auth = getAuth(getApp());
	await auth.signOut();
	return;
};

// Functions for Venues
const capListener = async () => {
	const q = query(collection(getFirestore(), `capacities`), limit(1));
	const sss = await getDocs(q);
	if (sss.size !== 1) {
		return;
	}
	const docRef = doc(getFirestore(), `capacities/${sss.docs[0].id}`);
	const unSub = onSnapshot(docRef, (ss) => {
		if (!ss.exists()) {
			return;
		}
		store.dispatch(
			setCapacities({
				...ss.data(),
			} as VenueTypes.Capacities)
		);
	});
	allUnSubs.push(unSub);
};

export const getMonthCounterById = async (id: string) => {
	const ss = await getDoc(doc(getFirestore(), `counters/${id}`));
	if (ss.exists()) {
		const data = fromDB(ss.data());
		return {
			id,
			...data,
		} as MonthCounters;
	} else {
		return null;
	}
};

// export const monthCountersListener = async () => {
//   const month = dayjs.tz(Date.now()).startOf("month").toDate();
//   const q = query(
//     collection(getFirestore(), `counters`),
//     where(`month`, `>=`, Timestamp.fromDate(month))
//   );
//   const unSub = onSnapshot(q, async (sss) => {
//     sss.forEach((ss) => {
//       const data = fromDB(ss.data());
//       const monthCounter = {
//         id: ss.id,
//         ...data,
//       } as MonthCounters;
//       store.dispatch(setMonthCountersByMonth(monthCounter));
//     });
//   });
//   allUnSubs.push(unSub);
// };

export const myBookingsListener = async (authUser: Customer) => {
	const q = query(
		collection(getFirestore(), `sessions`),
		where("bookingUser.id", "==", authUser.id),
		limit(30)
	);
	const unSub = onSnapshot(q, async (sss) => {
		let sessions: Session[] = [];
		sss.forEach((ss) => {
			const id = ss.id;
			const data = fromDB(ss.data());
			sessions.push({
				id,
				...data,
			} as Session);
		});
		store.dispatch(setSessions(sessions));
	});
	allUnSubs.push(unSub);
};

export const createLockedSession = async (session: Session) => {
	const { id, lockedAt, status, ...data } = session;
	const payload = toDB({
		lockedAt: dayjs.tz(Date.now()).add(15, `minutes`).toDate(),
		status: `locked`,
		...data,
	});

	// const sessionMouthCounters: MonthCounters[] = selectMonthCounters(
	//   store.getState()
	// );
	// const counterId = sessionMouthCounters.find((x) =>
	//   dayjs
	//     .tz(session.sessionDate)
	//     .startOf("month")
	//     .isSame(dayjs.tz(x.month).startOf("month"))
	// )?.id;
	// if (!counterId) {
	//   return {
	//     message: "Failed to start your booking, please try again later.",
	//   };
	// }

	// const counterRef = doc(getFirestore(), `counters/${counterId}`);
	const counters = await getCounterByDateV2(session.sessionDate);

	const singleCoachSchedules: CoachTypes.CoachSchedule[] =
		await getCoachScheduleByDate(session.sessionDate);
	const allRecurringCoachSchedules: CoachTypes.CoachSchedule[] =
		await getRecurringCoachSchedules();
	const recurringCoachSchedulesByDay = allRecurringCoachSchedules.filter(
		(x) => {
			const isRightDay =
				x.daysOfWeek?.filter(
					(y) => y.index === dayjs.tz(session.sessionDate).get("day")
				).length === 1;
			const isValidDate = x.endDate
				? x.endDate.valueOf() >= session.sessionDate.valueOf()
				: true;
			const isValidStartDate = x.startDate
				? x.startDate.valueOf() <= session.sessionDate.valueOf()
				: true;
			return isRightDay && isValidDate && isValidStartDate;
		}
	);

	// get coach schedules for use later
	const coachSchedules = singleCoachSchedules.concat(
		recurringCoachSchedulesByDay
	);
	const groupClassSlots: GroupClassTypes.GroupClassSlot[] =
		await getGroupClassSlotsByDate(
			dayjs.tz(session.sessionDate).startOf("day").toDate()
		);
	const recurringSlots: GroupClassTypes.GroupClassSlot[] =
		await getRGroupClassSlotsByDay(dayjs.tz(session.sessionDate).get("day"));
	const newSessionId = uuidv4();
	const newSessionRef = doc(getFirestore(), `sessions/${newSessionId}`);
	// const { sessionType, start, end, pax, playType, gears, groupClass } = session;
	// const newCounterSession: MonthSession = {
	// 	id: newSessionId,
	// 	sessionType,
	// 	start,
	// 	end,
	// 	pax,
	// 	playType,
	// 	gears,
	// 	groupClass,
	// };

	// use transaction to create the session
	try {
		await runTransaction(getFirestore(), async (txn) => {
			// const counterSs = await txn.get(counterRef);
			// if (!counterSs.exists()) {
			//   throw new Error(`Something's not right... Please try again later.`);
			// }
			// const data = fromDB(counterSs.data());
			// const counter: MonthCounters = {
			//   id: counterSs.id,
			//   ...data,
			// } as MonthCounters;

			const finalCheck = checkAvailability(
				session,
				counters,
				coachSchedules,
				groupClassSlots,
				recurringSlots,
				false
			);
			if (finalCheck.result) {
				// create the session doc
				txn.set(newSessionRef, payload);
				// txn.update(counterSs.ref, {
				//   sessions: arrayUnion(toDB(newCounterSession)),
				// });
			} else {
				throw new Error(`${finalCheck.message}`);
			}
		});
	} catch (error: any) {
		return {
			message: error.message,
		};
	}

	store.dispatch(setTempSession(defaultSingleSession));
	return {
		message: "success",
		sessionId: newSessionId,
	};
};

export const cancelLockedSession = async (session: Session) => {
	// const monthCounter: MonthCounters | undefined = selectMonthCountersByMonth(
	//   session.sessionDate
	// )(store.getState());
	// if (!monthCounter) {
	//   return "Network issue, Please retry later.";
	// }

	// const counterSession: MonthSession | undefined = monthCounter.sessions.find(
	//   (x) => x.id === session.id
	// );
	// if (!counterSession) {
	//   return "Network issue, Please retry later.";
	// }

	// const convertedCounter = toDB(counterSession);

	const batch = writeBatch(getFirestore());
	const docRef = doc(getFirestore(), `sessions/${session.id}`);
	// const counterRef = doc(getFirestore(), `counters/${monthCounter.id}`);

	batch.delete(docRef);
	// batch.update(counterRef, {
	//   sessions: arrayRemove(convertedCounter),
	// });

	await batch.commit();

	return "success";
};

export const getSessionById = async (id: string) => {
	const res = await getDoc(doc(getFirestore(), `sessions/${id}`));
	if (!res.exists()) {
		return null;
	}
	const data = fromDB(res.data());
	return {
		id,
		...data,
	} as Session;
};

export const paySession = async (session: Session, customerId: string) => {
	const genCode = async () => {
		let code = randomize(`0000`, 4);
		let isUnique = false;

		while (!isUnique) {
			const q = query(
				collection(getFirestore(), `sessions`),
				where(`bookingNumber`, `==`, String(code))
			);
			const codeDocs = await getDocs(q);
			if (codeDocs.size === 0) {
				isUnique = true;
			} else {
				let hasDupToday: Session[] = [];
				codeDocs.forEach((ss) => {
					const data = fromDB(ss.data());
					const temp: Session = {
						id: ss.id,
						...data,
					} as Session;
					const sessionDate = dayjs.tz(temp.sessionDate).valueOf();
					const todayStart = dayjs.tz(Date.now()).startOf("day").valueOf();
					const todayEnd = dayjs.tz(Date.now()).endOf("day").valueOf();
					if (sessionDate >= todayStart && sessionDate <= todayEnd) {
						hasDupToday.push(temp);
					}
				});
				if (!hasDupToday.length) {
					isUnique = true;
				} else {
					code = randomize(`0000`, 4);
				}
			}
		}

		return code;
	};

	const newCode = await genCode();

	// update session: confirm date, cancelled date
	const payload = {
		bookingNumber: String(newCode),
		status: `confirmed`,
		confirmedAt: dayjs.tz(Date.now()).toDate(),
		cancelledAt: null,
		lockedAt: null,
	};

	try {
		await setDoc(doc(getFirestore(), `sessions/${session.id}`), toDB(payload), {
			merge: true,
		});
		let usedCouponId = session.usedCoupon ? [session.usedCoupon.id] : [];
		await setDoc(
			doc(getFirestore(), `customers/${customerId}`),
			{
				wallet: {
					balance: increment(-session.totalCredits),
					spent: increment(0),
				},
				usedCouponId: arrayUnion(...usedCouponId),
			},
			{ merge: true }
		);

		if (session.usedCoupon && !session.usedCoupon.canReuse) {
			await setDoc(
				doc(getFirestore(), `coupons/${session.usedCoupon.id}`),
				{
					qty: increment(-1),
				},
				{ merge: true }
			);
		}
		return "success";
	} catch (error) {
		return error;
	}
};
// end of sessions functions

// Gear Functions
export const gearListener = async () => {
	const q = query(collection(getFirestore(), `gears`));
	const unSub = onSnapshot(q, (sss) => {
		const gears: GearTypes.Gear[] = [];
		sss.forEach((ss) => {
			gears.push({
				id: ss.id,
				...ss.data(),
			} as GearTypes.Gear);
		});
		store.dispatch(setAllGears(gears));
	});
	allUnSubs.push(unSub);
};

// coachSchedules
export const getCoachScheduleByDate = async (date: Date) => {
	// convert date to start and end timestamps
	const targetDate = {
		start: Timestamp.fromDate(dayjs.tz(date).startOf("day").toDate()),
		end: Timestamp.fromDate(dayjs.tz(date).endOf("day").toDate()),
	};
	const qq = query(
		collection(getFirestore(), `coachSchedules`),
		where("date", ">=", targetDate.start),
		where("date", "<=", targetDate.end)
	);
	const res = await getDocs(qq);
	const schedules: CoachSchedule[] = [];
	res.forEach((ss) => {
		const id = ss.id;
		const data = fromDB(ss.data());
		schedules.push({ id, ...data } as CoachSchedule);
	});
	return schedules;
};

// Topup
export const topup = async (credit: number) => {
	await setDoc(
		doc(getFirestore(), `customers/${store.getState().authUser.authUser?.id}`),
		{
			wallet: {
				balance: increment(credit),
				spent: increment(credit),
			},
		},
		{ merge: true }
	);
	return "success";
};

export const topupPackagesListener = async () => {
	const q = query(collection(getFirestore(), `topUpPackages`));
	const unSub = onSnapshot(q, async (sss) => {
		const topUpPackages: TopupTypes.TopUpPackages[] = [];
		sss.forEach((ss) => {
			const id = ss.id;
			const data = fromDB(ss.data());
			topUpPackages.push({ id, ...data } as TopupTypes.TopUpPackages);
		});
		store.dispatch(setTopupPackages(topUpPackages));
	});
	allUnSubs.push(unSub);
};

export const TopupRecordsListener = async (authUser: Customer) => {
	const q = query(
		collection(getFirestore(), `topUpRecords`),
		where("authUser.id", "==", authUser.id),
		// orderBy('authUser.id'),
		orderBy("createdAt")
	);
	const unSub = onSnapshot(q, async (sss) => {
		let orders: TopupTypes.TopupOrder[] = [];
		sss.forEach((ss) => {
			const id = ss.id;
			const data = fromDB(ss.data());
			orders.push({
				id,
				...data,
			} as TopupTypes.TopupOrder);
		});
		store.dispatch(setTopupOrders(orders));
	});
	allUnSubs.push(unSub);
};

export const getTopupOrderById = async (id: string) => {
	const docRef = doc(getFirestore(), `topUpRecords/${id}`);
	const ss = await getDoc(docRef);
	if (!ss.exists()) {
		return null;
	}
	const data = fromDB(ss.data());
	const order: any = { ...data, id: ss.id };
	return order;
};

// update emergency contact
export const updateEContact = async (
	user: UserTypes.Customer,
	contact: UserTypes.EmergencyContact
) => {
	const authUserDocRef = doc(getFirestore(), `customers/${user.id}`);
	await setDoc(authUserDocRef, { emergencyContact: contact }, { merge: true });
	return "success";
};

// storage listener
export const storageListener = async (authUser: Customer) => {
	const customerEmail = authUser.email;
	if (!customerEmail) {
		return;
	}
	const qq = query(
		collection(getFirestore(), `storage`),
		where("customerEmail", "==", customerEmail)
	);
	const unSub = onSnapshot(qq, async (sss) => {
		let storages: GearTypes.GearStorage[] = [];
		sss.forEach((ss) => {
			const id = ss.id;
			const data = fromDB(ss.data());
			storages.push({
				id,
				...data,
			} as GearTypes.GearStorage);
		});
		store.dispatch(setStorages(storages));
	});
	allUnSubs.push(unSub);
};

// Functions for FAQ
export const faqListener = async () => {
	const q = query(collection(getFirestore(), `faqCategories`));
	const qq = query(collection(getFirestore(), `faqQuestions`));
	const unSub = onSnapshot(q, async (sss) => {
		let faqCats: FaqCat[] = [];
		sss.forEach((ss) => {
			const data = fromDB(ss.data());
			const id = ss.id;
			faqCats.push({
				id,
				...data,
			} as FaqCat);
		});
		store.dispatch(setAllFaqCat(faqCats));
		store.dispatch(setFaqIsInit(true));
	});

	const unSub2 = onSnapshot(qq, async (sss) => {
		let faqQs: QNA[] = [];
		sss.forEach((ss) => {
			const data = fromDB(ss.data());
			const id = ss.id;
			faqQs.push({
				id,
				...data,
			} as QNA);
		});
		store.dispatch(setAllFaqQuestion(faqQs));
		store.dispatch(setFaqIsInit(true));
	});
	publicUnSubs.push(unSub);
	publicUnSubs.push(unSub2);
};

export const getMaintenencesByDate = async (date: Date) => {
	const qq = query(
		collection(getFirestore(), `maintenences`),
		where(
			"start",
			"<=",
			Timestamp.fromDate(dayjs.tz(date).startOf("day").add(1, "day").toDate())
		),
		where(
			"start",
			">=",
			Timestamp.fromDate(dayjs.tz(date).subtract(3, "months").toDate())
		)
	);

	const sss = await getDocs(qq);
	const results: VenueTypes.MaintenanceSession[] = [];
	sss.forEach((ss) => {
		const id = ss.id;
		const data = fromDB(ss.data());
		results.push({ id, ...data } as VenueTypes.MaintenanceSession);
	});

	return results;
};

// Customer Classes
export const CustomerClassesListener = async () => {
	const q = query(collection(getFirestore(), `customerClasses`));

	const unSub = onSnapshot(q, async (sss) => {
		const customerClasses: UserTypes.CustomerClass[] = [];
		sss.forEach((ss) => {
			const id = ss.id;
			const data = fromDB(ss.data());
			customerClasses.push({ id, ...data } as UserTypes.CustomerClass);
		});
		store.dispatch(setCustomerClasses(customerClasses));
	});
	allUnSubs.push(unSub);
};

// functions for coupons

export const getCouponByCode = async (couponCode: string) => {
	const q = query(
		collection(getFirestore(), `coupons`),
		where("code", "==", couponCode),
		limit(1)
	);
	const sss = await getDocs(q);
	let coupon: Coupon | null = null;
	if (sss.empty) {
		return null;
	}
	sss.forEach((ss) => {
		const id = ss.id;
		const data = fromDB(ss.data());
		coupon = { id, ...data } as Coupon;
	});
	return coupon;
};

// homepage content functions
export const homepageSlidesListener = async () => {
	const qq = query(collection(getFirestore(), `homepageSlides`));
	const unSub = onSnapshot(qq, async (sss) => {
		let slides: HomePageSlide[] = [];
		sss.forEach((ss) => {
			const id = ss.id;
			const data = fromDB(ss.data());
			slides.push({ id, ...data } as HomePageSlide);
		});
		store.dispatch(setHomepageSlides(slides));
	});
	allUnSubs.push(unSub);
};

export const getCounterByDate = async (date: Date) => {
	const monthStart = dayjs.tz(date).startOf("month").toDate();
	const monthEnd = dayjs.tz(date).endOf("month").toDate();
	const q = query(
		collection(getFirestore(), `counters`),
		where(`month`, `>=`, Timestamp.fromDate(monthStart)),
		where(`month`, `<=`, Timestamp.fromDate(monthEnd)),
		limit(1)
	);
	const sss = await getDocs(q);
	let counter: MonthCounters[] = [];
	sss.forEach((ss) => {
		const id = ss.id;
		const data = fromDB(ss.data());
		counter.push({ id, ...data } as MonthCounters);
	});
	if (counter.length !== 1) {
		return null;
	}
	return counter[0];
};

export const getCounterByDateV2 = async (date: Date) => {
	const dateStart = dayjs.tz(date).startOf("date").toDate();
	const dateEnd = dayjs.tz(date).endOf("date").toDate();
	const q = query(
		collection(getFirestore(), `sessions`),
		where(`sessionDate`, `>=`, Timestamp.fromDate(dateStart)),
		where(`sessionDate`, `<=`, Timestamp.fromDate(dateEnd))
	);
	const sessions: MonthSession[] = [];
	const sss = await getDocs(q);
	sss.forEach((ss) => {
		const data = fromDB(ss.data());
		const session = { id: ss.id, ...data } as Session;
		const {
			id,
			sessionType,
			start,
			end,
			pax,
			playType,
			gears,
			groupClass,
			privateCoach,
			status,
		} = session;
		if (status === "confirmed" || status === "locked") {
			sessions.push({
				id,
				sessionType,
				start,
				end,
				pax,
				playType,
				gears,
				groupClass,
				privateCoach,
			} as MonthSession);
		}
	});

	const tempCounters: MonthCounters = {
		id: dayjs.tz(date).format(`YYYYMMDD`),
		sessions,
		month: dateStart,
	};

	console.log(
		`sessions for date: ${dayjs.tz(date).format(`YYYY-MM-DD`)}`,
		tempCounters
	);

	return tempCounters;
};

// Group Class functions
export const setGroupClassPreset = async (
	preset: GroupClassTypes.GroupClassPreset
) => {
	const { id, ...payload } = preset;
	if (id === "new") {
		await addDoc(collection(getFirestore(), `groupClassPresets`), payload);
		return;
	} else {
		await setDoc(doc(getFirestore(), `groupClassPresets/${id}`), payload, {
			merge: true,
		});
		return;
	}
};

export const deleteGroupClassPreset = async (id: string) => {
	await deleteDoc(doc(getFirestore(), `groupClassPresets/${id}`));
	return;
};

export const groupClassPresetsListener = async () => {
	const q = query(collection(getFirestore(), `groupClassPresets`));

	const unSub = onSnapshot(q, async (sss) => {
		const groupClassPresets: GroupClassTypes.GroupClassPreset[] = [];
		sss.forEach((ss) => {
			const id = ss.id;
			const data = fromDB(ss.data());
			groupClassPresets.push({
				id,
				...data,
			} as GroupClassTypes.GroupClassPreset);
		});
		store.dispatch(setGroupClassPresets(groupClassPresets));
	});
	allUnSubs.push(unSub);
};

export const setGroupClassSlot = async (
	item: GroupClassTypes.GroupClassSlot
) => {
	const { id, ...payload } = item;
	if (id === "new") {
		await addDoc(collection(getFirestore(), `groupClassSlots`), payload);
		return;
	} else {
		await setDoc(doc(getFirestore(), `groupClassSlots/${id}`), payload, {
			merge: true,
		});
		return;
	}
};

export const getGroupClassSlotsByDate = async (date: Date) => {
	const dateStart = dayjs.tz(date).startOf("day").toDate();
	const dateEnd = dayjs.tz(date).endOf("day").toDate();
	const q = query(
		collection(getFirestore(), `groupClassSlots`),
		where("date", ">=", Timestamp.fromDate(dateStart)),
		where("date", "<=", Timestamp.fromDate(dateEnd))
	);
	const sss = await getDocs(q);
	const slots: GroupClassTypes.GroupClassSlot[] = [];
	sss.forEach((ss) => {
		const id = ss.id;
		const data = fromDB(ss.data());
		slots.push({ id, ...data } as GroupClassTypes.GroupClassSlot);
	});
	return slots;
};

export const getRGroupClassSlotsByDay = async (day: number) => {
	const q = query(
		collection(getFirestore(), `groupClassSlots`),
		where("dayOfWeek.index", "==", day)
	);
	const sss = await getDocs(q);
	const slots: GroupClassTypes.GroupClassSlot[] = [];
	sss.forEach((ss) => {
		const id = ss.id;
		const data = fromDB(ss.data());
		slots.push({ id, ...data } as GroupClassTypes.GroupClassSlot);
	});
	return slots;
};

export const getGroupClassSlotsSinceToday = async () => {
	const q = query(
		collection(getFirestore(), `groupClassSlots`),
		where("date", ">=", Timestamp.fromDate(new Date()))
	);
	const sss = await getDocs(q);
	const slots: GroupClassTypes.GroupClassSlot[] = [];
	sss.forEach((ss) => {
		const id = ss.id;
		const data = fromDB(ss.data());
		slots.push({ id, ...data } as GroupClassTypes.GroupClassSlot);
	});
	return slots;
};

export const getGroupClassSlotsByRange = async (from: Date, to: Date) => {
	const q = query(
		collection(getFirestore(), `groupClassSlots`),
		where("date", ">=", Timestamp.fromDate(from)),
		where("date", "<=", Timestamp.fromDate(to))
	);
	const rq = query(
		collection(getFirestore(), `groupClassSlots`),
		where("isRecurring", "==", true)
	);

	const sss = await getDocs(q);
	const rsss = await getDocs(rq);

	const slots: GroupClassTypes.GroupClassSlot[] = [];

	const addSlot = (ss: QueryDocumentSnapshot<DocumentData>) => {
		const id = ss.id;
		const data = fromDB(ss.data());
		slots.push({ id, ...data } as GroupClassTypes.GroupClassSlot);
	};

	sss.forEach((ss) => addSlot(ss));
	rsss.forEach((ss) => addSlot(ss));

	return slots;
};

export const deleteGroupClassSlot = async (id: string) => {
	await deleteDoc(doc(getFirestore(), `groupClassSlots/${id}`));
	return;
};

// Coach Schedule Function
export const getCoachSchedulesByRange = async (from: Date, to: Date) => {
	const fromTS = Timestamp.fromDate(from);
	const toTS = Timestamp.fromDate(to);

	const q = query(
		collection(getFirestore(), `coachSchedules`),
		where("date", ">=", fromTS),
		where("date", "<=", toTS)
	);
	const rq = query(
		collection(getFirestore(), `coachSchedules`),
		where("isRecurring", "==", true)
	);
	const schedules: CoachSchedule[] = [];
	const sss = await getDocs(q);
	const rsss = await getDocs(rq);
	sss.forEach((ss) => {
		const id = ss.id;
		const data = fromDB(ss.data());
		schedules.push({ id, ...data } as CoachSchedule);
	});
	rsss.forEach((ss) => {
		const id = ss.id;
		const data = fromDB(ss.data());
		schedules.push({ id, ...data } as CoachSchedule);
	});
	return schedules;
};

export const getRecurringCoachSchedules = async () => {
	const rq = query(
		collection(getFirestore(), `coachSchedules`),
		where("isRecurring", "==", true)
	);
	const schedules: CoachSchedule[] = [];
	const rsss = await getDocs(rq);
	rsss.forEach((ss) => {
		const id = ss.id;
		const data = fromDB(ss.data());
		schedules.push({ id, ...data } as CoachSchedule);
	});
	return schedules;
};

export const createNewCoachSchedule = async (
	schedule: CoachTypes.CoachSchedule
) => {
	const { id, ...data } = schedule;
	const payload = toDB(data);
	await addDoc(collection(getFirestore(), `coachSchedules`), payload);
	return "success";
};

export const updateCoachSchedule = async (
	schedule: CoachTypes.CoachSchedule
) => {
	const { id, ...data } = schedule;
	const payload = toDB(data);

	await setDoc(doc(getFirestore(), `coachSchedules/${id}`), payload, {
		merge: true,
	});
	return "success";
};

export const deleteCoachSchedule = async (id: string) => {
	await deleteDoc(doc(getFirestore(), `coachSchedules/${id}`));
	return "success";
};

//Pricing
export const pricingListener = async () => {
	const q = query(collection(getFirestore(), `pricing`), limit(1));
	const unSub = onSnapshot(q, async (sss) => {
		let pricing: Price[] = [];
		sss.forEach((ss) => {
			const data = fromDB(ss.data());
			pricing.push({ ...data } as Price);
		});
		if (pricing.length !== 1) {
			return;
		}
		store.dispatch(setPricing(pricing[0]));
		store.dispatch(setPricingIsInit(true));
	});
	allUnSubs.push(unSub);
};

// func for page content
export const pageContentsListener = async () => {
	const q = query(collection(getFirestore(), `pageContents`));
	const unSub = onSnapshot(q, async (sss) => {
		const pageContents: PageContent[] = [];
		sss.forEach((ss) => {
			const id = ss.id;
			const data = fromDB(ss.data());
			pageContents.push({ id, ...data } as PageContent);
		});
		store.dispatch(setAllPageContents(pageContents));
	});
	allUnSubs.push(unSub);
};

// func for other contents
export const otherContentsListener = async () => {
	const q = query(collection(getFirestore(), `otherContents`));
	const unSub = onSnapshot(q, async (sss) => {
		const contents: OtherContent[] = [];
		sss.forEach((ss) => {
			const id = ss.id;
			const data = fromDB(ss.data());
			contents.push({ id, ...data } as OtherContent);
		});
		store.dispatch(setAllOtherContents(contents));
	});
	allUnSubs.push(unSub);
};

const privateListeners = async () => {
	gearListener();
	// monthCountersListener();
};

const publicListeners = async () => {
	capListener();
	faqListener();
	CustomerClassesListener();
	topupPackagesListener();
	homepageSlidesListener();
	groupClassPresetsListener();
	pricingListener();
	pageContentsListener();
	otherContentsListener();
};
