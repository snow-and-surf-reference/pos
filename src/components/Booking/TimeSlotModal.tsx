import {
	IonButton,
	IonButtons,
	IonCard,
	IonCardContent,
	IonCheckbox,
	IonChip,
	IonContent,
	IonFooter,
	IonHeader,
	IonIcon,
	IonItem,
	IonLabel,
	IonList,
	IonModal,
	IonToolbar
} from "@ionic/react";
import * as CoachTypes from "@tsanghoilun/snow-n-surf-interface/types/coach";
import { GroupClassSlot } from "@tsanghoilun/snow-n-surf-interface/types/groupClass";
import * as SessionsTypes from "@tsanghoilun/snow-n-surf-interface/types/session";
import * as VenueType from "@tsanghoilun/snow-n-surf-interface/types/venue";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import {
	checkmarkCircleOutline,
	chevronBackOutline,
	timeOutline
} from "ionicons/icons";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import {
	getCoachScheduleByDate, getCounterByDateV2,
	getGroupClassSlotsByDate,
	getMaintenencesByDate,
	getRecurringCoachSchedules,
	getRGroupClassSlotsByDay
} from "../../app/firebase/global";
import {
	handleSlots,
	SlotsRawMaterials
} from "../../app/functions/handleSlots";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

interface Props {
	isOpen: boolean;
	onClose: () => void;
	onSubmit: (sessionClone: SessionsTypes.Session) => void;
	onUpdate: (daySlots: number | "loading") => void;
	session: SessionsTypes.Session;
	caps: VenueType.Capacities | null | undefined;
	isHalfPast: boolean;
	onChangeHalfPast: () => void;
}

interface TimeSlot {
	time: number;
	slots: number;
	checked: boolean;
}

// a simple hard coded bool for allowing half past btn or not
const allowHalfPast = false;

const TimeSlotModal: React.FC<Props> = (props) => {
	const { t } = useTranslation();
	const {
		isOpen,
		onClose,
		onSubmit,
		onUpdate,
		session,
		caps,
		isHalfPast,
		onChangeHalfPast,
	} = props;
	const [slots, setSlots] = useState<TimeSlot[]>([]);
	const [daySlots, setDaySlots] = useState<number | "loading">(0);
	const [sessionClone, setSessionClone] = useState(session);

	useEffect(() => {
		setSessionClone(session);
	}, [session]);

	// effect to update the slots as soon as session date changed
	useEffect(() => {
		const updateSlots = async () => {
			if (!caps) {
				return;
			}

			if (session.start && session.end) {
				return;
			}

			setDaySlots("loading");

			const counters: SessionsTypes.MonthCounters | null =
				await getCounterByDateV2(session.sessionDate);
			if (!counters) {
				return;
			}

			const singleCoachSchedules: CoachTypes.CoachSchedule[] =
				await getCoachScheduleByDate(session.sessionDate);
			const allRecurringCoachSchedules: CoachTypes.CoachSchedule[] =
				await getRecurringCoachSchedules();
			const recurringCoachSchedulesByDay = allRecurringCoachSchedules.filter(
				(x) => {
					const isRightDay =
						x.daysOfWeek?.filter(
							(y) => y.index === dayjs.tz(session.sessionDate).get("day")
						).length === 1;
					const isValidDate = x.endDate
						? x.endDate.valueOf() >= session.sessionDate.valueOf()
						: true;
					const isValidStartDate = x.startDate
						? x.startDate.valueOf() <= session.sessionDate.valueOf()
						: true;
					return isRightDay && isValidDate && isValidStartDate;
				}
			);

			// get coach schedules for use later
			const coachSchedules = singleCoachSchedules.concat(
				recurringCoachSchedulesByDay
			);

			const groupClassSlots: GroupClassSlot[] = await getGroupClassSlotsByDate(
				dayjs.tz(session.sessionDate).startOf("day").toDate()
			);
			const recurringSlots: GroupClassSlot[] = await getRGroupClassSlotsByDay(
				dayjs.tz(session.sessionDate).get("day")
			);
			const allClassSlots = groupClassSlots
				.concat(recurringSlots)
				.filter((x) => {
					const rStartDate = x.startDate || dayjs.tz(dayjs()).startOf("day");
					const rEndDate = x.endDate || dayjs.tz(`2099-12-31`).startOf("day");
					const isValidDate =
						rStartDate.valueOf() <= session.sessionDate.valueOf() &&
						rEndDate.valueOf() >= session.sessionDate.valueOf();
					return isValidDate;
				});

			// get maintenence sessions
			const maintenences: VenueType.MaintenanceSession[] =
				await getMaintenencesByDate(
					dayjs.tz(session.sessionDate).startOf("day").toDate()
				);

			const materials: SlotsRawMaterials = {
				caps,
				counters,
				coachSchedules,
				maintenences,
				sessionType: session.sessionType,
				playType: session.playType,
				sessionDate: session.sessionDate,
				sessionStart: session.start,
				sessionEnd: session.end,
				isHalfPast: isHalfPast,
				allClassSlots,
			};

			const results = handleSlots(materials);

			const tempSlots: TimeSlot[] = results?.tempSlots || [];
			const daySlots = results?.daySlots || 0;

			setDaySlots(daySlots);
			setSlots(tempSlots);
		};
		if (session.sessionType !== "groupClass") {
			updateSlots();
		}
	}, [
		caps,
		session.sessionType,
		session.playType,
		session.sessionDate,
		session.start,
		session.end,
		isHalfPast,
	]);

	useEffect(() => {
		onUpdate(daySlots);
	}, [daySlots, onUpdate]);

	useEffect(() => {
		let allItems = [...slots];
		let changeCount = 0;
		const checkedItems = [...slots.filter((x) => x.checked)];
		if (checkedItems.length === 0) {
			setSessionClone((s) =>
				Object.assign(
					{ ...s },
					{
						start: null,
						end: null,
					}
				)
			);
			return;
		}
		if (checkedItems.length === 1) {
			const item = checkedItems[0];
			setSessionClone((s) =>
				Object.assign(
					{ ...s },
					{
						start: dayjs.tz(item.time).toDate(),
						end: dayjs.tz(item.time).add(1, "hour").toDate(),
					}
				)
			);
			return;
		}
		checkedItems.forEach((item, idx) => {
			if (idx === 0) {
				return;
			}
			const initTime = dayjs.tz(checkedItems[0].time);
			const itemTime = dayjs.tz(item.time);
			const diff = Math.abs(initTime.diff(itemTime, "hour"));
			// console.log(idx, `diff > idx?`, diff > idx);
			if (diff > idx) {
				const originIdx = allItems.findIndex((x) => x.time === item.time);
				if (originIdx < 0) {
					return;
				}
				allItems[originIdx] = Object.assign({ ...item }, { checked: false });
				changeCount++;
			}
		});
		if (changeCount > 0) {
			setSlots(allItems);
		}
		const newCheckedSlots = [...allItems].filter((x) => x.checked);
		const start = dayjs.tz(newCheckedSlots[0].time).toDate();
		const end = dayjs
			.tz(newCheckedSlots[newCheckedSlots.length - 1].time)
			.add(1, "hour")
			.toDate();
		setSessionClone((s) =>
			Object.assign(
				{ ...s },
				{
					start: start,
					end: end,
				}
			)
		);
	}, [slots]);

	const handleSubmit = () => {
		// console.log(sessionClone);
		onSubmit(sessionClone);
	};

	return (
		<IonModal isOpen={isOpen} onDidDismiss={() => onClose()}>
			{caps ? (
				<>
					<IonHeader color="dark">
						<IonItem lines="none" color={`dark`} onClick={() => onClose()}>
							<IonButton slot="start">
								<IonIcon icon={chevronBackOutline} slot="start" />
								<IonLabel>Back</IonLabel>
							</IonButton>
							<IonLabel slot="end" className="ion-text-end">
								<h3>
									{t(`date`)}:{" "}
									<b>
										{sessionClone.sessionDate
											? dayjs
													.tz(sessionClone.sessionDate)
													.format(`DD MMM YYYY (ddd)`)
											: ""}
									</b>
								</h3>
								<h3>
									{t(`time`)}:{" "}
									{sessionClone.start && sessionClone.end ? (
										<b>
											{`${dayjs
												.tz(sessionClone.start)
												.format(`HH:mm`)} - ${dayjs
												.tz(sessionClone.end)
												.format(`HH:mm`)}`}
											{` (${dayjs
												.tz(sessionClone.end)
												.diff(dayjs.tz(sessionClone.start), "hours")} Hour${
												dayjs
													.tz(sessionClone.end)
													.diff(dayjs.tz(sessionClone.start), "hours") > 1
													? "s"
													: ""
											})`}
										</b>
									) : (
										t(`pleaseSelect`)
									)}
								</h3>
							</IonLabel>
						</IonItem>
						{allowHalfPast ? (
							<IonItem color={"light"} lines="none">
								<IonButtons slot="start">
									<IonChip
										color={isHalfPast ? `secondary` : `danger`}
										outline
										onClick={() => {
											onChangeHalfPast();
										}}
									>
										<IonIcon icon={timeOutline} />
										<IonLabel>
											{t(`switchTo`)} {isHalfPast ? t(`oClock`) : t(`halfPast`)}
										</IonLabel>
									</IonChip>
								</IonButtons>
							</IonItem>
						) : null}
					</IonHeader>
					<IonContent>
						{session.sessionType === "privateClass" ? (
							<IonCard>
								<IonCardContent>
									<IonLabel>
										{t(`Please contact us if you want to book more than 1 PT.`)}
									</IonLabel>
								</IonCardContent>
							</IonCard>
						) : null}
						
						<IonList
							style={{
								padding: "1rem",
							}}
						>
							{slots.map((item, index) => (
								<IonItem
									// button
									key={`slot-item-${index}`}
									color={item.checked ? "success" : "light"}
									lines="none"
									style={{
										marginBottom: "6px",
										borderRadius: "6px",
									}}
								>
									<IonLabel>
										<p>
											{`${dayjs.tz(item.time).format(`HH:mm`)} - ${dayjs
												.tz(item.time)
												.add(1, "h")
												.format(`HH:mm`)}`}
										</p>
									</IonLabel>
									<IonCheckbox
										slot="start"
										disabled={
											item.slots <= 0 ||
											item.slots < session.pax ||
											dayjs.tz(item.time).valueOf() <
												dayjs.tz(dayjs()).valueOf()
										}
										checked={item.checked}
										onIonChange={(e) => {
											let tempSlots = [...slots];
											let tempslot = Object.assign(
												{ ...tempSlots[index] },
												{ checked: e.detail.checked }
											);
											tempSlots[index] = tempslot;
											setSlots(tempSlots);
										}}
									/>
									<IonButtons slot="end">
										<IonChip
											color={
												item.checked
													? `dark`
													: item.slots > 9
													? `success`
													: item.slots > 4
													? `warning`
													: item.slots > 0
													? `danger`
													: `light`
											}
											outline
										>
											{item.slots > 0
												? session.sessionType === "privateClass" || session.sessionType === 'privateBelt'
													? t("Available")
													: item.slots < session.pax
													? `Less than ${session.pax}`
													: `${item.slots} ${
															item.slots > 1 ? t(`spaces`) : t(`space`)
													  }`
												: `Sold Out`}
										</IonChip>
									</IonButtons>
								</IonItem>
							))}
						</IonList>
					</IonContent>
				</>
			) : null}

			<IonFooter>
				<IonToolbar color="dark">
					<IonButton
						onClick={() => {
							handleSubmit();
						}}
						expand="block"
					>
						<IonIcon icon={checkmarkCircleOutline} />
						<IonLabel>{t(`update`)}</IonLabel>
					</IonButton>
				</IonToolbar>
			</IonFooter>
		</IonModal>
	);
};

export default TimeSlotModal;
