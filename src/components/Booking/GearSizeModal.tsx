import {
  IonButton,
  IonCard,
  IonChip,
  IonCol,
  IonContent,
  IonFooter,
  IonGrid,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonModal,
  IonRow,
  IonSegment,
  IonSegmentButton,
  IonSelect,
  IonSelectOption,
  IonTitle,
  IonToolbar
} from "@ionic/react";
import * as GearTypes from "@tsanghoilun/snow-n-surf-interface/types/gear";
import * as SessionTypes from "@tsanghoilun/snow-n-surf-interface/types/session";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import { checkmarkCircleOutline, closeOutline } from "ionicons/icons";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { getCounterByDateV2 } from "../../app/firebase/global";
import { handleGearsStock } from "../../app/functions/handleSlots";
import { useAppSelector } from "../../app/hooks";
import { selectGears } from "../../app/slices/gearSlice";
import {
  selectTempSession
} from "../../app/slices/sessionsSclice";
import { GearsByPax } from "../../pages/Booking/NewBooking";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

interface Props {
  isOpen: boolean;
  onClose: () => void;
  onSubmit: (gearByPax: GearsByPax) => void;
  gearByPax: GearsByPax | undefined;
}

const GearSizeModal: React.FC<Props> = (props) => {
  const { t } = useTranslation();
  const { isOpen, onClose, onSubmit, gearByPax } = props;
  const [stateGearByPax, setStateGearByPax] = useState<GearsByPax | undefined>(
    undefined
  );
  const allGearsRaw = useAppSelector(selectGears);
  const [allGears, setAllGears] = useState(allGearsRaw);
  // const monthCounters = useAppSelector(selectMonthCounters);
  const [playType, setPlayType] = useState<SessionTypes.PlayType>(
    gearByPax ? gearByPax.type : "ski"
  );
  const session = useAppSelector(selectTempSession);

  const interfaceOptions = {
    cssClass: "cusom-select-interface",
  };

  useEffect(() => {
    if (!gearByPax) {
      return;
    }
    setPlayType(gearByPax.type);
  }, [gearByPax]);

  useEffect(() => {
    if (!gearByPax || allGears.length <= 0) {
      return;
    }
    let cleanGearByPax = { ...gearByPax };
    allGears.forEach((gear) => {
      const newBookingGear: SessionTypes.BookingGear = {
        size: gearByPax.gears.find((x) => x.gear.id === gear.id)?.size || "",
        gear,
        class: `standard`,
      };
      const idx = cleanGearByPax.gears.findIndex((x) => x.gear.id === gear.id);
      if (idx < 0) {
        cleanGearByPax.gears.push(newBookingGear);
      } else {
        cleanGearByPax.gears[idx] = newBookingGear;
      }
    });
    setStateGearByPax(cleanGearByPax);

    return () => {
      setStateGearByPax(undefined);
    };
  }, [gearByPax, playType, allGears]);

  useEffect(() => {
    // const thisMonthCounter = monthCounters.find((x) =>
    //   dayjs
    //     .tz(x.month)
    //     .startOf("month")
    //     .isSame(dayjs.tz(session.start).startOf("month"))
    // );
    // if (!thisMonthCounter || !session || !session.start || !session.end) {
    //   return;
    // }

    // const tempGears = handleGearsStock(allGearsRaw, thisMonthCounter, session);

    // setAllGears(tempGears);
    const getLatestData = async () => {
			if (!session || !session.start || !session.end) {
				return
			}
			const thisMonthCounter = await getCounterByDateV2(session.sessionDate);
			const tempGears = handleGearsStock(
				allGearsRaw,
				thisMonthCounter,
				session
			);
			setAllGears(tempGears);
		}

		getLatestData();
  }, [session, allGearsRaw]);

  const handleUpdate = (
    gear: GearTypes.Gear,
    size: string | number,
    gearClass: GearTypes.GearClass
  ) => {
    if (!stateGearByPax) {
      return;
    }
    let tempGearPax: GearsByPax = { ...stateGearByPax };
    const existIdx = tempGearPax.gears.findIndex((x) => x.gear.id === gear.id);

    const temp: SessionTypes.BookingGear = {
      class: gearClass,
      size,
      gear,
    };

    if (existIdx < 0) {
      tempGearPax.gears.push(temp);
    } else {
      tempGearPax.gears[existIdx] = temp;
    }

    tempGearPax.gears
      .filter((x) => x.gear.type !== playType)
      .forEach((y) => (y.size = ""));

    setStateGearByPax(tempGearPax);
  };

  return (
    <IonModal
      isOpen={isOpen}
      onDidDismiss={() => {
        onClose();
      }}
    >
      {stateGearByPax && allGears && (
        <>
          <IonHeader>
            <IonToolbar color="dark">
              <IonTitle>{t(`selectGears`)}</IonTitle>
              <IonButton slot="start" onClick={() => onClose()}>
                <IonIcon icon={closeOutline} slot="start" />
                <IonLabel>{t(`cancel`)}</IonLabel>
              </IonButton>
            </IonToolbar>
          </IonHeader>
          <IonContent>
            <IonCard color={`medium`}>
              <IonSegment
                color={`light`}
                value={
                  playType === "snow" || playType === "all" ? "ski" : playType
                }
                onIonChange={(e) => {
                  setPlayType(e.detail.value as SessionTypes.PlayType);
                }}
              >
                {session.sessionType === "privateBelt" ||
                session.sessionType === "single" ||
                session.sessionType === "fullSnow" ||
                session.sessionType === "fullPark" ? (
                  <>
                    <IonSegmentButton value="ski">{t(`ski`)}</IonSegmentButton>
                    <IonSegmentButton value="sb">{t(`sb`)}</IonSegmentButton>
                  </>
                ) : (
                  <>
                    <IonSegmentButton value={playType}>
                      {playType === "ski" ? t(`ski`) : t(`sb`)}
                    </IonSegmentButton>
                  </>
                )}
              </IonSegment>
            </IonCard>
            <IonGrid>
              <IonRow>
                <IonCol className="ion-padding-start">
                  <IonLabel>
                    <h2>
                      <b>{t(`Select Gear's Size`)}</b>
                    </h2>
                  </IonLabel>
                </IonCol>
              </IonRow>
            </IonGrid>
            <br />
            <IonItem className="" lines="none">
              <IonChip slot="start">{t(`standard`)}</IonChip>
              <IonLabel className="ion-text-end">
                <p>
                  50 {t(`credits`)} {t(`per item`)}
                </p>
              </IonLabel>
            </IonItem>
            <IonList inset color="light">
              {allGears
                .filter(
                  (x) =>
                    x.type ===
                    (playType === "snow" || playType === "all"
                      ? "ski"
                      : playType)
                )
                .map((item) => (
                  <IonItem key={`gear-item-${item.id}`} color={`light`}>
                    <IonLabel>
                      <h3>
                        <b>{item.name}</b>
                      </h3>
                    </IonLabel>
                    <IonSelect
                      value={
                        stateGearByPax.gears.find((x) => x.gear.id === item.id)
                          ?.size || ""
                      }
                      interface="popover"
                      interfaceOptions={interfaceOptions}
                      onIonChange={(e) => {
                        handleUpdate(item, e.detail.value!, "standard");
                      }}
                    >
                      <IonSelectOption
                        value={""}
                        className="gear-select-option"
                      >
                        <b>{t(`Not Renting`)}</b>
                      </IonSelectOption>
                      {item.stock
                        .filter((x) => x.class === "standard")
                        .map((s) => (
                          <IonSelectOption
                            key={`gear-${item.id}-class-${s.class}-size-${s.size}`}
                            className="gear-select-option"
                            value={s.size}
                            disabled={s.qty <= 0}
                          >
                            {s.qty <= 0
                              ? `${s.size} Out of Stock`
                              : `${s.size} (${item.unit.name})`}
                          </IonSelectOption>
                        ))}
                    </IonSelect>
                  </IonItem>
                ))}
            </IonList>
            <IonGrid>
              <IonRow>
                <IonCol>{t(`gearSelectAdvice`)}</IonCol>
              </IonRow>
            </IonGrid>
            {/* <>
						<br />
						<IonItem className="" lines="none">
							<IonChip slot="start" color="warning">Premium</IonChip>
							<IonLabel className="ion-text-end"><p>100 credits per item</p></IonLabel>
						</IonItem>
						<IonList inset color="light">
							{allGears
								.filter((x) => x.type === playType)
								.map((item) => (
									<IonItem key={`gear-item-${item.id}`} color={`light`}>
										<IonLabel>
											<h3>
												<b>{item.name}</b>
											</h3>
										</IonLabel>
										<IonSelect
											value={null}
											interface="popover"
											interfaceOptions={interfaceOptions}
										>
											<IonSelectOption
												value={null}
												className="gear-select-option"
											>
												<b>Not Renting</b>
											</IonSelectOption>
											{item.stock
												.filter((x) => x.class === "premium")
												.map((s) => (
													<IonSelectOption
														key={`gear-${item.id}-class-${s.class}-size-${s.size}`}
														className="gear-select-option"
													>{`${s.size} (${item.unit.name})`}</IonSelectOption>
												))}
										</IonSelect>
									</IonItem>
								))}
						</IonList>
						</> */}
          </IonContent>
          <IonFooter color="dark">
            <IonToolbar color="dark">
              <IonButton
                expand="block"
                onClick={() => {
                  onSubmit(stateGearByPax);
                  onClose();
                }}
              >
                <IonIcon icon={checkmarkCircleOutline} slot="start" />
                <IonLabel>{t(`update`)}</IonLabel>
              </IonButton>
            </IonToolbar>
          </IonFooter>
        </>
      )}
    </IonModal>
  );
};

export default GearSizeModal;
