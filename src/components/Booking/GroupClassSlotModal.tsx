import {
	IonButton, IonCard, IonCheckbox,
	IonChip,
	IonContent,
	IonFooter,
	IonHeader,
	IonIcon,
	IonItem,
	IonLabel, IonModal,
	IonToolbar
} from "@ionic/react";
import { GroupClassSlot } from "@tsanghoilun/snow-n-surf-interface/types/groupClass";
import * as SessionsTypes from "@tsanghoilun/snow-n-surf-interface/types/session";
import * as VenueType from "@tsanghoilun/snow-n-surf-interface/types/venue";
import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import { checkmarkCircleOutline } from "ionicons/icons";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { getCounterByDateV2, getGroupClassSlotsByDate, getMaintenencesByDate, getRGroupClassSlotsByDay } from "../../app/firebase/global";
import {
	GroupClassSlotsMaterials,
	handleGroupClassSlots, SelectGroupClassSlot
} from "../../app/functions/handleSlots";
import i18n from "../../i18n";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault(`Asia/Hong_Kong`);

interface Props {
	isOpen: boolean;
	onClose: () => void;
	onSubmit: (sessionClone: SessionsTypes.Session) => void;
	onUpdate: (daySlots: number | "loading") => void;
	session: SessionsTypes.Session;
	caps: VenueType.Capacities | null | undefined;
	isHalfPast: boolean;
	onChangeHalfPast: () => void;

}

interface TimeSlot {
	time: number;
	slots: number;
	checked: boolean;
}

const GroupClassSlotModal: React.FC<Props> = (props) => {
	const {
		isOpen,
		onClose,
		onSubmit,
		onUpdate,
		session,
		caps,
		isHalfPast,
	} = props;

	const { t } = useTranslation();
	const [slots, setSlots] = useState<TimeSlot[]>([]);
	const [groupClassSlots, setGroupClassSlots] = useState<SelectGroupClassSlot[]>([]);
	const [daySlots, setDaySlots] = useState<number | 'loading'>(0);
	const [sessionClone, setSessionClone] = useState(session);

	// effect to update the cloned session in this modal, if the parent session has updates
	useEffect(() => {
		setSessionClone(session);
	}, [session]);

	// effect to update the slots as soon as session date changed
	useEffect(() => {
		const updateGroupSlots = async () => {
			if (session.sessionType !== 'groupClass') { return };
			if (!caps) {
				return;
			}
			
			setDaySlots("loading");

			const counters: SessionsTypes.MonthCounters | null = await getCounterByDateV2(session.sessionDate);
			if (!counters) { return };
			
			// get maintenence sessions
			const maintenences: VenueType.MaintenanceSession[] = await getMaintenencesByDate(dayjs.tz(session.sessionDate).startOf('day').toDate());

			const groupClassSlots: GroupClassSlot[] = await getGroupClassSlotsByDate(dayjs.tz(session.sessionDate).startOf('day').toDate());
			const recurringSlotsRaw: GroupClassSlot[] = await getRGroupClassSlotsByDay(dayjs.tz(session.sessionDate).get('day'));
			const recurringSlots = recurringSlotsRaw.filter(x => {
				const rStartDate = x.startDate || dayjs.tz(dayjs()).startOf('day');
				const rEndDate = x.endDate || dayjs.tz(`2099-12-31`).startOf('day');
				const isValidDate = rStartDate.valueOf() <= session.sessionDate.valueOf() && rEndDate.valueOf() >= session.sessionDate.valueOf();
				return isValidDate;
			});

			const materials: GroupClassSlotsMaterials = {
				caps,
				counters,
				maintenences,
				sessionDate: session.sessionDate,
				groupClassSlots,
				recurringSlots
			};

			const results = handleGroupClassSlots(materials);

			// const tempSlots: TimeSlot[] = results?.tempSlots || [];
			const daySlots = results?.daySlots || 0;

			setDaySlots(daySlots);
			setGroupClassSlots(results?.filteredSlots || []);
		};
		updateGroupSlots();
	}, [ caps, session.sessionType, session.sessionDate, isHalfPast]);

	useEffect(() => {
		onUpdate(daySlots)
	}, [daySlots, onUpdate]);

	useEffect(() => {
		let allItems = [...slots];
		let changeCount = 0;
		const checkedItems = [...slots.filter((x) => x.checked)];
		if (checkedItems.length === 0) {
			setSessionClone((s) =>
				Object.assign(
					{ ...s },
					{
						start: null,
						end: null,
					}
				)
			);
			return;
		}
		if (checkedItems.length === 1) {
			const item = checkedItems[0];
			setSessionClone((s) =>
				Object.assign(
					{ ...s },
					{
						start: dayjs.tz(item.time).toDate(),
						end: dayjs.tz(item.time).add(1, "hour").toDate(),
					}
				)
			);
			return;
		}
		checkedItems.forEach((item, idx) => {
			if (idx === 0) {
				return;
			}
			const initTime = dayjs.tz(checkedItems[0].time);
			const itemTime = dayjs.tz(item.time);
			const diff = Math.abs(initTime.diff(itemTime, "hour"));
			// console.log(idx, `diff > idx?`, diff > idx);
			if (diff > idx) {
				const originIdx = allItems.findIndex((x) => x.time === item.time);
				if (originIdx < 0) {
					return;
				}
				allItems[originIdx] = Object.assign({ ...item }, { checked: false });
				changeCount++;
			}
		});
		if (changeCount > 0) {
			setSlots(allItems);
		}
		const newCheckedSlots = [...allItems].filter((x) => x.checked);
		const start = dayjs.tz(newCheckedSlots[0].time).toDate();
		const end = dayjs
			.tz(newCheckedSlots[newCheckedSlots.length - 1].time)
			.add(1, "hour")
			.toDate();
		setSessionClone((s) =>
			Object.assign(
				{ ...s },
				{
					start: start,
					end: end,
				}
			)
		);
	}, [slots]);

	const handleSubmit = () => {
		// console.log(sessionClone);
		onSubmit(sessionClone);
	};

	return (
		<IonModal isOpen={isOpen} onDidDismiss={() => onClose()}>
			{caps ? (
				<>
					<IonHeader color="dark">
						<IonItem lines="none" color={`dark`}>
							<IonLabel>
								<h3>
								{t(`date`)}:{" "}
									<b>
										{sessionClone.sessionDate
											? dayjs
													.tz(sessionClone.sessionDate)
													.format(`DD MMM YYYY (ddd)`)
											: ""}
									</b>
								</h3>
								<h3>
								{t(`time`)}:{" "}
									{sessionClone.start && sessionClone.end ? (
										<b>
											{`${dayjs
												.tz(sessionClone.start)
												.format(`HH:mm`)} - ${dayjs
												.tz(sessionClone.end)
												.format(`HH:mm`)}`}
											{` (${dayjs
												.tz(sessionClone.end)
												.diff(dayjs.tz(sessionClone.start), "hours")} Hour${
												dayjs
													.tz(sessionClone.end)
													.diff(dayjs.tz(sessionClone.start), "hours") > 1
													? "s"
													: ""
											})`}
										</b>
									) : (
										t(`pleaseSelect`)
									)}
								</h3>
							</IonLabel>
						</IonItem>
					</IonHeader>
					<IonContent color={`light`}>
						{
							groupClassSlots.map(x => (
								<IonCard
									key={`group-class-slot-${x.class.id}-time-${x.class.start}`}
								>
									<IonItem lines="none">
										<IonCheckbox
											slot="start"
											checked={sessionClone.groupClass?.id === x.class.id}
											disabled={x.paxSlots <= 0}
											onClick={e => {
												const start = dayjs.tz(`${dayjs.tz(sessionClone.sessionDate).format(`YYYY-MM-DD`)}T${x.class.start}`).toDate();
												const end = dayjs.tz(`${dayjs.tz(sessionClone.sessionDate).format(`YYYY-MM-DD`)}T${x.class.end}`).toDate();
												setSessionClone(Object.assign({ ...sessionClone }, {
													start,
													end,
													groupClass: { ...x.class },
													playType: x.class.classPreset.classType
												}))
											}}
										/>
										<IonLabel>
											<h4>{t(`time`)}: {x.class.start} - {x.class.end}</h4>
											<h2><b>{i18n.language === 'en' ? x.class.classPreset.name.en : (x.class.classPreset.name.zh || x.class.classPreset.name.en)}</b></h2>
										</IonLabel>

										<IonChip slot="end">
											<IonLabel>{`${x.paxSlots} ${x.paxSlots > 1 ? t(`spaces`) : t(`space`)}`}</IonLabel>
										</IonChip>
									</IonItem>
								</IonCard>
							))
						}
					</IonContent>
				</>
			) : null}

			<IonFooter>
				<IonToolbar color="dark">
					<IonButton
						onClick={() => {
							handleSubmit();
						}}
						expand="block"
					>
						<IonIcon icon={checkmarkCircleOutline} />
						<IonLabel>{t(`update`)}</IonLabel>
					</IonButton>
				</IonToolbar>
			</IonFooter>
		</IonModal>
	);
};

export default GroupClassSlotModal;
