import { Customer } from "@tsanghoilun/snow-n-surf-interface/types/user";
import { useEffect } from "react";
import { useHistory, useLocation } from "react-router";
import { useAppSelector } from "../app/hooks";
import { useSearchParams } from "../app/hooks/useSearchParams";
import {
	selectAuthUser
} from "../app/slices/authUserSlice";

interface Props {
	children: any;
	setDevMode: (bool: boolean) => void;
}

const AuthCheck: React.FC<Props> = (props) => {
	const { setDevMode } = props;
	const authUser: Customer | null = useAppSelector(selectAuthUser);
	const location = useLocation();
	const history = useHistory();
	const search = useSearchParams();

	useEffect(() => {
		if (!search.get(`dev`)) {
			return
		}
		if (search.get(`dev`) === `8081`) {
			setDevMode(true);
		}
	}, [search, setDevMode]);

	useEffect(() => {
		const privatePaths = [
			`newBooking`,
			`orderPreview`,
			`checkout`,
			`paymentCompleted`,
			`topup`,
			`myBookings`
		]
		const authPaths = [
			`login`,
			`signup`
		]
		// console.log(location.pathname)
		// if (publicPaths.filter(path => location.pathname.includes(path)).length > 0) { return };
		if (privatePaths.filter(path => location.pathname.includes(path)).length > 0) {
			// console.log('private path')
			if (!authUser) {
				history.replace('/login');
				return
			}
			// console.log('authUser.emailVerified', authUser.emailVerified)
			if (authUser && !authUser.emailVerified) {
				history.replace('/emailNotVerify');
				return
			}
			return
		}
		if (authPaths.filter(path => location.pathname.includes(path)).length > 0) {
			if (authUser) {
				history.replace('/')
				return
			}
			return
		}
	}, [authUser, location, history])

	return props.children;
};

export default AuthCheck;
