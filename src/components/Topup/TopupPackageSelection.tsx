import {
	IonCard,
	IonCardContent,
	IonItem,
	IonIcon,
	IonLabel,
} from "@ionic/react";
import {
	diamondOutline,
	checkmarkCircle,
	ellipseOutline,
} from "ionicons/icons";
// import topupPackages from "../../app/slices/topupPackages";
import * as TopupTypes from "@tsanghoilun/snow-n-surf-interface/types/topUp";
import { useTranslation } from "react-i18next";

interface Props {
	topupPackages: TopupTypes.TopUpPackages[];
	selectedPackage: TopupTypes.TopUpPackages | null;
	mostValuedPackage: TopupTypes.TopUpPackages | null;
	setSelectedPackage: (selectedPackage: TopupTypes.TopUpPackages) => void;
	minimumToOrder?: number;
}

const TopupPackageSelection: React.FC<Props> = (props) => {
	const { t } = useTranslation();
	const {
		topupPackages,
		selectedPackage,
		mostValuedPackage,
		setSelectedPackage,
		minimumToOrder
	} = props;

	return (
		<>
			<div className="ion-padding-start ion-padding-top">
				<IonLabel>
					<h2>
						<b>{t(`Choose Package`)}</b>
					</h2>
				</IonLabel>
            </div>
            
			{[...topupPackages]
				.sort((a, b) => a.price - b.price)
				.map((p) => (
					<IonCard
						key={`topup-package-${p.id}`}
						color={p.id === selectedPackage?.id ? `warning` : `light`}
					>
						<IonCardContent className="ion-no-padding">
							{p.isPopular && <div className="chippy popular">{t(`Popular`)}</div>}
							{p.id === mostValuedPackage?.id && (
								<div className="chippy valued">{t(`Most Valued`)}</div>
							)}
							<IonItem
								lines="none"
								color={p.id === selectedPackage?.id ? `warning` : `light`}
								disabled={!minimumToOrder ? false : p.receivedCredits >= minimumToOrder ? false : true }
								onClick={() => {
									// handleChange()
									setSelectedPackage({ ...p });
								}}
								style={{
									cursor: "pointer",
								}}
							>
								<IonIcon icon={diamondOutline} slot="start" color="tertiary" />
								<IonLabel>
									<h1>
										<b>{p.receivedCredits.toLocaleString()}</b>
									</h1>
									{
										!minimumToOrder || p.receivedCredits >= minimumToOrder ?
										<p>
										HKD ${(p.initCredits / p.receivedCredits).toFixed(2)} = 1 {t(`credits`)}
											</p> : <p>{t(`Not enough for your booking`)}</p>
									}
									
								</IonLabel>
								<IonIcon
									icon={
										p.id === selectedPackage?.id
											? checkmarkCircle
											: ellipseOutline
									}
									slot="end"
									color={p.id === selectedPackage?.id ? "dark" : "dark"}
								/>
							</IonItem>
						</IonCardContent>
					</IonCard>
				))}
		</>
	);
};

export default TopupPackageSelection;
