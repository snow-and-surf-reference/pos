import {
	IonModal,
	IonHeader,
	IonToolbar,
	IonTitle,
	IonContent,
	IonGrid,
	IonRow,
	IonCol,
	IonFooter,
	IonButton,
} from "@ionic/react";
import { useAppSelector } from "../../app/hooks";
import { selectAllOtherContents } from "../../app/slices/otherContentsSlice";
import i18n from "../../i18n";

interface Props {
	showTermsModal: boolean;
	setShowTermsModal: (bool: boolean) => void;
}

const TopupTermsModal: React.FC<Props> = (props) => {
	const { showTermsModal, setShowTermsModal } = props;
	const otherContent = useAppSelector(selectAllOtherContents);

	return (
		<IonModal
			isOpen={showTermsModal}
			onDidDismiss={() => setShowTermsModal(false)}
		>
			<IonHeader>
				<IonToolbar>
					<IonTitle>{`Payment Terms & Conditions`}</IonTitle>
				</IonToolbar>
			</IonHeader>
			<IonContent>
				<IonGrid>
					<IonRow>
						<IonCol>
							<div
								className="ion-padding"
								dangerouslySetInnerHTML={{
									__html:
										i18n.language === "en"
											? String(
													otherContent.find(
														(x) => x.slug === "tnc_modal_content"
													)?.content.en
											  )
											: String(
													otherContent.find(
														(x) => x.slug === "tnc_modal_content"
													)?.content.zh
											  ) ||
											  String(
													otherContent.find(
														(x) => x.slug === "tnc_modal_content"
													)?.content.en
											  ),
								}}
							></div>
						</IonCol>
					</IonRow>
				</IonGrid>
			</IonContent>
			<IonFooter>
				<IonToolbar>
					<IonButton expand="block" onClick={() => setShowTermsModal(false)}>
						Close
					</IonButton>
				</IonToolbar>
			</IonFooter>
		</IonModal>
	);
};

export default TopupTermsModal;
