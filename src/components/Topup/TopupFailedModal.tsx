import { IonModal, IonHeader, IonToolbar, IonTitle, IonContent, IonCard, IonCardContent, IonLabel, IonGrid, IonRow, IonCol, IonButton } from "@ionic/react";
import { useTranslation } from "react-i18next";
import { BiError } from "react-icons/bi";

interface Props{
    showPaymentFailedModal: boolean;
    setShowPaymentFailedModal: (bool: boolean) => void;
}

const TopupFailedModal: React.FC<Props> = (props) => {
	const { t } = useTranslation();
    const { showPaymentFailedModal, setShowPaymentFailedModal } = props;
    return (
        <IonModal
				isOpen={showPaymentFailedModal}
				onDidDismiss={() => setShowPaymentFailedModal(false)}
			>
				<IonHeader>
					<IonToolbar>
					<IonTitle>{t(`Payment Failed`)}</IonTitle>
					</IonToolbar>
				</IonHeader>
				<IonContent>
					<IonCard color={"danger"}>
						<IonCardContent className="ion-text-center">
							<IonLabel>
								<BiError size={64} />
								<h2>
								<b>{t(`Your Payment has been Declined / Reject`)}</b>
								</h2>
							</IonLabel>
						</IonCardContent>
					</IonCard>
					<IonGrid className="ion-text-center">
						<IonRow>
							<IonCol>
								<div>
									{/* <IonIcon
										icon={alertCircleOutline}
										// slot="start"
										color={"tertiary"}
									/> */}
									<IonLabel color={"tertiary"}>
									<h2>{t(`You Credit card has not been charged.`)}</h2>
									</IonLabel>
								</div>
							</IonCol>
						</IonRow>
						<IonRow>
							<IonCol>
								<IonLabel>
									<p>
										{t(`The payment has been rejected by your bank, or there was a technical issue with the transaction. Please try again later.`)}
									</p>
								</IonLabel>
							</IonCol>
						</IonRow>
						<br/>
						<IonRow>
							<IonCol>
								<IonButton
									// expand="block"
									color={'warning'}
									onClick={() => setShowPaymentFailedModal(false)}
								>
								<IonLabel><b>{t(`back`)}</b></IonLabel>
								</IonButton>
							</IonCol>
						</IonRow>
					</IonGrid>
				</IonContent>
			</IonModal>
    )
}

export default TopupFailedModal;