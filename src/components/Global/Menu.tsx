import {
	IonAccordion,
	IonAccordionGroup,
	IonButton,
	IonContent,
	IonFooter,
	IonIcon,
	IonItem,
	IonLabel,
	IonList,
	IonMenu,
	IonToolbar
} from "@ionic/react";
import {
	addOutline, barbell, languageOutline,
	lockClosed, people, ticketOutline
} from "ionicons/icons";
import { useRef } from "react";
import { useTranslation } from "react-i18next";
import { HiHome } from "react-icons/hi";
import { IoAlertCircleOutline, IoBarbellOutline, IoCalendarNumberOutline, IoHelpCircleOutline, IoMailOutline, IoSnowOutline } from "react-icons/io5";
import { MdOutlineSurfing } from "react-icons/md";
import { useHistory } from "react-router";
import { useAppSelector } from "../../app/hooks";
import { selectAuthUser } from "../../app/slices/authUserSlice";
import { GiPartyPopper } from "react-icons/gi";
import i18n from '../../i18n';

interface MenuItemProps {
	children?: any;
	onClick: () => void;
}
const MenuItem: React.FC<MenuItemProps> = (props) => {
	const { children, onClick } = props;
	return (
		<IonItem
			className="main-menu__item"
			color="transparent"
			button
			onClick={() => onClick()}
			detail={false}
		>
			{children}
		</IonItem>
	);
};

const Menu: React.FC = () => {
	const { t } = useTranslation();
	const history = useHistory();
	const menu = useRef<HTMLIonMenuElement | null>(null);
	const authUser = useAppSelector(selectAuthUser);

	return (
		<IonMenu
			side="start"
			contentId="main"
			menuId="menu"
			type="reveal"
			color="dark"
			ref={menu}
		>
			<IonContent color="dark">
				<IonList
					lines="none"
					color="dark"
					style={{
						marginTop: `12vw`,
					}}
				>
					<MenuItem
						onClick={() => {
							menu.current?.close();
							history.push(`/`);
						}}
					>
						<HiHome className="ion-margin-end"/>
						<IonLabel>{t(`home`)}</IonLabel>
					</MenuItem>
					<IonAccordionGroup color="dark">
						<IonAccordion value="booking" color="dark" toggleIcon={addOutline}>
							<IonItem slot="header" color={"dark"} className="main-menu__item">
								<IoCalendarNumberOutline className="ion-margin-end"/>
								<IonLabel>{t(`bookNow`)}</IonLabel>
							</IonItem>
							<IonList
								slot="content"
								color="dark"
								style={{
									background: `var(--ion-color-dark)`,
								}}
							>
								<IonAccordionGroup>
									<IonAccordion
										value="bookSnow"
										style={{
											background: `var(--ion-color-dark)`,
										}}
									>
										<IonItem
											slot="header"
											color={`dark`}
											className="ion-margin-start"
										>
											<IonLabel>{t(`skiAndSnowboard`)}</IonLabel>
										</IonItem>
										<IonList
											slot="content"
											color="darker"
											style={{
												background: `var(--ion-color-darker)`,
											}}
										>
											<IonItem
												button
												color="darker"
												className="ion-margin-start"
												onClick={() => {
													menu.current?.close();
													// history.push(`/newBookingSnow`);
													window.location.assign(`/newBookingSnow`);
												}}
											>
												<IonIcon
													icon={ticketOutline}
													slot="start"
													size="small"
												/>
												<IonLabel>{t(`parkRide`)}</IonLabel>
											</IonItem>
											{authUser && !authUser.isPartner ? (
												<>
													<IonItem
														button
														color="darker"
														className="ion-margin-start"
														onClick={() => {
															menu.current?.close();
															history.push(`/newBooking/groupClass`);
														}}
													>
														<IonIcon icon={people} slot="start" size="small" />
														<IonLabel>{t(`groupClass`)}</IonLabel>
													</IonItem>
													<IonItem
														button
														color="darker"
														className="ion-margin-start"
														onClick={() => {
															menu.current?.close();
															history.push(`/newBooking/privateClass`);
														}}
													>
														<IonIcon icon={barbell} slot="start" size="small" />
														<IonLabel>{t(`privateTraining`)}</IonLabel>
													</IonItem>
													<IonItem
														button
														color="darker"
														className="ion-margin-start"
														onClick={() => {
															menu.current?.close();
															history.push(`/newBooking/privateBelt`);
														}}
													>
														<IonIcon
															icon={lockClosed}
															slot="start"
															size="small"
														/>
														<IonLabel>{t(`privateSnowBelt`)}</IonLabel>
													</IonItem>
												</>
											) : null}
										</IonList>
									</IonAccordion>
									{
										authUser ?
										<IonAccordion
										value="bookSurf"
										style={{
											background: `var(--ion-color-dark)`,
										}}
									>
										<IonItem
											slot="header"
											color={`dark`}
											className="ion-margin-start"
										>
											<IonLabel>{t(`surf`)}</IonLabel>
										</IonItem>
										<IonList
											slot="content"
											color="darker"
											style={{
												background: `var(--ion-color-darker)`,
											}}
										>
											<IonItem
												button
												color="darker"
												className="ion-margin-start"
												onClick={() => {
													menu.current?.close();
													//history.push(`/newBookingSurf`);
													window.location.assign(`/newBookingSurf`);
												}}
											>
												<IonIcon
													icon={ticketOutline}
													slot="start"
													size="small"
												/>
												<IonLabel>{t(`parkRide`)}</IonLabel>
											</IonItem>
											{
												authUser && !authUser.isPartner ? 
												<IonItem
												button
												color="darker"
												className="ion-margin-start"
												onClick={() => {
													menu.current?.close();
													history.push(`/newBooking/privateLane`);
												}}
											>
												<IonIcon icon={lockClosed} slot="start" size="small" />
														<IonLabel>{t(`privateSurfLane`)}</IonLabel>
											</IonItem>: null
											}
										</IonList>
									</IonAccordion>
											: null
									}
								</IonAccordionGroup>
							</IonList>
						</IonAccordion>
					</IonAccordionGroup>
					<MenuItem onClick={() => {
						menu.current?.close();
						history.push(`/page/Snow`);
					}}>
						<IoSnowOutline className="ion-margin-end"/>
						<IonLabel>{t(`snow`)}</IonLabel>
					</MenuItem>
					<MenuItem onClick={() => {
						menu.current?.close();
						history.push(`/page/Surf`);
					}}>
						<MdOutlineSurfing className="ion-margin-end" />
						<IonLabel>{t(`surf`)}</IonLabel>
					</MenuItem>
					<MenuItem onClick={() => {
						menu.current?.close();
						history.push(`/page/Our Coach`);
					}}>
						<IoBarbellOutline className="ion-margin-end"/>
						<IonLabel>{t(`coaching`)}</IonLabel>
					</MenuItem>
					<MenuItem onClick={() => {
						menu.current?.close();
						history.push(`/page/Event Space`);
					}}>
						<GiPartyPopper className="ion-margin-end"/>
						<IonLabel>{t(`eventSpace`)}</IonLabel>
					</MenuItem>
					<MenuItem
						onClick={() => {
							menu.current?.close();
							history.push(`/faq`);
						}}
					>
						<IoHelpCircleOutline className="ion-margin-end"/>
						<IonLabel>{t(`faq`)}</IonLabel>
					</MenuItem>
					<MenuItem onClick={() => {
						menu.current?.close();
						history.push(`/page/Contact Us`);
					}}>
						<IoMailOutline className="ion-margin-end"/>
						<IonLabel>{t(`contactUs`)}</IonLabel>
					</MenuItem>
					<MenuItem onClick={() => {
						menu.current?.close();
						history.push(`/page/Terms & Conditions`);
					}}>
						<IoAlertCircleOutline className="ion-margin-end"/>
						<IonLabel>{t(`Terms & Conditions`)}</IonLabel>
					</MenuItem>
				</IonList>
			</IonContent>
			<IonFooter color="dark">
				<IonToolbar color="dark">
					<IonButton
						expand="full"
						onClick={() => {
							const newLang = i18n.language === 'zh' ? 'en' : 'zh';
							i18n.changeLanguage(newLang);
							localStorage.setItem('lang', newLang);
						}}
					>
						<IonIcon icon={languageOutline} slot='start' />
						<IonLabel
							slot="end"
						>{t(`lan`)}</IonLabel>
					</IonButton>
				</IonToolbar>
			</IonFooter>
		</IonMenu>
	);
};

export default Menu;
