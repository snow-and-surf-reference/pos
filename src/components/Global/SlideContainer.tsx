

interface Props{
    className?: string;
}
const Container: React.FC<Props> = ({ children }, props) => {
    const { className } = props;
    return (
        <div
            className={className ? className : ''}
            style={{
                maxWidth: '1024px',
                width: '100%',
                height: `100%`,
                margin: 'auto',
                display: `flex`,
                flexDirection: 'column',
                justifyContent: `flex-end`,
                padding: `1rem`,
            }}
        >
            {children}
        </div>
    )
}

export default Container;