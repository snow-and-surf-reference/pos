import { IonCol, IonContent, IonGrid, IonRow } from "@ionic/react";

interface Props{
    direction?: string;
}

const WaterfallContent: React.FC<Props> = ({ children }, props) => {
    const { direction } = props

	return (
        <IonContent>
            <IonGrid
                // className="fluid"
				style={{
                    width: "100%",
					minHeight: "100%",
					position: "relative",
					display: "flex",
					justifyContent: "start",
					alignItems: "start",
					flexDirection: direction || "column",
					padding: `2rem`,
					maxWidth: `1280px`,
					margin: `auto`
				}}
			>
				<IonRow>
					<IonCol>
					{children}
					</IonCol>
				</IonRow>
			</IonGrid>
		</IonContent>
	);
};

export default WaterfallContent;
