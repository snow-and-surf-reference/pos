import { IonContent } from "@ionic/react";

interface Props{
	direction?: string;
	className?: string;
	style?: any;
}

const CenteredContent: React.FC<Props> = ({ children }, props) => {
    const { direction, className, style } = props

	return (
		<IonContent
			className={className ? className : ''}
            style={style ? style : {}}
        >
            <div
                className="fluid"
				style={{
                    width: "100%",
					minHeight: "100%",
					position: "relative",
					display: "flex",
					justifyContent: "center",
					alignItems: "center",
					flexDirection: direction || "column",
				}}
			>
				{children}
			</div>
		</IonContent>
	);
};

export default CenteredContent;
