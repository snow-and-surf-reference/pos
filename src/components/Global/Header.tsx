import {
	IonButton,
	IonButtons,
	IonHeader,
	IonIcon,
	IonImg,
	IonMenuToggle,
	IonSpinner,
	IonTitle,
	IonToolbar,
} from "@ionic/react";
import { logInOutline, menuOutline, personCircle } from "ionicons/icons";
import { useHistory } from "react-router";
import { useAppSelector } from "../../app/hooks";
import { selectAuthUser, selectAuthUserIsInit } from "../../app/slices/authUserSlice";
import Logo from "../../assets/images/2x/logo.png";

const Header: React.FC = () => {
	const history = useHistory();
	const authUser = useAppSelector(selectAuthUser);
	const authUserIsInit = useAppSelector(selectAuthUserIsInit);
	return (
		<IonHeader color="dark" className="dark">
			<IonToolbar color="dark" className="fluid">
				<IonTitle
					onClick={() => {
						history.push(`/`)
					}}
					style={{
						cursor: 'pointer'
					}}
				>
					<IonImg
						src={Logo}
						style={{
							height: "32px",
							width: "auto",
						}}
					/>
				</IonTitle>
                <IonButtons slot="start">
                    <IonMenuToggle menu="menu">
                        <IonButton fill="clear" color="light">
                            <IonIcon icon={menuOutline} slot='icon-only'/>
                        </IonButton>
                    </IonMenuToggle>
                </IonButtons>
                <IonButtons slot="end">
                    <IonMenuToggle menu="userMenu">
						<IonButton fill="clear" color="light">
							{
								authUserIsInit ?
								<IonIcon icon={
									authUser ? personCircle : logInOutline
								} slot='icon-only' />
									:
								<IonSpinner name="crescent"/>
							}
                        </IonButton>
                    </IonMenuToggle>
                </IonButtons>
			</IonToolbar>
		</IonHeader>
	);
};

export default Header;
