import Color from 'color';
import randomColor from 'randomcolor';
import { IonImg } from "@ionic/react";

interface Props {
	src?: string;
	size?: number;
	borderRadius?: number;
    usrname?: string;
    className?: string;
}

const bgColor = Color(randomColor({
    luminosity: 'light'
}));
const fontColor = bgColor.isDark() ? '#fff' : '#333';
const fontRatio: number = 1.8;

const Avatar: React.FC<Props> = (props) => {
    const { src, size, borderRadius, usrname, className } = props;

	return (
        <div
            className={`customAvatar ${className ? className : ''}`}
			style={{
				width: size ? size + "px" : `48px`,
				height: size ? size + "px" : `48px`,
				borderRadius: borderRadius ? borderRadius + "px" : "99px",
                overflow: "hidden",
                backgroundColor: src ? 'transparent' : bgColor.hex(),
                color: fontColor
			}}
		>
            {src ? <IonImg src={src} /> : usrname ? <h1 style={{fontSize: `${size ? size/fontRatio : 48/fontRatio}px`}} >{usrname[0]}</h1> : null}
		</div>
	);
};

export default Avatar;
