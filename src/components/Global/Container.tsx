

interface Props{
    className?: string;
}
const Container: React.FC<Props> = ({ children }, props) => {
    const { className } = props;
    return (
        <div
            className={className ? className : ''}
            style={{
                maxWidth: '1024px',
                width: '100%',
                margin: 'auto',
                display: `flex`,
                flexDirection: 'column',
            }}
        >
            {children}
        </div>
    )
}

export default Container;