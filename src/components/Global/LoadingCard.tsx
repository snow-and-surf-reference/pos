import { IonCard, IonCardContent, IonLabel, IonSpinner } from "@ionic/react"


const LoadingCard: React.FC = () => {
    return (
        <IonCard color={'light'}>
            <IonCardContent className="ion-text-center">
                <IonSpinner name="dots" />
                <br/>
                <IonLabel>Loading</IonLabel>
            </IonCardContent>
        </IonCard>
    )
}

export default LoadingCard;