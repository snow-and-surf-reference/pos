import {
  IonActionSheet,
  IonButton,
  IonButtons,
  IonCard,
  IonCardContent,
  IonChip,
  IonCol,
  IonContent,
  IonFooter,
  IonGrid, IonHeader, IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonMenu,
  IonModal,
  IonRow, IonToolbar
} from "@ionic/react";
import { ActionSheetButton } from "@ionic/react/dist/types/components/IonActionSheet";
import {
  Customer,
  CustomerClass
} from "@tsanghoilun/snow-n-surf-interface/types/user";
import {
  addCircleOutline,
  chevronBackOutline,
  diamondOutline,
  fileTrayFullOutline,
  logInOutline,
  logOutOutline,
  qrCodeOutline,
  storefrontOutline
} from "ionicons/icons";
import { QRCodeSVG } from "qrcode.react";
import { useEffect, useRef, useState } from "react";
import { useTranslation } from "react-i18next";
import { MdHelp } from "react-icons/md";
import { useHistory } from "react-router";
import { logout } from "../../app/firebase/global";
import { useAppSelector } from "../../app/hooks";
import {
  selectAuthUser,
  selectMaybeStaff,
  selectSubAccounts,
  setMaybeStaff
} from "../../app/slices/authUserSlice";
import { selectCustomerClasses } from "../../app/slices/customerClassesSlice";
import { store } from "../../app/store";
import i18n from "../../i18n";
import CustomerClassHelpModal from "../../pages/User/CustomerClassHelpModal";
import Avatar from "./Avatar";

interface MenuItemProps {
  children?: any;
  onClick: () => void;
}
const MenuItem: React.FC<MenuItemProps> = (props) => {
  const { children, onClick } = props;
  return (
    <IonItem
      className="main-menu__item"
      color="transparent"
      button
      onClick={() => onClick()}
    >
      {children}
    </IonItem>
  );
};

const UserMenu: React.FC = () => {
  const menu = useRef<HTMLIonMenuElement | null>(null);
  const authUser: Customer | null = useAppSelector(selectAuthUser);
  const subAccounts = useAppSelector(selectSubAccounts);
  const history = useHistory();
  const [openQrModal, setOpenQrModal] = useState(false);
  const [AllUsers, setAllUsers] = useState<Customer[]>([]);
  const [targetUserNo, setTargetUserNo] = useState("");
  const [showSwitchAccounts, setShowSwitchAccount] = useState(false);
  const classes: CustomerClass[] = useAppSelector(selectCustomerClasses);
  const [currentClass, setCurrentClass] = useState<CustomerClass | null>(null);
  const [showClassModal, setShowClassModal] = useState(false);
  const { t } = useTranslation();
  const translate = useTranslation();
  const maybeStaff = useAppSelector(selectMaybeStaff);

  useEffect(() => {}, [translate]);

  useEffect(() => {
    if (authUser) setTargetUserNo(authUser.memberId);
  }, [authUser]);

  useEffect(() => {
    if (authUser && subAccounts) {
      setAllUsers([...subAccounts, authUser]);
    }
  }, [targetUserNo, authUser, subAccounts]);

  useEffect(() => {
    if (!authUser || classes.length <= 0) {
      return;
    }
    const tempClasses: CustomerClass[] = classes
      .filter((x) => x.creditTier <= authUser.wallet.spent)
      .sort((a, b) => a.creditTier - b.creditTier);
    const highestClass: CustomerClass = tempClasses[tempClasses.length - 1];
    setCurrentClass({ ...highestClass });
  }, [classes, authUser]);

  return (
    <IonMenu
      side="end"
      contentId="main"
      menuId="userMenu"
      type="overlay"
      color="dark"
      ref={menu}
    >
      <IonContent color="dark">
        {authUser ? (
          <>
            <div
              style={{
                marginTop: `12vw`,
              }}
            >
              <IonGrid style={{ padding: `1rem` }}>
                <IonRow>
                  <IonCol size="3">
                    <Avatar usrname={authUser.usrname} />
                  </IonCol>
                  <IonCol>
                    <IonLabel color="light">
                      <h3>{t(`welcomeBack`)},</h3>
                      <h2>{authUser.usrname}</h2>
                    </IonLabel>
                    <IonLabel color={`warning`}>
                      {currentClass && (
                        <h4
                          style={{
                            display: "flex",
                            justifyItems: "center",
                            alignItems: "center",
                          }}
                        >
                          {i18n.language === "en"
                            ? currentClass.name.en
                            : currentClass.name.zh || currentClass.name.en}
                          {` `}
                          <MdHelp
                            size={20}
                            style={{ marginLeft: "0.5rem", cursor: "pointer" }}
                            onClick={() => {
                              setShowClassModal(true);
                            }}
                          />
                        </h4>
                      )}
                    </IonLabel>
                  </IonCol>
                </IonRow>
                <IonRow>
                  <IonCol>
                    <IonButtons>
                      <IonChip color="secondary">
                        <IonIcon icon={diamondOutline} size="small" />
                        <IonLabel>
                          <h3>{authUser.wallet.balance}</h3>
                        </IonLabel>
                      </IonChip>
                      <IonChip
                        color="light"
                        onClick={() => {
                          menu.current?.close();
                          history.push(`/topup`);
                        }}
                      >
                        <IonLabel>
                          <h3>
                            {t(`buyCredits`)} {`>`}
                          </h3>
                        </IonLabel>
                      </IonChip>
                    </IonButtons>
                  </IonCol>
                </IonRow>
                <IonRow>
                  <IonCol>
                    <IonButton
                      color={`warning`}
                      onClick={() => {
                        setOpenQrModal(true);
                      }}
                    >
                      <IonIcon icon={qrCodeOutline} size="large" slot="start" />
                      <IonLabel>#{authUser.memberId}</IonLabel>
                    </IonButton>
                  </IonCol>
                </IonRow>
              </IonGrid>
              <br />
              <IonList lines="none" color="transparent">
                <MenuItem
                  onClick={() => {
                    menu.current?.close();
                    history.push("/myBookings");
                  }}
                >
                  <IonIcon
                    slot="start"
                    size="small"
                    icon={fileTrayFullOutline}
                  />
                  <IonLabel>{t(`myBookings`)}</IonLabel>
                </MenuItem>
                <MenuItem
                  onClick={() => {
                    menu.current?.close();
                    history.push("/myTopups");
                  }}
                >
                  <IonIcon slot="start" size="small" icon={diamondOutline} />
                  <IonLabel>{t(`myTopUpHistory`)}</IonLabel>
                </MenuItem>
                <MenuItem
                  onClick={() => {
                    menu.current?.close();
                    history.push("/myAccount");
                  }}
                >
                  <IonIcon slot="start" size="small" icon={storefrontOutline} />
                  <IonLabel>{t(`myAccount`)}</IonLabel>
                </MenuItem>
                <MenuItem
                  onClick={async () => {
                    await logout();
                    menu.current?.close();
                    history.push("/");
                  }}
                >
                  <IonIcon slot="start" size="small" icon={logOutOutline} />
                  <IonLabel>{t(`logout`)}</IonLabel>
                </MenuItem>
              </IonList>
            </div>
            <IonModal
              isOpen={openQrModal}
              onDidDismiss={() => setOpenQrModal(false)}
            >
              <IonContent>
                {AllUsers.filter((x) => x.memberId === targetUserNo).map(
                  (item) => (
                    <div key={`card-${item.id}`}>
                      <IonCard color={`dark`}>
                        <IonCardContent className="ion-no-padding">
                          <IonItem color={`dark`} lines="none">
                            <Avatar
                              usrname={authUser.usrname}
                              size={32}
                              className="ion-margin-end"
                            />
                            <IonLabel>
                              <h2>{item.usrname}</h2>
                              <p>{item.isSub ? `Sub Account` : item.email}</p>
                            </IonLabel>
                          </IonItem>
                        </IonCardContent>
                      </IonCard>
                      <IonCard color={`light`}>
                        <IonCardContent>
                          <div className="ion-margin ion-text-center">
                            <QRCodeSVG value={`${item.id}`} size={192} />
                          </div>
                          <br />
                          <div className="ion-margin ion-text-center">
                            <p>{t(`member`)}#</p>
                            <div className="large-order-no">
                              {item.memberId}
                            </div>
                          </div>
                        </IonCardContent>
                      </IonCard>
                    </div>
                  )
                )}

                {AllUsers.length > 1 && (
                  <IonButton
                    expand="block"
                    className="ion-margin"
                    color="tertiary"
                    onClick={() => setShowSwitchAccount(true)}
                  >
                    {t(`Switch Sub Accounts`)}
                  </IonButton>
                )}
              </IonContent>
              <IonFooter color="dark">
                <IonToolbar color="dark">
                  <IonButton
                    expand="full"
                    onClick={() => {
                      setOpenQrModal(false);
                    }}
                  >
                    {t(`finish`)}
                  </IonButton>
                </IonToolbar>
              </IonFooter>
            </IonModal>

            <IonActionSheet
              isOpen={showSwitchAccounts}
              onDidDismiss={() => {
                setShowSwitchAccount(false);
              }}
              buttons={
                [
                  ...AllUsers.map((x) => {
                    return {
                      text: `${x.isSub ? `` : `(${t(`mainAccount`)}) `}${
                        x.usrname
                      }`,
                      handler: () => {
                        setTargetUserNo(x.memberId);
                      },
                    };
                  }),
                ] as ActionSheetButton[]
              }
            ></IonActionSheet>
          </>
        ) : (
          <>
            <div
              style={{
                marginBottom: "15vh",
              }}
            ></div>
            <IonGrid>
              <IonRow>
                <IonCol>
                  <IonCard color={"darker"}>
                    <IonCardContent className="ion-text-center">
                      <IonLabel className="ion-text-center" color={"medium"}>
                        {t(`You are not signed in.`)}
                      </IonLabel>
                    </IonCardContent>
                  </IonCard>
                </IonCol>
              </IonRow>
              <IonRow>
                <IonCol>
                  <IonButton
                    color={`warning`}
                    expand="block"
                    onClick={() => {
                      menu.current?.close();
                      history.push(`/login`);
                    }}
                  >
                    <IonIcon icon={logInOutline} slot="start" />
                    <IonLabel>{t(`login`)}</IonLabel>
                  </IonButton>
                </IonCol>
              </IonRow>
              <IonRow>
                <IonCol>
                  <IonButton
                    color={`secondary`}
                    fill="clear"
                    expand="block"
                    onClick={() => {
                      menu.current?.close();
                      history.push(`/signup`);
                    }}
                  >
                    <IonIcon icon={addCircleOutline} slot="start" />
                    <IonLabel>{t(`Create New Account`)}</IonLabel>
                  </IonButton>
                </IonCol>
              </IonRow>
            </IonGrid>
          </>
        )}
      </IonContent>
      <CustomerClassHelpModal
        isOpen={showClassModal}
        onDismiss={() => setShowClassModal(false)}
      />
      <IonModal
        isOpen={maybeStaff}
        onDidDismiss={() => {
          store.dispatch(setMaybeStaff(false))
        }}
      >
        <IonHeader>
          <IonToolbar>
            <IonButton slot="start"
              fill="clear"
              color={'danger'}
              onClick={() => {
                store.dispatch(setMaybeStaff(false))
              }}
            >
              <IonIcon icon={chevronBackOutline} slot='start' />
              <IonLabel>Back</IonLabel>
            </IonButton>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          <IonCard color={'light'}>
            <IonCardContent>
              <IonLabel>
                <h3><b>{`Looks like you are a Staff and trying to login as a customer... Please make sure to login with your customer user's email. Staff accounts and Customer accounts are NOT shared. Thanks`}</b></h3>
              </IonLabel>
            </IonCardContent>
          </IonCard>
        </IonContent>
      </IonModal>
    </IonMenu>
  );
};

export default UserMenu;
